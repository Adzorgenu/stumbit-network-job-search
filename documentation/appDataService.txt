Service holds some key information/data needed for the app.

userInfo = {
    token: "",
    accountActivated: false,
    registrationType:""
};

Getters and setters for each user property.

User token information, user account activation status, and user registration Type (to make sure user is on the
right section)