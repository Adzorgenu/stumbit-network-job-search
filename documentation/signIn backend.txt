Routes for backend signIn:

ROUTE ENDPOINT 1: signin In
POST: domain/accounts/signIn

form properties:
{
    email:"",
    password:""
}

Email should be a valid standard email. Password should only contain certain characters only.
Refer to functions.js for definitons.
Validations are performed on all fields.password should be at least 8 characters.

If validation fails, the form is returned back to the client with the appropriate messages.

Validation passes:
User information is checked in the database. if either user exists and password is incorrect or user does not
exist, a response is sent to the user, indicating that the verified status is false with a message.
response.data.other = {verified:false,msg:"Incorrect username  or password"};

Response type is of three types: error(server error),okay(everything went fine), and form(form validation error).

In all cases the server responds with a status of 200. Okay (but an innner response status of whatever it should be).

If user exists and password is correct, a token is generated for the user. Token contains:
userID,userRegistrationType,userEmail as payload

response.data.other  is set to {verified:true/false,activated:true/fase} based on whether user was
correctly verified or not and activated whether user has activated his/her account.


