Form properties:
{
    email:"",
    password:""
}

Form's validity status is being tracked by the object:
signInFormState = {
    email: {isValid: true, value:null, msg: null},
    password: {isValid: true, value:null, msg: null}
};

The local form state changes on blur of the email and password fields. On blur, the fields are checked
if they're valid based on their validation rules.

password is validated using a pattern. All validation rules and messages are found in
services/other/custom-validators service.

Error messages are displayed based on the msg content of each form property in signInFormState.

The submit button is disabled when the form is invalid.

The formState is reset only once after data/response has been received from the server. this is to prevent
displaying an error message twice since the same form state properties are used for validation by server and
client.

When the form is submitted, any validation error is displayed in the error alerts. However, an internal server error
is displayed on top of the form(right beneath the modal-header) with a message "oh snap! a problem occurred."

if the information is checked and user is properly authenticated, the received token is stored in app-data service,
and the user is authenticated on the client(using authentication service).

User is then rerouted to the right dashboard(user/company)

Two variables/members:
serverErrorEncountered:bool
serverErrorMsg: string,
keep track of a server response whether it has an error(system error) or not. An alert is displayed if the
response from the server is a system error(status: 500). Alert is displayed beneath the modal header