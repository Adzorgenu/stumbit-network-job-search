import {NgModule} from '@angular/core';
import {Routes, RouterModule} from "@angular/router";

//components
import { HomeComponent } from './home/home.component';
import { CompanyDashboardComponent } from './dashboard/company/company.component';
import { ApplicantDashboardComponent } from './dashboard/applicant/applicant.component';
import { CompanyProfileComponent } from './company-features/profile/profile.component';
import { CompanyDashboardHomeComponent } from './company-features/dashboard-home/dashboard-home.component';
import { ApplicantDashboardHomeComponent } from './applicant-features/dashboard-home/dashboard-home.component';
import { ApplicantSearchJobsComponent } from './applicant-features/search-jobs/search-jobs.component';
import { CompanyPostJobsComponent } from './company-features/post-jobs/post-jobs.component';
import { ApplicantAppliedJobsComponent } from './applicant-features/applied-jobs/applied-jobs.component';
import { ApplicantViewAppliedJobDetailsComponent } from './applicant-features/view-applied-job-details/view-applied-job-details.component';
import { ApplicantApplyConfirmationComponent } from './applicant-features/apply-confirmation/apply-confirmation.component';
import { ApplicantProfileComponentOne} from "./applicant-features/profile/profile-one.component";
import { ApplicantProfileComponentTwo} from "./applicant-features/profile/profile-two.component";
import { ApplicantProfileComponentThree} from "./applicant-features/profile/profile-three.component";
import { ApplicantProfileComponentFour} from "./applicant-features/profile/profile-four.component";
import { ApplicantLatestJobsComponent } from './applicant-features/latest-jobs/latest-jobs.component';

//resolvers
import {ApplicantProfileBasicResolverService} from "./services/other/applicant-profile-basic-resolver.service";
import {ApplicantProfileContactResolverService} from "./services/other/applicant-profile-contact-resolver.service";
import {ApplicantProfileResidenceResolverService} from "./services/other/applicant-profile-residence-resolver.service";
import {ApplicantProfileFilesResolverService} from "./services/other/applicant-profile-file-resolver.service";

//routes
//define app routes
//define app routes
//noinspection TypeScriptUnresolvedVariable
const appRoutes: Routes = [
	{path:'',component:HomeComponent},
	{path:'company',component:CompanyDashboardComponent,children:[
		{path:'',redirectTo:'dashboard',pathMatch:'full'},
		{path:'dashboard',component:CompanyDashboardHomeComponent},
		{path:'profile',component:CompanyProfileComponent},
		{path: 'post-jobs', component: CompanyPostJobsComponent}
	]},
	{path:'applicant',component:ApplicantDashboardComponent,children:[
		{path:'',redirectTo:'dashboard',pathMatch:'full'},
		{path:'dashboard',component:ApplicantDashboardHomeComponent},
		{path:'profile',redirectTo:'profile/basic'},
		{path:'profile/basic',component:ApplicantProfileComponentOne,
			resolve:{resolver:ApplicantProfileBasicResolverService}
		},
		{path:'profile/contact',component:ApplicantProfileComponentTwo,
			resolve:{resolver:ApplicantProfileContactResolverService}
		},
		{path:'profile/residence',component:ApplicantProfileComponentThree,
			resolve:{resolver:ApplicantProfileResidenceResolverService}
		},
		{path:'profile/files',component:ApplicantProfileComponentFour,
			resolve:{resolver:ApplicantProfileFilesResolverService}
		},
		{path: 'search-jobs', component: ApplicantSearchJobsComponent},
		{path: 'applied-jobs', component: ApplicantAppliedJobsComponent},
		{path: 'view-details', component: ApplicantViewAppliedJobDetailsComponent},
		{path: 'confirm-application', component: ApplicantApplyConfirmationComponent},
		{path: 'latest-jobs', component: ApplicantLatestJobsComponent}
	]},
];

@NgModule({
	imports: [
		RouterModule.forRoot(appRoutes)
	],
	exports:[RouterModule]
})

export class AppRoutingModule {
}