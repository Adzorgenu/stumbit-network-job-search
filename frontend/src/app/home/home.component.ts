import {Component,OnInit} from '@angular/core';

@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: [
		'./home.component.css',
		'../../generic/styles/landing-page.min.css',
		'../../generic/styles/generic-styles.css'
	]
})

// ,'../../generic/styles/landing-page.min.css'
export class HomeComponent implements OnInit{
	isNavbarCollapsed: boolean = true;
	constructor() {}

	ngOnInit() {
	}
}
