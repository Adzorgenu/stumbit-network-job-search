import { ViewChild, Component, OnInit,} from '@angular/core';
import {NgForm} from "@angular/forms";
import {Router} from '@angular/router';

import {SignUpInService} from "../../services/backend-http/sign-up-in.service";
import {AppDataService} from "../../services/data/app-data.service";
import {AuthenticationService} from "../../services/other/authentication.service";
import {CustomValidatorsService} from "../../services/other/custom-validators.service";

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: [
    '../home.component.css',
    '../../../generic/styles/landing-page.min.css',
  ]})

// ,'../../../generic/styles/landing-page.min.css'
export class SignUpComponent implements OnInit {
  @ViewChild('signUpForm') signUpForm: NgForm;
  
  userID:string = "";
  isEmailSent:boolean = false;
  serverErrorEncountered:boolean = false;
  serverErrorMsg:string = "";
  passwordRegex:string = "";

  //centralize validation messages/error messages
  //form validity
  signUpFormState = {
    email: {isValid: true, value:null, msg: null},
    password: {isValid: true, value:null, msg: null},
    retypePassword: {isValid: true, value:null, msg: null},
    registrationType: {isValid: true, value:null, msg: null}
  };

  //are we on signUp stage 2. set to true if we're on the second phase
  // is used to toggle content of signUp modal
  isSignUpStage2 = false;

  constructor(private signInUpbackend: SignUpInService,private appData:AppDataService,
              private authServ:AuthenticationService, private router: Router,
              private validators:CustomValidatorsService)
  {
    this.passwordRegex = validators.passwordRegex();

    //sigIn form validation messages gotten from custom-validators service
    this.signUpFormState.email.msg = validators.getValidationMsg('email');
    this.signUpFormState.password.msg = validators.getValidationMsg('password');
    this.signUpFormState.retypePassword.msg = validators.getValidationMsg('password');
    this.signUpFormState.registrationType.msg = validators.getValidationMsg('registrationType');
  }

  ngOnInit() {
  }

  //update form validity state
  updateFormState(type:string)
  {
    //update form validity state state
    if(type == "email") {
      //noinspection TypeScriptUnresolvedVariable
      this.signUpFormState.email.isValid = this.signUpForm.controls.email.valid;
      //get validation message. this is to help overide any value set after a response was received from the server
      this.signUpFormState.email.msg = this.validators.getValidationMsg('email'); 

      //check backend if email exists.
      if(this.signUpFormState.email.isValid){
        this.checkIfEmailExists(this.signUpForm.value.email);
      }
    }
    else if(type == "password") {
      //noinspection TypeScriptUnresolvedVariable
      this.signUpFormState.password.isValid = this.signUpForm.controls.password.valid;
      this.signUpFormState.password.msg = this.validators.getValidationMsg('password');
    }
    else if(type == "retypePassword") {
      //noinspection TypeScriptUnresolvedVariable
      this.signUpFormState.retypePassword.isValid = this.signUpForm.controls.retypePassword.valid;
      this.signUpFormState.retypePassword.msg = this.validators.getValidationMsg('password');

      //no need to check for password match if retypePassword is invalid
      if(this.signUpFormState.retypePassword.isValid)
      {
        //check for password match
        if(this.signUpForm.value.password != this.signUpForm.value.retypePassword){
          this.signUpFormState.retypePassword.isValid = false;
          this.signUpFormState.retypePassword.msg = this.validators.getValidationMsg('confirmPassword');
        }else{
          this.signUpFormState.retypePassword.isValid = true;
        }
      }//end of retypePassword validity
    }//end of retype password condition
    else if(type == "registrationType") {
      //noinspection TypeScriptUnresolvedVariable
      this.signUpFormState.registrationType.isValid = this.signUpForm.controls.registrationType.valid;
      this.signUpFormState.registrationType.msg = this.validators.getValidationMsg('registrationType');
    }
  }//end of method.

  //check backend if user email exists
  checkIfEmailExists(email) {
    this.signInUpbackend.userEmailExists(email)
         .subscribe((response) =>
         {
           // console.log(response);
           //process response
           this.processCheckEmailExistence(response);
         },(error) => {
           // console.log(error);
           //indicate that something went wrong
           //process error
           // this.processError(error.statusText);
           this.processError("A problem occurred.");
         });
  }

  //process the results after an email is checked whether it exists
  processCheckEmailExistence(response)
  {
    //update signUp form state with server form validation response
    if(response.response == "form"){
      this.signUpFormState.email.isValid = response.form.email.isValid;
      this.signUpFormState.email.msg = response.form.email.msg;
    }
    else if(response.response == "okay")
    {
      //IF EMAIL EXISTS ?
      if(response.data.other.exists)
      {
        //email is no longer valid if email exists because it's has been used.
        this.signUpFormState.email.isValid = false;
        this.signUpFormState.email.msg = this.validators.getValidationMsg('emailExists');
      }else {
        this.signUpFormState.email.isValid = true;
      }//end of exists
    }//end of okay

  }//end of method

  //Handle signup form submission
  onSubmitSignUp()
  {
    //validation would have been handled
    if(this.signUpForm.valid && this.signUpFormState.email.isValid && this.signUpFormState.retypePassword.isValid)
    {
        // send form to backend server
        // console.log(this.signUpForm.value);
        this.signInUpbackend.submitSignUpForm(this.signUpForm.value)
             .subscribe((response) => {
               // console.log(response);
               //process response
               this.processSignUpFormResponse(response);
             },(error) => {
               // console.log(error);
               //indicate that something went wrong
               //process error
               // this.processError(error.statusText);
               this.processError("A problem occurred.");
             });

    }//end of valid form
  }//end of onsubmit of sign up


  //process signUp Form Response
  processSignUpFormResponse(response)
  {
    //1.check if it's validation error. if it's then add error messages to DOM
    //2.else replace modal contents with a new html
    //3.else indicate that there's a problem was encountered.(Try again)
    if (response.response == "form") {
      this.signUpFormState = response.form;
      this.serverErrorEncountered = false;  //server didn't encounter any error. so no need showing alert
      this.serverErrorMsg = "";
    }
    else if (response.response == "okay") {
      //replace modal contents with a new content
      // indicating whether an email was sent or not.
      //set email sent fields and userId if email was not sent
      this.isSignUpStage2 = true;

      if(response.data.other == "notsent") {
        this.isEmailSent = false;
        this.userID = response.data.fectched;
      }else {
        this.isEmailSent = true;
      }

      this.serverErrorEncountered = false;  //server didn't encounter any error. so no need showing alert
      this.serverErrorMsg = "";

    }
    else if(response.response == "error") {
      this.processError(response.msg);
    }
  }//end of processing signup form

  //resend user's email
  resendEmailMsg(){
    // resend email to client
    // console.log(this.userID);
    this.signInUpbackend.resendUserEmail(this.userID)
         .subscribe((response) => {
           // console.log(response);
           //process response
           this.processSignUpFormResponse(response);
         },(error) => {
           // console.log(error);
           //indicate that something went wrong
           //process error
           // this.processError(error.statusText);
           this.processError("A problem occurred");
         });
  }


  /*
   * GENERIC
   */
  // process error. Indicate that something went wrong
  //and display on the form
  processError(errorMsg) {
    this.serverErrorEncountered = true;  //server encountered any error.
    this.serverErrorMsg = errorMsg;
  }
}
