import { ViewChild, Component, OnInit,OnChanges} from '@angular/core';
import {NgForm} from "@angular/forms";
import {Router} from '@angular/router';

import {SignUpInService} from "../../services/backend-http/sign-up-in.service";
import {AppDataService} from "../../services/data/app-data.service";
import {AuthenticationService} from "../../services/other/authentication.service";
import {CustomValidatorsService} from "../../services/other/custom-validators.service";

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: [
    '../home.component.css',
    '../../../generic/styles/landing-page.min.css',
    '../../../generic/styles/generic-styles.css'
  ]
})

// ,'../../../generic/styles/landing-page.min.css'
export class SignInComponent implements OnInit {
  @ViewChild('signInForm') signInForm: NgForm;
  serverErrorEncountered:boolean = false;
  serverErrorMsg:string = "";
  passwordRegex = "";
  resetSt = 0;

  //centralize validation messages/error messages
  signInFormState = {
    email: {isValid: true, value:null, msg: null},
    password: {isValid: true, value:null, msg: null}
  };

  constructor(private signInUpbackend: SignUpInService,private appData:AppDataService,
              private authServ:AuthenticationService, private router: Router,
              private validators:CustomValidatorsService)
  {
    this.passwordRegex = validators.passwordRegex();

    //sigIn form validation messages
    this.signInFormState.email.msg = validators.getValidationMsg('email');
    this.signInFormState.password.msg = validators.getValidationMsg('password');
  }

  ngOnInit() {
  }

  //reset signInForm State
  resetSignInFormState(){
    this.signInFormState = {
      email: {isValid: true, value:null, msg: this.validators.getValidationMsg('email')},
      password: {isValid: true, value:null, msg: this.validators.getValidationMsg('password')}
    };
    this.resetSt = 0;
  }//end of method
  
  //update form validity state
  updateFormState(type:string) 
  {
    //reset signInForm only once
    if (this.resetSt == 1)
      this.resetSignInFormState();

    //update form validity state state
    if(type == "email") {
      //noinspection TypeScriptUnresolvedVariable
      this.signInFormState.email.isValid = this.signInForm.controls.email.valid;
    }
    else if(type == "password") {
      //noinspection TypeScriptUnresolvedVariable
      this.signInFormState.password.isValid = this.signInForm.controls.password.valid;
    }
  }//end of method.

  //handle sign up form submission
  onSubmitSignIn()
  {
    //validation would have been handled
    if (this.signInForm.valid)
    {
        // console.log(this.signInForm)
        // send form to backend server
        // console.log(this.signInFormState);
        this.signInUpbackend.submitSignInForm(this.signInForm.value)
             .subscribe((response) => {
               // console.log(response);
               //process response
               this.processSignInForm(response);
             },(error) => {
               // console.log(error);
               //indicate that something went wrong
               //process error
               // this.processError(error.statusText);
               this.processError("A problem occurred.");
              });
    }//end of valid form
  }//end of submit

  //process signIn Form Response
  processSignInForm(response)
  {
    //1.check if it's validation error. if it's then add error messages to DOM
    //2.else replace modal contents with a new html
    //3.else indicate that there's a problem was encountered.(Try again)
    if (response.response == "form") {
      //set response form to form state
      this.signInFormState.email.isValid = response.form.email.isValid;
      this.signInFormState.email.msg = response.form.email.msg;
      this.signInFormState.password.isValid = response.form.password.isValid;
      this.signInFormState.password.msg = response.form.password.msg;

      this.serverErrorEncountered = false;  //server didn't encounter any error. so no need showing alert
      this.serverErrorMsg = "";
    }
    else if (response.response == "okay")
    {
      //response.data.other contains info as to whether the user was authenticated or not.
      //is user was properly verified and is a valid user
      if (response.data.other.verified)
      {
        //store the verified status, and token in a data service, and registration type
        //in app data
        this.appData.setUserToken(response.data.token);
        this.appData.setAccountActivatedStatus(response.data.other.activated);
        this.appData.setUserRegistrationType(response.data.path);

        //set user Authentication
        this.authServ.authenticateUser();

        //redirect to right dashboard. dashboard.
        let path = (response.data.path == "applicant") ? "user": response.data.path;

        //domain/whatever/dashboard
        this.router.navigate(['/'+path,'dashboard']);

      }else {
        //change server authentication results
        this.signInFormState.email.isValid = false;
        this.signInFormState.email.msg = response.data.other.msg;
        this.resetSt = 1; //set reset state(to enable reseting form state only once)
      }

      this.serverErrorEncountered = false;  //server didn't encounter any error. so no need showing alert
      this.serverErrorMsg = "";
    }
    else if(response.response == "error") {
      this.processError(response.msg);
    }
  }//end of processing signin form

  /*
   * GENERIC
   */
  // process error. Indicate that something went wrong
  //and display on the form
  processError(errorMsg) {
    this.serverErrorEncountered = true;  //server encountered any error.
    this.serverErrorMsg = errorMsg;
  }
}
