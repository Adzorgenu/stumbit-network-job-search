import {Component, ViewChild} from '@angular/core';
import {NgForm} from "@angular/forms";
import {Router, ActivatedRoute, Data, Params} from '@angular/router';
import {CustomValidatorsService} from "../../services/other/custom-validators.service";
import {ApplicantOperationsService} from "../../services/backend-http/applicant-operations.service";

@Component({
	selector: 'company-profile-one',
	templateUrl: './profile-one.component.html',
	styleUrls: ['../../../assets/styles/profile.component.css']
})

export class ApplicantProfileComponentOne {
	@ViewChild('form') form:NgForm; //basic form
	serverErrorEncountered:boolean = false;
	serverErrorMsg:string = "";

	//form state
	formState= {
		fname:{isValid:true,value:"",errMsg:"",regex:""},
		mname:{isValid:true,value:"",errMsg:"",regex:""},
		lname:{isValid:true,value:"",errMsg:"",regex:""},
		bdate:{isValid:true,value:null,errMsg:""},
		maritalStatus:{isValid:true,value:"",errMsg:""},
		sex:{isValid:true,value:"",errMsg:""},
		highEdu:{isValid:true,value:"",errMsg:""},
		school_other:{isValid:true,value:"",errMsg:"",regex:""},
		employStatus:{isValid:true,value:"",errMsg:""},
		work_other:{isValid:true,value:"",errMsg:"",regex:""},
		profileID:{value:""}
	};

	//toggline highEdu other and employ status other fields
	isHighEduOther:boolean = false;
	isEmployOther:boolean = false;

	hasFormChanged:boolean = false;
	isNewForm:boolean = false;
	phase:number = 1;

	//types for select fields
	sexTypes = [];
	maritalStatusTypes = [];
	employStatusTypes = [];
	highEduTypes = [];


	constructor(private router: Router, private route:ActivatedRoute,private validators:CustomValidatorsService,
	private applicantOp: ApplicantOperationsService)
	{
		//set the formState regex and msgs
		this.formState.fname.regex = this.validators.nameRegex();
		this.formState.fname.errMsg = this.validators.getValidationMsg('name');
		this.formState.mname.regex = this.validators.nameRegex();
		this.formState.mname.errMsg = this.validators.getValidationMsg('name');
		this.formState.lname.regex = this.validators.nameRegex();
		this.formState.lname.errMsg = this.validators.getValidationMsg('name');
		this.formState.bdate.errMsg = this.validators.getValidationMsg('date');
		this.formState.maritalStatus.errMsg = this.validators.getValidationMsg('select');
		this.formState.sex.errMsg = this.validators.getValidationMsg('select');
		this.formState.highEdu.errMsg = this.validators.getValidationMsg('select');
		this.formState.school_other.regex = this.validators.wordsRegex();
		this.formState.school_other.errMsg = this.validators.getValidationMsg('name');
		this.formState.employStatus.errMsg = this.validators.getValidationMsg('select');
		this.formState.work_other.regex = this.validators.wordsRegex();
		this.formState.work_other.errMsg = this.validators.getValidationMsg('name');
	}

	ngOnInit()
	{
		//get form data from resolver
		//get applicant profile information from server
		this.route.data.subscribe(
			(data:Data) =>
			{
				let response = data['resolver'];

				//check whether the response is an array.(only errors are in arrays[false,err])
				//normal response are in response
				if(Array.isArray(response)){
					//happens when an error is encountered. resolver makes response false
					let errMsg = (response[1].name && response[1].name == "HttpErrorResponse") ?
						"Could not connect to server.": "Something went wrong.";
					//process error
					this.processError(errMsg);
				}
				else{ //no error
					this.processResponse(response);     //process response
				}
			},(err) => {
				let errMsg = (err.name && err.name == "HttpErrorResponse") ?
					"Could not connect to server.": err.statusText;
				this.processError(errMsg);
			});
	}

	//process response
	processResponse(response)
	{
		//1.update form with values
		//3. handle error
		if (response.response == "error") {
			this.processError(response.msg);
		}
		else if (response.response == "okay")
		{
			this.sexTypes = response.data.other.sexTypes;
			this.maritalStatusTypes = response.data.other.maritalStatusTypes;
			this.highEduTypes = response.data.other.highEduTypes;
			this.employStatusTypes = response.data.other.employStatusTypes;

			//check if isNewProfile
			if(response.data.other.isNewProfile){
				this.isNewForm = true;
			}
			else
			{
				this.isNewForm = false;
				//set form state(s)
				this.formState.fname.value = response.data.fetched.fname;
				this.formState.mname.value = response.data.fetched.mname;
				this.formState.lname.value = response.data.fetched.lname;
				this.formState.maritalStatus.value = response.data.fetched.maritalStatus;
				this.formState.sex.value = response.data.fetched.sex;
				this.formState.highEdu.value = response.data.fetched.highEdu;
				this.formState.school_other.value = response.data.fetched.school_other;
				this.formState.employStatus.value = response.data.fetched.employStatus;
				this.formState.work_other.value = response.data.fetched.work_other;
				if(response.data.fetched.bdate != ""){
					this.formState.bdate.value = new Date(response.data.fetched.bdate);
				}
				this.formState.profileID.value = response.data.fetched.profileID;

				//toggle employStatus other field
				this.isEmployOther = (this.formState.employStatus.value == "other") ? true:false;

						//toggle fields (higher education other)
				this.isHighEduOther = (this.formState.highEdu.value == "other") ? true:false;

			}//existing profile
		}//end of res.okay
	}//end method

	//update formState
	//update formState & form validity
	updateFormState(type:string)
	{
		//a form was update
		this.hasFormChanged = true;

		//set for state validity
		if(type=="fname"){
			//noinspection TypeScriptUnresolvedVariable
			this.formState.fname.isValid = this.form.controls.fname.valid;
		}
		else if(type =="mname"){
			//noinspection TypeScriptUnresolvedVariable
			this.formState.mname.isValid = this.form.controls.mname.valid;
		}
		else if(type =="lname"){
			//noinspection TypeScriptUnresolvedVariable
			this.formState.lname.isValid = this.form.controls.lname.valid;
		}
		else if(type == "sex"){
			//noinspection TypeScriptUnresolvedVariable
			this.formState.sex.isValid = this.form.controls.sex.valid;
		}
		else if(type == "bdate") {
			//noinspection TypeScriptUnresolvedVariable
			this.formState.bdate.isValid = this.form.controls.bdate.valid;
		}
		else if(type == "maritalStatus")
		{
			//noinspection TypeScriptUnresolvedVariable
			this.formState.maritalStatus.isValid = this.form.controls.maritalStatus.valid;
		}
		else if(type == "highEdu"){
			//noinspection TypeScriptUnresolvedVariable
			this.formState.highEdu.isValid = this.form.controls.highEdu.valid;

			//toggle fields (higher education other)
			this.isHighEduOther = (this.formState.highEdu.value == "other") ? true:false;
		}
		else if(type == "school_other") {
			//noinspection TypeScriptUnresolvedVariable
			this.formState.school_other.isValid = this.form.controls.school_other.valid;
		}
		else if(type == "employStatus") {
			//noinspection TypeScriptUnresolvedVariable
			this.formState.employStatus.isValid = this.form.controls.employStatus.valid;

			//toggle employStatus other field
			this.isEmployOther = (this.formState.employStatus.value == "other") ? true:false;
		}
		else if(type == "work_other"){
			//noinspection TypeScriptUnresolvedVariable
			this.formState.work_other.isValid = this.form.controls.work_other.valid;
		}
	}

	//skip this section
	skipForm() {
		//navigate to next phase s
		this.router.navigate(['../contact'],{relativeTo:this.route});
	}

	//previousForm
	previousForm() {
		//navigate to previous phase
		this.router.navigate(["../basic"],{relativeTo:this.route});
	}

	//submit form
	onSubmit()
	{
		if(this.hasFormChanged)
		{
			if (this.form.valid)
			{
				//submit forms before moving to the next phase
				//since date will be a date object ,convert to string in the format yyyy-mm-dd
				//getMonth() returns 0-11. Add 1 to get correct month
				let bdate = this.formState.bdate.value.getFullYear() + "-" + (this.formState.bdate.value.getMonth() + 1);
				bdate += "-" + this.formState.bdate.value.getDate();

				let submittedForm = {
					fname: this.formState.fname.value,
					mname: this.formState.mname.value,
					lname: this.formState.lname.value,
					bdate: bdate,
					maritalStatus: this.formState.maritalStatus.value,
					sex: this.formState.sex.value,
					highEdu: this.formState.highEdu.value,
					school_other: (this.isHighEduOther) ? this.formState.school_other.value : "",
					employStatus: this.formState.employStatus.value,
					work_other: (this.isEmployOther) ? this.formState.work_other.value : "",
					profileID: this.formState.profileID.value
				};


				let mode = (this.isNewForm) ? "n" : "u";

				//submit form
				// console.log(submittedForm);
				this.applicantOp.submitApplicantProfileForm(submittedForm, this.phase, mode)
					.subscribe((response:any)=> {
						this.processFormSubmissionResponse(response);
					}, (error)=> {
						//happens when an error is encountered. resolver makes response false
						let errMsg = (error.name == "HttpErrorResponse") ? "Could not connect to server." :
							'Something went wrong.';
						this.processError(errMsg);
					});
			}//end of valid form
		}//form was changed
		else{
			//navigate to next phase (if form wasn't changed)
			this.skipForm();
		}
	}

	//process form submission
	processFormSubmissionResponse(response)
	{
		if(response.response == "okay")
		{
			if(response.data.other.isSaved)
			{
				if(this.isNewForm){
					this.isNewForm = false;
				}

				//navigate to next form
				this.skipForm();
			}
		}
		else if(response.response == "error"){
			this.processError(response.msg);
		}
		else if(response.response == "form")
		{
			//set form error messages and validities
			this.formState.fname.isValid = response.form.fname.valid;
			if(!this.formState.fname.isValid) {
				this.formState.fname.errMsg = response.form.fname.error;
			}

			this.formState.mname.isValid = response.form.mname.valid;
			if(!this.formState.mname.isValid) {
				this.formState.mname.errMsg = response.form.mname.error;
			}

			this.formState.lname.isValid = response.form.lname.valid;
			if(!this.formState.lname.isValid) {
				this.formState.lname.errMsg = response.form.lname.error;
			}

			this.formState.bdate.isValid = response.form.bdate.valid;
			if(!this.formState.bdate.isValid) {
				this.formState.bdate.errMsg = response.form.bdate.error;
			}

			this.formState.maritalStatus.isValid = response.form.maritalStatus.valid;
			if(!this.formState.maritalStatus.isValid) {
				this.formState.maritalStatus.errMsg = response.form.maritalStatus.error;
			}

			this.formState.sex.isValid = response.form.sex.valid;
			if(!this.formState.sex.isValid) {
				this.formState.sex.errMsg = response.form.sex.error;
			}

			this.formState.highEdu.isValid = response.form.highEdu.valid;
			if(!this.formState.highEdu.isValid) {
				this.formState.highEdu.errMsg = response.form.highEdu.error;
			}

			this.formState.school_other.isValid = response.form.school_other.valid;
			if(!this.formState.school_other.isValid) {
				this.formState.school_other.errMsg = response.form.school_other.error;
			}

			this.formState.employStatus.isValid = response.form.employStatus.valid;
			if(!this.formState.employStatus.isValid) {
				this.formState.employStatus.errMsg = response.form.employStatus.error;
			}

			this.formState.work_other.isValid = response.form.work_other.valid;
			if(!this.formState.work_other.isValid) {
				this.formState.work_other.errMsg = response.form.work_other.error;
			}
		}//end of form
	}

	//on error alert closed
	onErrorAlertClosed(res:boolean){
		if(res){
			this.serverErrorEncountered = false;
			this.serverErrorMsg = "";
		}
	}

	// process error. Indicate that something went wrong
	//and display on the form
	processError(errorMsg) {
		this.serverErrorEncountered = true;  //server encountered any error.
		this.serverErrorMsg = errorMsg;
	}
}
