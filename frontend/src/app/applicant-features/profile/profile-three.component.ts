import {Component, ViewChild} from '@angular/core';
import {NgForm} from "@angular/forms";
import {Router, ActivatedRoute, Data, Params} from '@angular/router';
import {CustomValidatorsService} from "../../services/other/custom-validators.service";
import {ApplicantOperationsService} from "../../services/backend-http/applicant-operations.service";
import {WorldCountriesServiceService} from "../../services/other/world-countries.service";

@Component({
	selector: 'company-profile-three',
	templateUrl: './profile-three.component.html',
	styleUrls: ['../../../assets/styles/profile.component.css'],
	providers:[WorldCountriesServiceService]
})
export class ApplicantProfileComponentThree {
	@ViewChild('form') form:NgForm; //basic form
	serverErrorEncountered:boolean = false;
	serverErrorMsg:string = "";

	//form state
	formState= {
		countryOrigin:{isValid:true,value:"",errMsg:"",regex:""},
		countryResidence:{isValid:true,value:"",errMsg:"",regex:""},
		city:{isValid:true,value:"",errMsg:"",regex:""},
		profileID:{value:""}
	};

	hasFormChanged:boolean = false;
	isNewForm:boolean = false;
	phase:number = 3;
	worldCountries = null;

	constructor(private router: Router, private route:ActivatedRoute,private validators:CustomValidatorsService,
					private applicantOp: ApplicantOperationsService,private wc:WorldCountriesServiceService)
	{
		this.formState.countryOrigin.regex = this.validators.wordsRegex();
		this.formState.countryOrigin.errMsg = this.validators.getValidationMsg('text');
		this.formState.countryResidence.regex = this.validators.wordsRegex();
		this.formState.countryResidence.errMsg = this.validators.getValidationMsg('text');
		this.formState.city.regex = this.validators.wordsRegex();
		this.formState.city.errMsg = this.validators.getValidationMsg('text');
	}

	ngOnInit()
	{
		//get form data from resolver
		//get applicant profile information from server
		this.route.data.subscribe(
			(data:Data) =>
			{
				let response = data['resolver'];
				//check whether the response is an array.(only errors are in arrays[false,err])
				//normal response are in response
				if(Array.isArray(response)){
					//happens when an error is encountered. resolver makes response false
					let errMsg = (response[1].name && response[1].name == "HttpErrorResponse") ?
						"Could not connect to server.": "Something went wrong.";
					//process error
					this.processError(errMsg);
				}
				else{ //no error
					this.processResponse(response);     //process response
				}
			},(err) => {
				let errMsg = (err.name && err.name == "HttpErrorResponse") ?
					"Could not connect to server.": err.statusText;
				this.processError(errMsg);
			});

		//get world-countries json
		this.wc.getWorldCountriesJSON()
			.subscribe(data=>this.worldCountries=data,error=>console.log(error));

	}//end of method

	//process response
	processResponse(response)
	{
		//1.update form with values
		//3. handle error
		if (response.response == "error") {
			this.processError(response.msg);
		}
		else if (response.response == "okay")
		{
			//check if isNewProfile
			if(response.data.other.isNewProfile){
				this.isNewForm = true;
			}
			else
			{
				this.isNewForm = false;
				//set form state(s)
				this.formState.countryOrigin.value = response.data.fetched.countryOrigin;
				this.formState.countryResidence.value = response.data.fetched.countryResidence;
				this.formState.city.value = response.data.fetched.city;
				this.formState.profileID.value = response.data.fetched.profileID;
			}//existing profile
		}//end of res.okay
	}//end method

	//update formState
	//update formState & form validity
	updateFormState(type:string)
	{
		//a form was update
		this.hasFormChanged = true;

		if(type == "countryOrigin"){
			//noinspection TypeScriptUnresolvedVariable
			this.formState.countryOrigin.isValid = this.form.controls.countryOrigin.valid;
		}
		else if(type == "countryResidence"){
			//noinspection TypeScriptUnresolvedVariable
			this.formState.countryResidence.isValid = this.form.controls.countryResidence.valid;
		}
		else if(type == "city"){
			//noinspection TypeScriptUnresolvedVariable
			this.formState.city.isValid = this.form.controls.city.valid;
		}
	}//end of method

	//skip this section
	skipForm() {
		//navigate to next phase
		this.router.navigate(["../files"],{relativeTo:this.route});
	}

	//previousForm
	previousForm() {
		//navigate to previous phase
		this.router.navigate(["../contact"],{relativeTo:this.route});
	}

	//submit form
	onSubmit()
	{
		if(this.hasFormChanged)
		{
			if (this.form.valid)
			{
				//submit forms before moving to the next phase
				let submittedForm = {
					countryOrigin: this.formState.countryOrigin.value,
					countryResidence: this.formState.countryResidence.value,
					city: this.formState.city.value,
					profileID: this.formState.profileID.value
				};

				let mode = (this.isNewForm) ? "n" : "u";

				// submit form
				this.applicantOp.submitApplicantProfileForm(submittedForm,this.phase, mode)
					.subscribe((response:any)=> {
						this.processFormSubmissionResponse(response);
					}, (error)=> {
						//happens when an error is encountered. resolver makes response false
						let errMsg = (error.name == "HttpErrorResponse") ? "Could not connect to server." :
							'Something went wrong.';
						this.processError(errMsg);
					});
			}//end of valid form
		}//form was changed
		else{
			//navigate to next phase (if form wasn't changed)
			this.skipForm();
		}//end of form not changed
	}//end of submission

	//process form submission
	processFormSubmissionResponse(response)
	{
		if(response.response == "okay")
		{
			if(response.data.other.isSaved)
			{
				if(this.isNewForm){
					this.isNewForm = false;
				}
				// navigate to next form
				this.skipForm();
			}
		}
		else if(response.response == "error"){
			this.processError(response.msg);
		}
		else if(response.response == "form")
		{
			//set form error messages and validities
			this.formState.countryOrigin.isValid = response.form.countryOrigin.valid;
			if(!response.form.countryOrigin.valid) {
				this.formState.countryOrigin.errMsg = response.form.countryOrigin.error;
			}

			this.formState.countryResidence.isValid = response.form.countryResidence.valid;
			if(!response.form.countryResidence.valid) {
				this.formState.countryResidence.errMsg = response.form.countryResidence.error;
			}

			this.formState.city.isValid = response.form.city.valid;
			if(!response.form.city.valid) {
				this.formState.city.errMsg = response.form.city.error;
			}
		}//end of form
	}

	//on error alert closed
	onErrorAlertClosed(res:boolean){
		if(res){
			this.serverErrorEncountered = false;
			this.serverErrorMsg = "";
		}
	}

	// process error. Indicate that something went wrong
	//and display on the form
	processError(errorMsg) {
		this.serverErrorEncountered = true;  //server encountered any error.
		this.serverErrorMsg = errorMsg;
	}
}
