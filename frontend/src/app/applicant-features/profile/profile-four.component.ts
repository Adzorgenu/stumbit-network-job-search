import {Component, ViewChild} from '@angular/core';
import {NgForm} from "@angular/forms";
import {Router, ActivatedRoute, Data, Params} from '@angular/router';
import {FileUploader, FileItem, FileLikeObject} from 'ng2-file-upload';
import {HttpDomainService} from "../../services/backend-http/http-domain.service";
import {ApplicantOperationsService} from "../../services/backend-http/applicant-operations.service";

@Component({
	selector: 'company-profile-four',
	templateUrl: './profile-four.component.html',
	styleUrls: ['../../../assets/styles/profile.component.css']
})
export class ApplicantProfileComponentFour {
	serverErrorEncountered:boolean = false;
	serverErrorMsg:string = "";

	public uploader:FileUploader;
	public isFormValid = false;
	public profileID:string;
	public cvFile:string;
	public degreeFile:string;
	public url:string;
	public domain:string;

	constructor(private router: Router, private route:ActivatedRoute,private dmn:HttpDomainService,
					private applicantOp: ApplicantOperationsService) {
		this.url= this.dmn.getDomain()+'/applicant/profile/files?pID=';
		this.domain = this.dmn.getDomain()+"";
	}

	ngOnInit()
	{
		//get form data from resolver
		//get applicant profile information from server
		this.route.data.subscribe(
			(data:Data) =>
			{
				let response = data['resolver'];
				//check whether the response is an array.(only errors are in arrays[false,err])
				//normal response are in response
				if(Array.isArray(response)){
					//happens when an error is encountered. resolver makes response false
					let errMsg = (response[1].name && response[1].name == "HttpErrorResponse") ?
						"Could not connect to server.": "Something went wrong.";
					//process error
					this.processError(errMsg);
				}
				else{ //no error
					this.processResponse(response);     //process response
				}
			},(err) => {
				let errMsg = (err.name && err.name == "HttpErrorResponse") ?
					"Could not connect to server.": err.statusText;
				this.processError(errMsg);
			});
	}

	//process response
	processResponse(response)
	{
		//1.update form with values
		//3. handle error
		if (response.response == "error") {
			this.processError(response.msg);
		}
		else if (response.response == "okay")
		{
			//check if isNewProfile
			if(response.data.other.isNewProfile){
				this.processError('Please fill the previous forms first before adding your files');
			}
			else
			{
				if(response.data.fetched.cert != ""){
					this.isFormValid = true;
				}

				this.profileID = response.data.fetched.profileID;
				this.cvFile = response.data.fetched.cv;
				this.degreeFile = response.data.fetched.cert;

				//set url for uploads
				this.uploader = new FileUploader({url: this.url+this.profileID});
			}//existing profile
		}//end of res.okay
	}//end method

	public onFileSelected(e,fieldName):void
	{
		// console.log(this.uploader);
		// console.log(e);
		for (let i = 0; i < this.uploader.queue.length; i++)
		{
			let f:FileItem = this.uploader.queue[i];
			//check if a previous FileItem was added to it first
			//if a fileItem with this alias exists,, remove it
			if (f.alias == fieldName && e[0].name == f.file.name) {
				this.uploader.queue[i].remove();
			}

			//change the alias from "file" to the alias passed
			if (e[0].name == f.file.name && fieldName != f.alias && f.alias=="file") {
				this.uploader.queue[i].alias = fieldName;
				break;
			}

			//reset
			f = null;
		}

		//the required one is a certificate
		if (fieldName == "cert") {
			this.isFormValid = true;
		}

		//remove any redundant field
		let foundCV = false;
		let foundDegree = false;

		//eg: for (j=10(length)-1; j>=0; j--)
		for (let j=this.uploader.queue.length-1; j>=0; j--)
		{
			//start looping from last item to first
			//so that the recent ones will not be removed but older ones
			//remove redundant cv files
			if(this.uploader.queue[j].alias == "cv" && foundCV){
				this.uploader.queue[j].remove();
			}else if(this.uploader.queue[j].alias == "cv" && !foundCV){
				foundCV = true;
			}
			//remove redundant degree files
			if(this.uploader.queue[j].alias == "cert" && foundDegree){
				this.uploader.queue[j].remove();
			}else if(this.uploader.queue[j].alias == "cert" && !foundDegree){
				foundDegree = true;
			}
		}
		//reset fields
		foundCV = null;
		foundDegree = null;
	}

	//skip this section
	public skipForm():void {
		//navigate to next phase
		this.router.navigate(["../files"],{relativeTo:this.route});
	}

	//previousForm
	public previousForm():void {
		//navigate to previous phase
		this.router.navigate(["../residence"],{relativeTo:this.route});
	}

	//submit form
	public onSubmit():void {
		let phase = 4;
		let that = this;
		this.uploader.onCompleteAll = function()
		{
			//get applicant profile information from server
			//after successfully uploading files
			that.applicantOp.getApplicantProfile(phase)
				.subscribe(
					(response) => {
						this.processResponse(response);     //process response
					},(err) => {
						let errMsg = (err.name && err.name == "HttpErrorResponse") ?
							"Could not connect to server.": err.statusText;
						this.processError(errMsg);
					});
		};
		//submit files
		// console.log(this.uploader);
		this.uploader.uploadAll();
	}

	//on error alert closed
	onErrorAlertClosed(res:boolean){
		if(res){
			this.serverErrorEncountered = false;
			this.serverErrorMsg = "";
		}
	}

	// process error. Indicate that something went wrong
	//and display on the form
	processError(errorMsg) {
		this.serverErrorEncountered = true;  //server encountered any error.
		this.serverErrorMsg = errorMsg;
	}

}
