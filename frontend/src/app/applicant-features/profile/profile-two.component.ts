import {Component, ViewChild} from '@angular/core';
import {NgForm} from "@angular/forms";
import {Router, ActivatedRoute, Data, Params} from '@angular/router';
import {CustomValidatorsService} from "../../services/other/custom-validators.service";
import {ApplicantOperationsService} from "../../services/backend-http/applicant-operations.service";

@Component({
	selector: 'company-profile-two',
	templateUrl: './profile-two.component.html',
	styleUrls: ['../../../assets/styles/profile.component.css']
})

export class ApplicantProfileComponentTwo {
	@ViewChild('form') form:NgForm; //basic form
	serverErrorEncountered:boolean = false;
	serverErrorMsg:string = "";
	isAllPhoneNoValid = true;
	//form state
	formState= {
		email:{isValid:true,value:"",errMsg:"",regex:""},
		altEmail:{isValid:true,value:"",errMsg:"",regex:""},
		phone:{isValid:true,value:"",errMsg:"",regex:""},
		altPhoneNumber:{isValid:true,value:"",regex:"",errMsg:""},
		postalAddress:{isValid:true,value:"",regex:"",errMsg:""},
		altPostalAddress:{isValid:true,value:"",regex:"",errMsg:""},
		profileID:{value:""}
	};

	hasFormChanged:boolean = false;
	isNewForm:boolean = false;
	phase:number = 2;

	constructor(private router: Router, private route:ActivatedRoute,private validators:CustomValidatorsService,
					private applicantOp: ApplicantOperationsService)
	{
		//validate phone numbers with wordRegex before using google's liphonenumber-js functions
		this.formState.email.regex = this.validators.emailRegex();
		this.formState.email.errMsg = this.validators.getValidationMsg('email');
		this.formState.altEmail.regex = this.validators.emailRegex();
		this.formState.altEmail.errMsg = this.validators.getValidationMsg('email');
		this.formState.phone.regex = this.validators.wordsRegex();
		this.formState.phone.errMsg = this.validators.getValidationMsg('phone');
		this.formState.altPhoneNumber.regex = this.validators.wordsRegex();
		this.formState.altPhoneNumber.errMsg = this.validators.getValidationMsg('phone');
		this.formState.postalAddress.regex = this.validators.wordsRegex();
		this.formState.postalAddress.errMsg = this.validators.getValidationMsg('text');
		this.formState.altPostalAddress.regex = this.validators.wordsRegex();
		this.formState.altPostalAddress.errMsg = this.validators.getValidationMsg('text');
	}

	ngOnInit()
	{
		//get form data from resolver
		//get applicant profile information from server
		this.route.data.subscribe(
			(data:Data) =>
			{
				let response = data['resolver'];
				//check whether the response is an array.(only errors are in arrays[false,err])
				//normal response are in response
				if(Array.isArray(response)){
					//happens when an error is encountered. resolver makes response false
					let errMsg = (response[1].name && response[1].name == "HttpErrorResponse") ?
						"Could not connect to server.": "Something went wrong.";
					//process error
					this.processError(errMsg);
				}
				else{ //no error
					this.processResponse(response);     //process response
				}
			},(err) => {
				let errMsg = (err.name && err.name == "HttpErrorResponse") ?
					"Could not connect to server.": err.statusText;
				this.processError(errMsg);
			});
	}

	//process response
	processResponse(response)
	{
		//1.update form with values
		//3. handle error
		if (response.response == "error") {
			this.processError(response.msg);
		}
		else if (response.response == "okay")
		{
			//check if isNewProfile
			if(response.data.other.isNewProfile){
				this.isNewForm = true;
			}
			else
			{
				this.isNewForm = false;
				//set form state(s)
				this.formState.email.value = response.data.fetched.email;
				this.formState.altEmail.value = response.data.fetched.altEmail;
				this.formState.phone.value = response.data.fetched.phone;
				this.formState.altPhoneNumber.value = response.data.fetched.altPhoneNumber;
				this.formState.postalAddress.value = response.data.fetched.postalAddress;
				this.formState.altPostalAddress.value = response.data.fetched.altPostalAddress;
				this.formState.profileID.value = response.data.fetched.profileID;
			}//existing profile
		}//end of res.okay
	}//end method

	//update formState
	//update formState & form validity
	updateFormState(type:string)
	{
		//a form was update
		this.hasFormChanged = true;

		if(type == "email"){
			//noinspection TypeScriptUnresolvedVariable
			this.formState.email.isValid = this.form.controls.email.valid;
		}
		else if(type == "altEmail"){
			//noinspection TypeScriptUnresolvedVariable
			this.formState.altEmail.isValid = this.form.controls.altEmail.valid;
		}
		else if(type == "phone"){
			//since custom validations(diff from ngModel)
			//update isFormValid
			//noinspection TypeScriptUnresolvedVariable
			this.formState.phone.isValid = this.validators.isValidPhoneNumber(this.formState.phone.value);
			this.isAllPhoneNoValid = this.formState.phone.isValid;

			//re-format number using google's liphonenumber
			if(this.formState.phone.isValid){
				this.formState.phone.value = this.validators.formatPhoneNumber(this.formState.phone.value);
			}
		}
		else if(type == "altPhoneNumber"){
			//only works validate when there's a value
			if(this.formState.altPhoneNumber.value != ""){
				//noinspection TypeScriptUnresolvedVariable
				this.formState.altPhoneNumber.isValid=this.validators.isValidPhoneNumber(this.formState.altPhoneNumber.value);
				this.isAllPhoneNoValid = this.formState.altPhoneNumber.isValid;

				//format number using google's liphonenumber
				if(this.formState.altPhoneNumber.isValid){
					this.formState.altPhoneNumber.value=this.validators.formatPhoneNumber(this.formState.altPhoneNumber.value);
				}
			}
		}
		else if(type == "postalAddress"){
			//noinspection TypeScriptUnresolvedVariable
			this.formState.postalAddress.isValid = this.form.controls.postalAddress.valid;
		}
		else if(type == "altPostalAddress"){
			//noinspection TypeScriptUnresolvedVariable
			this.formState.altPostalAddress.isValid = this.form.controls.altPostalAddress.valid;
		}
	}

	//skip this section
	skipForm() {
		//navigate to next phase
		this.router.navigate(["../residence"],{relativeTo:this.route});
	}

	//previousForm
	previousForm() {
		//navigate to previous phase
		this.router.navigate(["../basic"],{relativeTo:this.route});
	}

	//submit form
	onSubmit()
	{
		if(this.hasFormChanged)
		{
			if (this.form.valid)
			{
				//submit forms before moving to the next phase
				let submittedForm = {
					email: this.formState.email.value,
					altEmail: this.formState.altEmail.value,
					phone: this.formState.phone.value,
					altPhoneNumber: this.formState.altPhoneNumber.value,
					postalAddress: this.formState.postalAddress.value,
					altPostalAddress: this.formState.altPostalAddress.value,
					profileID: this.formState.profileID.value
				};


				let mode = (this.isNewForm) ? "n" : "u";

				//submit form
				this.applicantOp.submitApplicantProfileForm(submittedForm,this.phase, mode)
					.subscribe((response:any)=> {
						this.processFormSubmissionResponse(response);
					}, (error)=> {
						//happens when an error is encountered. resolver makes response false
						let errMsg = (error.name == "HttpErrorResponse") ? "Could not connect to server." :
							'Something went wrong.';
						this.processError(errMsg);
					});
			}//end of valid form
		}//form was changed
		else{
			//navigate to next phase (if form wasn't changed)
			this.skipForm();
		}//end of form not changed
	}//end of submission

	//process form submission
	processFormSubmissionResponse(response)
	{
		if(response.response == "okay")
		{
			if(response.data.other.isSaved)
			{
				if(this.isNewForm){
					this.isNewForm = false;
				}
				
				// navigate to next form
				this.skipForm();
			}
		}
		else if(response.response == "error"){
			this.processError(response.msg);
		}
		else if(response.response == "form")
		{
			//set form error messages and validities
			this.formState.email.isValid = response.form.email.valid;
			if(!response.form.email.valid) {
				this.formState.email.errMsg = response.form.email.error;
			}

			this.formState.altEmail.isValid = response.form.altEmail.valid;
			if(!response.form.altEmail.valid) {
				this.formState.altEmail.errMsg = response.form.altEmail.error;
			}

			this.formState.phone.isValid = response.form.phone.valid;
			if(!response.form.phone.valid) {
				this.formState.phone.errMsg = response.form.phone.error;
			}

			this.formState.altPhoneNumber.isValid = response.form.altPhoneNumber.valid;
			if(!response.form.altPhoneNumber.valid) {
				this.formState.altPhoneNumber.errMsg = response.form.altPhoneNumber.error;
			}

			this.formState.postalAddress.isValid = response.form.postalAddress.valid;
			if(!response.form.postalAddress.valid) {
				this.formState.postalAddress.errMsg = response.form.postalAddress.error;
			}

			this.formState.altPostalAddress.isValid = response.form.altPostalAddress.valid;
			if(!response.form.altPostalAddress.valid) {
				this.formState.altPostalAddress.errMsg = response.form.altPostalAddress.error;
			}
		}//end of form
	}


	//on error alert closed
	onErrorAlertClosed(res:boolean){
		if(res){
			this.serverErrorEncountered = false;
			this.serverErrorMsg = "";
		}
	}

	// process error. Indicate that something went wrong
	//and display on the form
	processError(errorMsg) {
		this.serverErrorEncountered = true;  //server encountered any error.
		this.serverErrorMsg = errorMsg;
	}
}
