declare function require(path:string);
import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'applicant-view-applied-job-details',
	templateUrl: './view-applied-job-details.component.html',
	styleUrls: ['../../../generic/styles/applicant-view-applied-job-details/applicant-view-applied-job-details.css']
})

export class ApplicantViewAppliedJobDetailsComponent implements OnInit {

    ngOnInit(){
        
    }

  jobs_logo = require('../../../assets/image/jobs-logo.jpg');

  constructor() { }

}