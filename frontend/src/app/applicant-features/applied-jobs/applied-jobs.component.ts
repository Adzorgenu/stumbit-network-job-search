declare function require(path:string);
import { Component, OnInit } from '@angular/core';

@Component({
	selector: 'applicant-applied-jobs',
	templateUrl: './applied-jobs.component.html',
	styleUrls: ['../../../generic/styles/applicant-applied-jobs/applicant-applied-jobs.css']
})

export class ApplicantAppliedJobsComponent implements OnInit {
  isFirstCollapsed: boolean = true;
  isSecCollapsed: boolean = true;
  isThirdCollapsed: boolean = true;
  isNavbarCollapsed: boolean = true;

  jobs_logo = require('../../../assets/image/jobs-logo.jpg');

  constructor() { }

  ngOnInit() {
  }
}