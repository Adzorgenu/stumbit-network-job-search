/**
 * Created by kingsley on 6/27/18.
 */
declare function require(path:string);
import { Component, OnInit } from '@angular/core';
@Component({
  selector: 'applicant-search-jobs',
  templateUrl: './search-jobs.component.html',
  styleUrls: ['../../../generic/styles/applicant-search-jobs/applicant-search-jobs.css']
})

export class ApplicantSearchJobsComponent implements OnInit {
  isFirstCollapsed: boolean = true;
  isSecCollapsed: boolean = true;
  isThirdCollapsed: boolean = true;
  isNavbarCollapsed: boolean = true;

  jobs_logo = require('../../../assets/image/jobs-logo.jpg');

  constructor() { }

  ngOnInit() {
  }
}
