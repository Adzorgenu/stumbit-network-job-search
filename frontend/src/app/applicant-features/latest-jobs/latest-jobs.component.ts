declare function require(path:string);
import { Component, OnInit } from '@angular/core';

@Component({
	selector: 'latest-jobs',
	templateUrl: './latest-jobs.component.html',
	styleUrls: ['../../../generic/styles/applicant-latest-jobs/applicant-latest-jobs.css']
})

export class ApplicantLatestJobsComponent implements OnInit {
  isFirstCollapsed: boolean = true;
  isSecCollapsed: boolean = true;
  isThirdCollapsed: boolean = true;
  isNavbarCollapsed: boolean = true;

  jobs_logo = require('../../../assets/image/jobs-logo.jpg');

  constructor() { }

  ngOnInit() {
  }
}