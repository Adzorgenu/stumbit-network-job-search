declare function require(path:string);
import { Component } from '@angular/core';
import { Router } from '@angular/router';


@Component({
	selector: 'company-dashboard-home',
	templateUrl: './dashboard-home.component.html',
	styleUrls: ['./dashboard-home.component.css']
})

export class ApplicantDashboardHomeComponent {
	jobs_logo = require('../../../assets/image/jobs-logo.jpg');

	constructor(private router: Router){}
}
