declare function require(path:string);
import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'applicant-apply-confirmation',
	templateUrl: './apply-confirmation.component.html',
	styleUrls: ['../../../generic/styles/applicant-apply-confirmation/applicant-apply-confirmation.css']
})

export class ApplicantApplyConfirmationComponent implements OnInit {

    ngOnInit(){

    }

    jobs_logo = require('../../../assets/image/jobs-logo.jpg');

    constructor(){}
}