/**
 * Created by kingsley on 6/27/18.
 */
/**
 * Created by kingsley on 6/27/18.
 */
declare function require(path:string);
import { Component, OnInit } from '@angular/core';
@Component({
  selector: 'applicant-search-jobs',
  templateUrl: './post-jobs.component.html',
  styleUrls: ['../../../generic/styles/company-post-jobs/company-post-jobs.css']
})

export class CompanyPostJobsComponent implements OnInit {
  isFirstCollapsed: boolean = true;
  isSecCollapsed: boolean = true;
  isThirdCollapsed: boolean = true;
  isNavbarCollapsed: boolean = true;

  jobRequirements = [];

  salaryCurrency: string;

  constructor() { }

  ngOnInit() {
  }
}
