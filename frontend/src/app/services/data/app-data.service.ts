import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

//This class contain every data needed for the app.
export class AppDataService {
  //everything defaults to false;
  userInfo = {
    token: "",
    accountActivated: false,
    registrationType:""
  };

  //constructor
  constructor() {
  }

  //set user token (generated from the backend)
  setUserToken(token:string) {
    this.userInfo.token = token;
  }

  //get user stored token
  getUserToken() {
    return this.userInfo.token ;
  }

  //set user verification status( whether account has been verified)
  setAccountActivatedStatus(status:boolean) {
    this.userInfo.accountActivated = status;
  }

  //get user verification status( whether account has been verified)
  getAccountActivatedStatus() {
    return this.userInfo.accountActivated ;
  }

  //set registration type of user(applicant or company)
  setUserRegistrationType(registrationType:string) {
    this.userInfo.registrationType = registrationType;
  }

  //get registration type of user(applicant or company)
  getUserRegistrationType() {
    return this.userInfo.registrationType ;
  }
}
