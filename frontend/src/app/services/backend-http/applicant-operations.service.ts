import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {HttpDomainService} from "./http-domain.service";

@Injectable()
export class ApplicantOperationsService {
	serverDomain:string;

	constructor(private  http: HttpClient,private domain:HttpDomainService){
		this.serverDomain = this.domain.getDomain()+'/applicant';
	}

	//submit profile form  to server
	submitApplicantProfileForm(formData,phase,mode) {
		//headers
		const headers = new HttpHeaders({
			'Content-Type':'application/json'
		});

		//submit information to server
		return this.http.post(this.serverDomain+'/profile?m='+mode+'&p='+phase,formData,{headers:headers});
	}

	//get basic form
	getApplicantProfile(phase) {
		//submit information to server
		return this.http.get(this.serverDomain+'/profile?p='+phase);
	}
}
