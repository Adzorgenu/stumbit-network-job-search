export class HttpDomainService {
	serverDomain:string;

  constructor() {
	  this.serverDomain = 'http://localhost:3000';
  }

	getDomain():string {
		return this.serverDomain;
	}
}
