import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {HttpDomainService} from "./http-domain.service";

// import {map} from "rxjs/operators";
// import 'rxjs/Rx';

@Injectable()
export class SignUpInService {
  serverDomain:string;

  constructor(private  http: HttpClient,private domain:HttpDomainService){
    this.serverDomain = this.domain.getDomain();
  }

  //submit signUpForm to server
  submitSignUpForm(formData)
  {
    //headers
    const headers = new HttpHeaders({
      'Content-Type':'application/json'
    });

    //submit information to server
    return this.http.post(this.serverDomain+'/signUp',formData,{headers:headers});
  }

  // resend user email if it fails.
  resendUserEmail(userID) {
    //submit information to server
    return this.http.get(this.serverDomain+'/resendEmail?id='+userID);
  }

  //check if user email exist
  userEmailExists(email) {
    //submit information to server
    return this.http.get(this.serverDomain+'/emailCheck?email='+email);
  }

  //submit signUpForm to server
  submitSignInForm(formData)
  {
    //headers
    const headers = new HttpHeaders({
      'Content-Type':'application/json'
    });

    //submit information to server
    return this.http.post(this.serverDomain+'/signIn',formData,{headers:headers});
  }

}//end of class
