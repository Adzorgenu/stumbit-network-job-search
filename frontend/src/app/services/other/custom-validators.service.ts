import { Injectable } from '@angular/core';
import {parse,format} from 'libphonenumber-js';
//noinspection TypeScriptCheckImport
import isEqual from 'lodash/isEqual';

@Injectable({
  providedIn: 'root'
})

export class CustomValidatorsService {
	//declare validations
	validationMsg: { [type: string]: string } = {
		email:"A valid email is required", //email requirement
		password:"A valid password with a minimum of 8 characters is required",  //password requirement
		registrationType:"Invalid registration type. Please select a correct field", //registration type(signUp)
		confirmPassword:"Password does not match", //confirm password for signUp
		emailExists:"Email is used by another user", //email exists (for signUp)
		name:"Provide a valid name. Special characters are not allowed",
		date:"Please select a valid date",
		select:"Please select a value",
		text:"Invalid text. Special characters are not allowed",
		phone:"Invalid phone number. Should be in the format XXX-XXX-XXXX"
	};

	constructor() {
	}

	//google's liphonenumber -s
	// parseNumber('0545878123', 'GH')
	// Outputs: { country: 'GH', phone: '0545878123' }
	gparseNumber(numb:string,countryI:any='GH'):any{
		return parse(numb,countryI);
	}

	//validate phone number --google liphonenumber
	// parseNumber('0545878123sxxx', 'GH')
	// Outputs: {}s
	isValidPhoneNumber(numb:string,countryI:any='GH'):boolean{
		//if after parsing, results is {} means it's invalid
		return !isEqual(this.gparseNumber(numb,countryI),{});
	}

	//format phone number --google liphonenumber
	//formatNumber('+12133734253', 'National')
	// Outputs: '(213) 373-4253'
	formatPhoneNumber(numb:string,c:any='GH',f:any='National'):any {
		return format(this.gparseNumber(numb,c),f);
	}

	//  define all patterns needed for validation
	passwordRegex(){
		return "^[A-Za-z0-9._@#$!%^&*()-+]+$"
	}

	emailRegex(){
		return "^[A-Za-z0-9._@#$!%^&*()-+]+$"
	}

	nameRegex(min=0,max=50) {
		//Only letters,numbers,spaces and hyphen is allowed
		//use \\ to persist with \ else it'll be ignored but may be crucial during validation
		//[\.\s] :=>[.s] , but [\\.\\s] :=> [\.\s]
		return "^[A-Za-z0-9-\\s\\.]{"+min+","+max+"}";
	}

	wordsRegex(min=0,max=50){
		//patterns
		//use \\ to persist with \ else it'll be ignored but may be crucial during validation
		//[\.\s] :=>[.s] , but [\\.\\s] :=> [\.\s]
		return "^[A-Za-z0-9-,_:@+\\(\\)\\.\\s]{"+min+","+max+"}$";
	}

	textRegex(min=0,max=100){
		//allowed characters: A-Z a-z 0-9 space(s) . - ,
		//Only letters,numbers,spaces and hyphen is allowed
		//use \\ to persist with \ else it'll be ignored but may be crucial during validation
		//[\.\s] :=>[.s] , but [\\.\\s] :=> [\.\s]
		return "/^[A-Za-z0-9-,\'\";_:@$+\\(\\)\\?\\s\\n\\t\\r\\.]${"+min+","+max+"}";
	}
	
	getValidationMsg(type:string) {
		return this.validationMsg[type];
	}

}
