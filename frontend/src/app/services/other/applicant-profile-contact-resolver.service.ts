import { Injectable } from'@angular/core';
import {Resolve,RouterStateSnapshot,ActivatedRouteSnapshot} from "@angular/router";
import {Observable, of} from "rxjs";
import { catchError } from 'rxjs/operators';
import {ApplicantOperationsService} from "../backend-http/applicant-operations.service";

@Injectable()
export class ApplicantProfileContactResolverService implements  Resolve<any>{
  	constructor(private applicantOp: ApplicantOperationsService) { }
	resolve(route:ActivatedRouteSnapshot,state:RouterStateSnapshot): Observable<any> {
		//phase:1 for basic profile
		let phase = 2;
		//get applicant profile information from server
		return this.applicantOp.getApplicantProfile(phase).pipe(
			catchError(e => {
				return of([false,e]); //indicating there was an error.
			}));//propagate error
	}
}
