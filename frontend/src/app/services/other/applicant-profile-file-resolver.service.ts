import {Injectable } from'@angular/core';
import {Resolve,RouterStateSnapshot,ActivatedRouteSnapshot} from "@angular/router";
import {Observable, of} from "rxjs";
import { catchError } from 'rxjs/operators';
import {ApplicantOperationsService} from "../backend-http/applicant-operations.service";

@Injectable()
export class ApplicantProfileFilesResolverService implements  Resolve<any>{
	constructor(private applicantOp: ApplicantOperationsService) { }
	resolve(route:ActivatedRouteSnapshot,state:RouterStateSnapshot): Observable<any> {
		//phase:3 for residence profile
		let phase = 4;
		//get applicant profile information from server
		return this.applicantOp.getApplicantProfile(phase).pipe(
			catchError(e => {
				return of([false,e]); //indicating there was an error.
			}));//propagate error
	}
}