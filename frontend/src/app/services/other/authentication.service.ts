import { Injectable } from '@angular/core';
import {AppDataService} from '../data/app-data.service';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  authenticated:boolean = false;

  //constructor
  constructor(private appData:AppDataService) {
  }

  authenticateUser() {
    let token = this.appData.getUserToken();
    if (token != ""){
      this.authenticated = true;

      // store jwt token in local storage to keep user logged in between page refreshes
      localStorage.setItem('stumbit-job-user-token',token);
    }
  }

  logout() {
    this.appData.setUserToken("");
    this.authenticated = false;

    // remove jwt token in local storage
    localStorage.removeItem('stumbit-job-user-token');
  }

  isAuthenticated() {
    return this.authenticated;
  }
}
