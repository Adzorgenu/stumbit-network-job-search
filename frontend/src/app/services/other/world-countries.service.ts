import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class WorldCountriesServiceService {
	constructor(private  http: HttpClient){
	}

	public getWorldCountriesJSON() {
		// country-by-name.json in assets dir
		return this.http.get("assets/country-by-name.json");
	}
}
