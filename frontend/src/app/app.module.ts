//core modules
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {Routes, RouterModule} from "@angular/router";
import {FormsModule,ReactiveFormsModule } from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";

//ngx bootstrap components
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { ModalModule } from 'ngx-bootstrap/modal';
import { AlertModule } from 'ngx-bootstrap/alert';
import { CustomFormsModule } from 'ng4-validators';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ProgressbarModule } from 'ngx-bootstrap/progressbar';

// form tags
import { TagInputModule } from 'ngx-chips';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

//components
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { CompanyDashboardComponent } from './dashboard/company/company.component';
import { ApplicantDashboardComponent } from './dashboard/applicant/applicant.component';
import { CompanyProfileComponent } from './company-features/profile/profile.component';
import { CompanyDashboardHomeComponent } from './company-features/dashboard-home/dashboard-home.component';
import { ApplicantDashboardHomeComponent } from './applicant-features/dashboard-home/dashboard-home.component';
import { ApplicantSearchJobsComponent } from './applicant-features/search-jobs/search-jobs.component';
import { ErrorAlertComponent } from './generic/error-alert/error-alert.component';
import { CompanyPostJobsComponent } from './company-features/post-jobs/post-jobs.component';
import { SignInComponent } from './home/sign-in/sign-in.component';
import { SignUpComponent } from './home/sign-up/sign-up.component';
import { ApplicantAppliedJobsComponent } from './applicant-features/applied-jobs/applied-jobs.component';
import { ApplicantViewAppliedJobDetailsComponent } from './applicant-features/view-applied-job-details/view-applied-job-details.component';
import { ApplicantApplyConfirmationComponent } from './applicant-features/apply-confirmation/apply-confirmation.component';
import {ApplicantProfileComponentOne} from "./applicant-features/profile/profile-one.component";
import {ApplicantProfileComponentTwo} from "./applicant-features/profile/profile-two.component";
import {ApplicantProfileComponentThree} from "./applicant-features/profile/profile-three.component";
import {ApplicantProfileComponentFour} from "./applicant-features/profile/profile-four.component";
import { ApplicantLatestJobsComponent } from './applicant-features/latest-jobs/latest-jobs.component';

//service(s)
import {SignUpInService } from './services/backend-http/sign-up-in.service';
import {AppDataService} from './services/data/app-data.service';
import {AuthenticationService} from './services/other/authentication.service';
import {CustomValidatorsService} from './services/other/custom-validators.service';
import {ApplicantOperationsService} from "./services/backend-http/applicant-operations.service";
import {HttpDomainService} from "./services/backend-http/http-domain.service";

//resolvers
import {ApplicantProfileBasicResolverService} from "./services/other/applicant-profile-basic-resolver.service";
import {ApplicantProfileContactResolverService} from "./services/other/applicant-profile-contact-resolver.service";
import {ApplicantProfileResidenceResolverService} from "./services/other/applicant-profile-residence-resolver.service";
import {ApplicantProfileFilesResolverService} from "./services/other/applicant-profile-file-resolver.service";

//modules
import { LoadingBarHttpClientModule } from '@ngx-loading-bar/http-client';
import { LoadingBarRouterModule } from '@ngx-loading-bar/router';
import { FileUploadModule } from 'ng2-file-upload';

//other
import {AppRoutingModule} from "./app-routing.module";

@NgModule({
  declarations: [
	  AppComponent,
	  HomeComponent,
	  CompanyDashboardComponent,
	  ApplicantDashboardComponent,
	  CompanyProfileComponent,
	  ApplicantProfileComponentOne,
	  CompanyDashboardHomeComponent,
	  ApplicantDashboardHomeComponent,
	  ApplicantSearchJobsComponent,
	  ErrorAlertComponent,
	  CompanyPostJobsComponent,
	  SignInComponent,
	  SignUpComponent,
	  ApplicantAppliedJobsComponent,
	  ApplicantViewAppliedJobDetailsComponent,
	  ApplicantApplyConfirmationComponent,
	  ApplicantProfileComponentTwo,
	  ApplicantProfileComponentThree,
	  ApplicantProfileComponentFour,
	  ApplicantLatestJobsComponent
  ],
  imports: [
	  BrowserModule,
	  ProgressbarModule.forRoot(),
	  BsDropdownModule.forRoot(),
	  CollapseModule.forRoot(),
	  ModalModule.forRoot(),
	  AlertModule.forRoot(),
	  BsDatepickerModule.forRoot(),
	  AppRoutingModule,
	  FormsModule,
	  HttpClientModule,
	  ReactiveFormsModule,
	  TagInputModule,
	  BrowserAnimationsModule,
	  CustomFormsModule,
	  LoadingBarRouterModule,
	  LoadingBarHttpClientModule,
	  FileUploadModule
  ],
  providers: [
	  SignUpInService,
	  HttpDomainService,
	  AppDataService,
	  AuthenticationService,
	  CustomValidatorsService,
	  ApplicantOperationsService,
	  ApplicantProfileBasicResolverService,
	  ApplicantProfileContactResolverService,
	  ApplicantProfileResidenceResolverService,
	  ApplicantProfileFilesResolverService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
