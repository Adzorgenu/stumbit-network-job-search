import { Component, OnInit } from '@angular/core';

@Component({
	selector: 'applicant-dashboard',
	templateUrl: './applicant.component.html',
	styleUrls: ['../../../generic/styles/dashboard/dashboard-styles.css']
})

export class ApplicantDashboardComponent implements OnInit {
	isFirstCollapsed: boolean = true;
	isSecCollapsed: boolean = true;
	isThirdCollapsed: boolean = true;
	isNavbarCollapsed: boolean = true;

	constructor() { }

	ngOnInit() {
	}
}
