import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
	selector: 'app-company-dashboard',
	templateUrl: './company.component.html',
	styleUrls: ['../../../generic/styles/dashboard/dashboard-styles.css']
})

export class CompanyDashboardComponent implements OnInit {
	isFirstCollapsed: boolean = true;
	isSecCollapsed: boolean = true;
	isThirdCollapsed: boolean = true;
	isNavbarCollapsed: boolean = true;

	constructor(private router: Router) { }

	redirect(){
		console.log('redirecting..')
		this.router.navigate(['profile'])
	}
	
	ngOnInit() {
	}
}
