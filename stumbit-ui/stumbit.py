from flask import Flask, render_template

app = Flask(__name__)


@app.route('/search-jobs')
def search_jobs():
    return render_template('search-jobs.html')

@app.route('/post-jobs')
def post_jobs():
    return render_template('post-jobs.html')


if __name__ == '__main__':
    app.run()
