--------------------------------------------------
---@ General Information (Directory Structure) ---
--------------------------------------------------
==private: contains all necessary back-end files/codes.sub folders are: classes,configuration,functions and routes.

	++classes: contains various classes/objects for different back-end activities. Files in this folder are authentication.js,dbLogger.js,emailClass.js,jobPosting.js,signInClass.js and signUpClass.js

	++configuration:contains the app configurations and database connection.Files in this directory are config.js and db_connection. Config.js contains the server's configuration while db_connection as the name suggest connects to the database.

	++functions: This directory contains a generic functions file used in the app.Codes are used by all sectors

	++routes: contains all routes defined in my application

==sessionStore: this is where all sessions info are stored.

== package.json: contains all installed dependencies/libraries

==public: everything public goes here. sub folders are accounts,applicant,company,lib and templates

  ++accounts: contains files needed to successfully register and verify a user.

  ++applicant: nothing there for now.

  ++company:has contains jobPosting files.

  ++lib: contains css and javascript files for my application

  ++templates: email templates to be used by emailClass for sending emails.

==server.js: application init file

-----------------------------------
--- @end of General Information ---
-----------------------------------

---------------------------
--@ Likely editable files --
---------------------------
Possible Files to be used during integration are 
/private/configuration/config.js,
/private/configuration/db_connection.js
/private/classes/emailClass.js



-----------------------------------
--- @Server and Database config ---
-----------------------------------
SERVER::
  Port in use:3000

DATABASE::
 Username: bossu
 Password: nokofioGH
 Database Name: stumbitDB
 Port: 27017

 (info will be found in private/configuration/db_connection.js)
-----------------------------------------
---@end of Server and Database config ---
-----------------------------------------


-----------------
--- @ Testing ---
----------------- 
 Note: please prefix url with localhost:3000
 Requests               Action 
 / or /signIn           Render Sign in page
 /accounts/signUp		Render sign up page
 /company/jPost			Render company's job posting page
 /logout				Logout of the application



------------------------
--- @ Other Comments ---
------------------------
I still have to improve the company job posting ui.
Other outstanding works (like applying to a job,getting the list of available jobs etc) haven't been dealt with.
Error handling routes/codes should be in place (in production mode)
Will be replacing native promises with bluebird's promises(due to efficiency).

-------------------------------
--- @ end of Other Comments ---
-------------------------------