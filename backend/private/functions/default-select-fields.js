module.exports =
{
	registrationTypes(){
		return ["applicant","company"];
	},
	jobTypes() {
       //list of available job types set by the system - job section
		return ["full","part"];
	},
	maritalStatuses(){
        //list of valid marital statuses for frontend applicant profile
		return ["single","married","divorced"];
	},
	maritalStatusesObject(){
        //list of valid marital statuses for frontend applicant profile
		return [{val:"single",dat:"Single"},{val:"married",dat:"Married"},{val:"divorced",dat:"Divorced"}];
	},
	sexTypes(){
        //list of available sexes f:female,m:male,o:other - applicant profile
		return ["f","m","o"];
	},
	sexTypesObject(){
        //list of available sexes f:female,m:male,o:other - applicant profile
		return [{val:"f",dat:"Female"},{val:"m",dat:"Male"},{val:"o",dat:"Other"}];
	},
    educationLevel(){
    	//education level for frontend applicant profle
    	return ["phd","masters","bachelors","diploma,senior_high","junior_high","other"];
 	},
    educationLevelObject(){
    	//education level for frontend applicant profle
    	return [ {val:"phd",dat:"PhD"},{val:"masters",dat:"Masters Degree Holder"},
    	{val:"bachelors",dat:"Bachelors Degree Holder"}, {val:"diploma",dat:"Diploma Degree Holder"},
    	{val:"senior_high",dat:"Senior High School Leaver"},
    	{val:"junior_high",dat:"Junior High School Leaver"},{val:"other",dat:"Other"}];
 	},
	currencyTypes(){
        //acceptable currencies for job-section
 	    return ["GHC","$","£","€","₣","¥","₹"];		
	},
	employmentStatus(){
		return  ["employed","unemployed","other"];
	},
	employmentStatusObjects(){
		return  [{val:"employed",dat:"Employed"},
		{val:"unemployed",dat:"Unemployed"},{val:"other",dat:"Other"}];
	}
};