//construct response json
class FrontEndResponse {
    constructor(){
        //nothing
    }

    //get new response
    getNewFrontEndResponse()
    {
        //construct and return a response object
        return this.constructNewResponse();
    }

    //construct a response object
    //response will be sent back to the front-end
    //08-01-2018
    constructNewResponse()
    {
        //all server statutes will be 200 regardless of the server's output
        //but the the response status will be dependent on the actual server's output

        //formats to be used
        //for validation errors: response type will form and the message will be validation failed the form
        //The form property will carry the form properties with their corresponding validation msgs
        //The form properties will have three fields:value,isValid, and validity message

        //For internal server error:
        //the response will have the value "error" and a message "internal server error"

        //A third format for the response is "okay"
        //indicating that everything is working well

        let custResponse = {
            response:null,
            status:null,
            msg:null,
            form:null,
            data:{
                token:null,
                path:null,
                fetched:null,
                other:null
            }
        };

        return custResponse;
    }
}

// let response = {
//     response:null,
//     status:null,
//     msg:null,
//     form:null ,
//     data:{
//         token:null,
//         path:null,
//         fetched:null,
//         other:null
//     }
// };

// {response:null,status:null,msg:null,form:null ,data:{token:null,path:null,fetched:null,other:null}}
// module.exports = response;
module.exports = FrontEndResponse;
