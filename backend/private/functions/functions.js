var _ = require("lodash");
var defaultTypes = require('./default-select-fields');
var moment = require('moment');
var phoneUtil = require('google-libphonenumber').PhoneNumberUtil.getInstance();

module.exports =
{
    formatJSON: function(info,error) {
        return {msg:info,err:error}
    },
    //isString
    isString(param){
        return(_.isString(param));
     },
     //isArray
    isArray(param){
        return(_.isArray(param));
     },
    validateEmail(email){
        //only validate fields that are not null
        if(!_.isNull(email)){
            //html provides that functionality for us
            return (email.match(/^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,}$/)) ? true:false;
        }
        return false;
     },
     validatePhone(number,country="GH"){
        //use google's phoneNumber validator
        try{
            let isValid = phoneUtil.isValidNumber(phoneUtil.parse(number, country));
            return isValid;
        }catch(e){
            return false;
        }

     },
    validatePassword(password){
        //only validate fields that are not null
        if(!_.isNull(password)){
            //I also modified the regex for the password
            //old: return (password.match(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/)) ? true:false;
            return ((password.length >= 8) && (password.match(/^[A-Za-z0-9._@#$!%^&*()-+]+$/)) ) ? true:false;
        }
        return false;
     },
    compare(oldVal,newVal){
        return (oldVal === newVal);
     },
    username(userName){
        //accepts alphabets,numbers and . _ @ # /
        return (userName.match(/^[A-Za-z0-9._@#\/]+$/)) ? true:false;
     },
    validateRetypePassword: function(password,retypedPass) {
        //only validate fields that are not null
        if(!_.isNull(retypedPass)){
            return (this.validatePassword(retypedPass) && this.compare(password,retypedPass)) ? true:false;
        }
        return false;
       },
    validateRegistrationType(value) {
         //only validate fields that are not null
        if(!_.isNull(registrationType)){
            let allowed = defaultTypes.registrationTypes();
            let exists = false;

            //loop through types
            for(let i=0; i<allowed.length; i++){
                if(value == allowed[i]){
                    exists = true;
                    break;
                }
            }
            return exists;
        }
         return false;
     },
    isWord(word,min=0,max=50){
        //no spaces
       //allowed characters: A-Z a-z 0-9
        return (word.match(/^[A-Za-z0-9-\.\s]$/)) ? true:false;
    },
    isValidWord(word,min=0,max=50){
        //no spaces
       //allowed characters: A-Z a-z 0-9
        return (word.match(/^[A-Za-z0-9-\.\s]+$/)) ? true:false;
    },
    isWords(word,min=0,max=50){
        //allowed characters: A-Z a-z 0-9 space(s) . - ,
        //Only letters,numbers,spaces and hyphen is allowed
        return (word.match(/^[A-Za-z0-9-,'";_:@$\(\)\?\s\n\t\r\.]+$/)) ? true:false;
    },
    isValidWords(word,min=0,max=50){
        //allowed characters: A-Z a-z 0-9 space(s) . - ,
        //Only letters,numbers,spaces and hyphen is allowed
        return (word.match(/^[A-Za-z0-9-,_:@\(\)\s\.]+$/)) ? true:false;
    },
    isValidText(word,min=0,max=50){
        //allowed characters: A-Z a-z 0-9 space(s) . - ,
        //Only letters,numbers,spaces and hyphen is allowed
        return (word.match(/^[A-Za-z0-9-,'";_:@$\(\)\?\s\n\t\r\.]+$/)) ? true:false;
    },
    validateObjectId(id){
        //validate document id
        //eg: 5997830ae2f73fa7127cee01
        return (id.match(/^[0-9a-fA-F]{24}$/)) ? true:false;
     },
    validateJobTypes: function(value) 
    {
        //This function validates jobTypes currently set by the system
        let allowed = defaultTypes.jobTypes();
        let exists = false;

        //loop through types
        for(let i=0; i<allowed.length; i++){
            if(value == allowed[i]){
                exists = true;
                break;
            }
        }
        return exists;
    },
    validateMaritalStatus:function(value)
    {
        let allowed = defaultTypes.maritalStatuses();
        let exists = false;

        //loop through types
        for(let i=0; i<allowed.length; i++){
            if(value == allowed[i]){
                exists = true;
                break;
            }
        }
        return exists;
    },
    validateSex:function(value)
    {
        let allowed = defaultTypes.sexTypes();
        let exists = false;

        //loop through types
        for(let i=0; i<allowed.length; i++){
            if(value == allowed[i]){
                exists = true;
                break;
            }
        }
        return exists;
    },
    validateHighEdu(value)
    {
        let allowed = defaultTypes.educationLevel();
        let exists = false;

        //loop through types
        for(let i=0; i<allowed.length; i++){
            if(value == allowed[i]){
                exists = true;
                break;
            }
        }
        return exists;
    },
    validateEmploymentStatus(value)
    {
        let allowed = defaultTypes.employmentStatus();
        let exists = false;

        //loop through types
        for(let i=0; i<allowed.length; i++){
            if(value == allowed[i]){
                exists = true;
                break;
            }
        }
        return exists;
    },
    validateCurrency: function(value) 
    {
        let allowed = defaultTypes.currencyTypes();
        let exists = false;
        
        //loop through types
        for(let i=0; i<allowed.length; i++){
            if(value == allowed[i]){
                exists = true;
                break;
            }
        }
        return exists;
    },
    validateNumberRange: function(value) {
        return (value.match(/^[0-9-,\s]+$/g)) ? true:false;
    },
    validateDate: function(date){
        //known format for submission dd/mm/yyyy or yyyy-mm-dd
        return moment(date,["YYYY-MM-DD","YYYY-M-D","YYYY-MM-DD"],true).isValid();
    }
};

