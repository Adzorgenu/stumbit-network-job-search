//Define routes for application
module.exports = function(app, express)
{
    let appRoutes = express.Router();
    
    //require and  use the signUp router
    const signUpRouter = require("./accounts/signUp")(app,express);
    appRoutes.use("/accounts",signUpRouter);

    //require and  use signIn router
    const signInRouter = require("./accounts/signIn")(app,express);
    appRoutes.use("/accounts",signInRouter);

    //require and  use change password router
    const changePassword = require("./accounts/changePassword");
    appRoutes.use("/accounts",changePassword);
    
    //Logout router
    const logoutRouter = require("./accounts/logout");
    appRoutes.use(logoutRouter);


    //middleware

    const applicantProfileUpdateRouter = require('./applicant/applicant-profile')(app,express);
    app.use('/applicant', applicantProfileUpdateRouter);

    // Jerr
    // var jobPosting = require("./private/routes/company/jobPosting");
    // app.use("/company",jobPosting);
    // var seedJobPostingsRoute = require("./private/routes/seed/seed-job-postings-route");
    // app.use("/seed-job-postings", seedJobPostingsRoute);
    // var seedUserRegistrationRoute = require("./private/routes/seed/seed-user-registration-route");
    // app.use("/seed-user-registration", seedUserRegistrationRoute);
    // var seedCompanyDetailsRoute = require("./private/routes/seed/seed-company-details-route");
    // app.use("/seed-company-details", seedCompanyDetailsRoute);
    // var seedApplicantDetailsRoute = require("./private/routes/seed/seed-applicant-details-route");
    // app.use("/seed-applicant-details", seedApplicantDetailsRoute);
    // var applicantListJobsRoute = require("./private/routes/applicant/applicant-list-jobs-route");
    // app.use("/applicant-list-jobs", applicantListJobsRoute);
    // var applicantMyJobsRoute = require("./private/routes/applicant/applicant-my-jobs-route");
    // app.use("/applicant-my-jobs", applicantMyJobsRoute);
    // var companyListJobsRoute = require("./private/routes/company/company-list-jobs-route");
    // app.use("/company-list-jobs", companyListJobsRoute);
    // var jobApplication = require("./private/routes/applicant/job-application-route");
    // app.use("/job-application", jobApplication);

    // // Biney
    // var applicantProfileUpdateRouter = require('./private/routes/applicant/applicantProfileUpdateRouter');
    // var applicantFileUploadRouter = require('./private/routes/applicant/applicantFileUploadRouter');
    // var adRouter = require('./private/routes/ad/ad_router');
    // var companyProfileUpdateRouter = require('./private/routes/company/companyProfileUpdateRouter');
    // var companyFileUploadRouter = require('./private/routes/company/companyFileUploadRouter');

    // app.use('/ad', adRouter);
    // app.use('/companyProfileUpdatePage', companyProfileUpdateRouter);
    // app.use('/uploadCompanyLogo', companyFileUploadRouter);
    // app.use('/applicantProfileUpdatePage', applicantProfileUpdateRouter);
    // app.use('/uploadApplicantFiles', applicantFileUploadRouter); 


    //return app routes
    return appRoutes;
};
