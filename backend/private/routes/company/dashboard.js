var express = require("express");
var router = express.Router();

//export router
module.exports = router;


/*
 * Redirecting to company dashboard
 */
router.get("/company/dashboard",function (req,res,next) {
    res.render("company/dashboard");
});
