var bodyParser = require("body-parser");
var express = require("express");
var router = express.Router();
var functions = require("../../functions/functions");

router.use(bodyParser.urlencoded({ extended:true}));

var companyJobPosting = require('../../models/job-postings-model');
var userRegistration = require('../../models/user-registration-model');
var applicantDetails = require('../../models/applicant-details-model');
var companyDetails = require('../../models/company-details-model');

module.exports = router;

router.get('/', function(req, res, next){
    // var applicant_id = req.session.id;
    var applicant_id = "";
    var locals = {};
    companyJobPosting.find(function(err, companyJobPostings) {
      locals.companyJobPostings = companyJobPostings;
      userRegistration.find(function(err, userRegistrations) {
        locals.userRegistrations = userRegistrations;
        applicantDetails.find(function(err, applicantDetails) {
          locals.applicantDetails = applicantDetails;
          companyDetails.find(function(err, companyDetails) {
            locals.companyDetails = companyDetails;
            res.render('company/company-list-jobs', { locals });
          });
        });
      }).populate('jobs');
    }).populate('companyId');
});


// Delete Routes
router.post('/delete-job-posting', function(req, res, next) {
  var id = req.body.id;
  companyJobPosting.findByIdAndRemove(id).exec();
  res.redirect('/applicant-list-jobs');
});

router.post('/delete-user-registration', function(req, res, next) {
  var id = req.body.id;
  userRegistration.findByIdAndRemove(id).exec();
  res.redirect('/applicant-list-jobs');
});

router.post('/delete-applicant-details', function(req, res, next) {
  var id = req.body.id;
  applicantDetails.findByIdAndRemove(id).exec();
  res.redirect('/applicant-list-jobs');
});

router.post('/delete-company-details', function(req, res, next) {
  var id = req.body.id;
  companyDetails.findByIdAndRemove(id).exec();
  res.redirect('/applicant-list-jobs');
});