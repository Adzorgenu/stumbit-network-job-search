var express = require('express');// won't u call express dude? ----then what u gonna do then? call me?-- :)
var mongoose = require('mongoose');// redundant over here ATM
var bodyparser = require('body-parser');
var multer = require('multer');
var hbs = require('hbs');

var Companies = require('../../models/companyReg-schema');// calls the companyReg-schema which holds the schema for our mongodb collection(table)


var app = express();
var companyProfileUpdateRouter = express.Router();
companyProfileUpdateRouter.use(bodyparser.urlencoded({extended:false}));


companyProfileUpdateRouter.route('/')
        

// register a new company
.post(function(req, res, next){
  
  //console.log("The company name is: " + req.body.companyName);
  var email = req.body.email;
  var pwd = req.body.password;
  var conf_pwd = req.body.confirm_password;
  var phone = req.body.phone;
  var alt_phone = req.body.alt_phone;

  if(pwd.length > 0){
    var re = /^(?=.*\d)(?=.*[A-Za-z])[0-9A-Za-z!@#$%]{8,}$/;
    if(!(re.test(pwd))){
      console.log('Password must be at least 8 characters long and must contain at lest 1 uppercase letter, 1 lowercase letter, 1 number and an optional special character');
      return false;
		}
  }
              
  if(pwd != conf_pwd) {
    console.log('Password and its confirmation must match!');
    return false;
  } else {
    Companies.create(req.body, function(err, client){
      if(err) throw err;
      var id = client._id;
      userRegistration.findById(req.session.userId, function(err, doc) {
        if (err) {
          console.error('error, no entry found');
        }
        doc.company_details = client._id;
        doc.save();
      });
    });
  }   
})


.get(function(req, res){
  
    Companies.findOne({
      companyName : "Stumbit Incorporated"
    }, function(err, results){
      
       if(err) throw err;
       
      
       if(!results){
         
          res.render('company/companyProfileUpdate.hbs', {
            errorMessage : ""
          });

       }else{
         
       res.render('company/companyProfileUpdate.hbs', {
         
         companyName : results.companyName,
         sector : results.sector,
         category : results.category,
         location : results.region_of_location,
         address : results.address,
         alternate_address : results.alt_address,
         website : results.website,
         email : results.email,
         phone : results.phone,
         alternate_phone : results.alt_phone,
         description : results.description
         
       });
       
     }
 
   });
  
});

module.exports = companyProfileUpdateRouter;