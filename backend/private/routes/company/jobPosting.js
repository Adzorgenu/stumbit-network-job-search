/*
 * Require necessary dependencies,frameworks and libraries
 * :Mongoose,bodyParser,express
 * Require the functions file.File has generic functions to be used in the app
 */
var bodyParser = require("body-parser");
var express = require("express");
var router = express.Router();
var fn = require("../../functions/functions");
var jobPostModule = require("../../classes/jobPosting");

router.use(bodyParser.urlencoded({ extended:true}));

    /*
    * Render jobPosting page
    */
    router.get('/jPost', function(req, res,next)
    {
            //if a jobId was submitted together with and edit qs
        var editJ = req.query.edit;
        var jobId = req.query.jpId;

        // neccessary vars
        let info = {};
        let error = {};

                //edit has a jobId and edit=t
        if(editJ == 't')
        {
            //validate jobId
            if(fn.validateObjectId(jobId))
            {
                var userId = req.session.userId;
                var jobPostClass = new jobPostModule(userId,jobId);

                    //fetch first part of information
                jobPostClass.getJobInformationOnly(function(results,err)
                {
                    if(!err.response)
                    {
                        info = {jobTitle:results.jobTitle,jobType:results.jobType,
                            jobValidity:results.jobValidity,jobAddSal:results.jobSalary[0].addSalary,
                            jobSalaryCurr:results.jobSalary[0].salaryCurrency,jobSalary:results.jobSalary[0].salaryAmount,
                            jobDesc:results.jobDescription,jobId:jobId,edit:'t'}

                        // error is an empty object
                        // return json as a response
                        res.json(fn.formatJSON(info,error))
                    }
                    else
                    {
                        //Perform error handling ooperations
                        
                        // error is an empty object
                        // return json as a response
                        res.json(fn.formatJSON(info,error))
                    }

                });
            }
            else
            {
                //perform error handling
                //unauthorized/unacceptable object Id
                res.json(fn.formatJSON(info,error))
            }
        }
        else
        {
            //set error messages to empty
            //return an okay json #info,error:empty
            res.json(fn.formatJSON(info,error));
        }

    });


//on post of the job posting form
router.post('/jPost',function (req,res,next)
{
    // neccessary vars
    let info = {};
    let error = {};

        //get jobId and edit if there are
    //if a jobId was submitted together with and edit qs
    var editJ = req.query.edit;
    var jobId = req.query.jpId;

    //get form field;
    var jTitle    = req.body.jobTitle;
    var jType     = req.body.jType;
    var jAddSal   = req.body.jAddSal;
    var jSalCurr  = req.body.jSalCurr;
    var jsalary   = req.body.jsalary;
    var jValidity = req.body.jValididty;
    var jDesc     = req.body.jDesc;

    //validate field;
    var validJTitle    = (fn.validateWords(jTitle))                            ? true  : false;
    var validJType     = (fn.validateJobTypes(jType))                          ? true  : false;
    var validJSalCur   = (fn. validateCurrency(jSalCurr))                      ? true  : false;
    var validJSalary   = (fn.validateNumberRange(jsalary))                     ? true  : false;
    var validJAddSal   = (jAddSal == "true" && (!validJSalCur || !validJSalary)) ? false : true;
    var validJValidity = (fn.validateDate(jValidity))                          ? true  : false;
    var validJDesc     = (fn.validateWords(jDesc))                             ? true  : false;

      //make job salary currency and salary/range "" if add is false
      if(jAddSal == "false")
      {
          jsalary = "";
          jSalCurr = "";
      }

      //continue to next phase
      if(validJTitle && validJType && validJAddSal && validJValidity && validJDesc)
      {
          //put data into a json/object format
          var jobData = {
              jTitle: jTitle,
              jType: jType,
              jValidity: jValidity,
              jDesc: jDesc,
              jAddSal: jAddSal,
              jsalary: jsalary,
              jSalCurr: jSalCurr
          };

          var userId = req.session.userId;
          var jobPostClass = new jobPostModule(userId,jobId);

                //editing an already posted job
            if(editJ == 't')
            {
                //insert record
                jobPostClass.updateJobRequirements(jobData,function(data,err)
                {
                    if(!err.response)
                    {
                        //redirect to jpostRequirements
                        //this time I'm including the edit param too.
                        // var url = "jPostR?jpId="+jobId+"&edit=t";
                        info = data;
                        res.json(fn.formatJSON(info,error));
                    }
                    else
                    {
                        //redirect to an error handler.
                        //jump to one of the error handling routes
                        res.json(fn.formatJSON(info,error));
                    }
                });
            }
                    //submitting a new job post
            else
            {
                //insert record
                jobPostClass.addNewBasicJobPost(jobData,function(data,err)
                {
                    if(!err.response)
                    {
                        var jobID = data;

                        //redirect to jpostRequirements
                        // var url = "jPostR?jpId="+jobID;
                        res.json(fn.formatJSON(info,error));
                    }
                    else
                    {
                        //error handling.
                        res.json(fn.formatJSON(info,error));

                    }
                });

            }

      }
      else
      {
         //return data with error messages.
         //set error messages
         var errTitle   = (!validJTitle)    ? "These characters {!@#$%^&*()+=<>?/|} are not allowed" : "";
         var errJType   = (!validJType)     ? "Invalid selection"                                    : "";
         var errJAddSal = (!validJAddSal)   ? "There was a problem"                                  : "";
         var errJVal    = (!validJValidity) ? "Incorrect Date"                                       : "";
         var errJDesc   = (!validJDesc)     ? "These characters {!@#$%^&*()+=<>?/|} are not allowed" : "";

        //  define error object
        error = {errTitle:errTitle,errJType:errJType,errJAddSal:errJAddSal,errJVal:errJVal,
             errJDesc:errJDesc}
      }

});

/*
 * Render jobPosting requirements page
 */
router.get('/jPostR', function(req, res,next)
{
    var jobId = req.query.jpId;
    var jEdit = req.query.edit;

    //validate jobId
    if(func.validateObjectId(jobId))
    {

        if(jEdit == 't')
        {
            //get job requirement 

            var userId = req.session.userId;
            var jobPostClass = new jobPostModule(userId,jobId);

            //fetch job requirements
            jobPostClass.getJobRequirements(function(results,err)
            {
                if(!err.response)
                {
                    var myResults = jobPostClass.formatJobRequirement(results);

                    //render page with data in it.
                    //    res.render("company/jPostR",{jobId:jobId,advancedReqSize:myResults[0].advancedReqSize,
                    //        advRequirements:myResults[1],basicRequirements:myResults[2]
                    //      });
                    res.json({jobId:jobId,advancedReqSize:myResults[0].advancedReqSize,
                        advRequirements:myResults[1],basicRequirements:myResults[2]
                        });
                }
                else
                {
                    //redirect to an error handler.
                    //jump to one of the error handling routes

                }

            });

        }
        else
            {
                // //set error messages to empty
                // res.render("company/jPostR",{jobId:jobId});

                //set error messages to empty
                res.json({err});
            }
    }
    else
    {
        //unauthorized/unacceptable object Id
        //redirect to an error handler.
        //jump to one of the error handling routes specifically a badInput Error

    }

});

// router.use(function(req,res,next){
//     console.log("incoming request:"+ req.url)
//     next();
// });

/*
 * Handle posting of a job requirement(s)
 * Both basic and advanced
 */
router.post('/jPostR',function(req,res,next)
{
    //require lodash
    var _ = require("lodash");

    var jobId = req.query.jpId;

    //get form fields
    //get basic/advanced requirement field
    //put basic requirement into an object form;
    //do same for advanced requirement;

    //note:all off fields are not added to the database. on those with a value of "on" are added

    var basicObject = [];
    var advancedObject = [];

         //loop through the req.body object
        //first parameter is req.body
        var numb = null;
        var corAccept = null;

    _.forEach(req.body,function(value,key)
    {
            //basic requirements. advanced req fields have _ in them hence its use here.
        if(key.indexOf('_') == -1)
        {
            if(value == "on")
              basicObject.push({fieldName:key});
        }
        else
        {
            //i'm only intersted in the field names. not field types

           if(key.match(/field_/g))
           {
               //get the numeric part of the field_number
               numb = key.slice(key.indexOf('_')+1);
               corAccept = _.get(req.body, "fType_"+numb);

                    //push object
                advancedObject.push({fieldName:value,acceptedType:corAccept});
           }
        }

    });


    var userId = req.session.userId;
    var jobPostClass = new jobPostModule(userId,jobId);

          //add job requirements
        jobPostClass.addJobRequirements(basicObject,advancedObject,function(success,err)
        {
                if(!err.response)
                {
                   //redirect to jpost final
                    var url = "jPostF?jId="+jobId;

                    res.redirect(url);
                }
                else
                {
                    //redirect to an error handler.
                    //jump to one of the error handling routes
                    
                }
        });
});

/*
 * Render jobPosting summary page
 */
router.get('/jPostF', function(req, res,next)
{

    var jobId = req.query.jId;

        //validate jobId
      if(func.validateObjectId(jobId))
      {

                var userId = req.session.userId;
                var jobPostClass = new jobPostModule(userId,jobId);

                 jobPostClass.getJobInformation(function(results,err)
                 {
                    if(!err.response)
                    {
                        var myResults = jobPostClass.formatResult(results);

                            //get first five fields
                        res.render("company/jPostF",{jobTitle:myResults[0].jobTitle,jobType:myResults[1].jobType,
                            jobValidity:myResults[2].jobValidity,jobSalary:myResults[4].jobSalary,
                            jobDesc:myResults[3].jobDescription,jobId:jobId,requirements:myResults[5]});
                    }
                    else 
                    {
                        //redirect to an error handler.
                        //jump to one of the error handling routes

                    }

                 });
      }
      else
      {
          //unauthorized/unacceptable object Id
          //redirect to an error handler.
          //jump to one of the error handling routes specifically a badInput Error

      }

});
