var express = require('express');
var mongoose = require('mongoose');
var bodyparser = require('body-parser');
var multer = require('multer');
var hbs = require('hbs');

var app = express();
var companyFileUploadRouter = express.Router();
companyFileUploadRouter.use(bodyparser.urlencoded({extended:false}));

var storage =   multer.diskStorage({
  destination: function (req, file, callback) {
    callback(null, './company_logo_uploads');
  },
  filename: function (req, file, callback) {
     
    callback(null, file.fieldname + '-' + Date.now());
  },
  
});

var upload = multer({
   storage : storage, 
   
      limits: { fieldNameSize: 100, fileSize: 514000},
   
       fileFilter : function(req, file, callback){
        if(!file.originalname.match(/\.(jpg|jpeg|png)$/)){
          return callback(new Error('Only image files are allowed!'));
       }
        callback(null, true);
    }
  
}).single('companyLogo');


companyFileUploadRouter.route('/')

.get(function(req, res){
    
     res.render("company/companyFileUpload.hbs");
    
})

.post(function(req, res, next){
  
       
      upload(req, res, function(err){
    
     if(err){
       
        return res.end("Error uploading files");
     }
    res.render("dummy_home.hbs");

  });
  
  
});

module.exports = companyFileUploadRouter;
