var bodyParser = require("body-parser");
var express    = require("express");
var router     = express.Router();
var functions  = require("../../functions/functions");

router.use(bodyParser.urlencoded({ extended:true}));

var companyJobPosting = require('../../models/job-postings-model');
var userRegistration = require('../../models/user-registration-model');
var applicantDetails = require('../../models/applicant-details-model');
var companyDetails = require('../../models/company-details-model');

module.exports = router;

router.get('/', function(req, res, next){

    var item = {
        jobTitle: "Naotyoruku",
        jobType: "Cool",
        jobValidity: "17 Jan",
        jobDescription: "Very Cool Job",
        jobRequirements: [
           {
               basicReq: [
                   { 
                       fieldName: "basic field" 
                   }
               ],
               advancedReq: [
                   {
                       fieldName: "advanced field",
                       acceptedType: "String"
                   }
               ]
           }
        ]
    };

    var data = new companyJobPosting(item);
    data.save();
    
    res.render("seed/seed-job-postings",
        {
            message: "Inserted a sample job post."
        });
});

router.get('/update-relation', function(req, res, next){

    var job_posting_id = "5895fe884a679b282c217fe4";
    var company_id = "589610b5d68e0d29ec5a2abe";
    
    companyJobPosting.findById(job_posting_id, function(err, doc) {
        if (err) {
            console.error('error, no entry found');
        }
        doc.companyId = company_id;
        doc.save();
    })
    
    res.render("seed/seed-job-posting-update", {
                                            message: "Seeded relation."
                                        });
});

router.get('/add-to-user-registration', function(req, res, next){

    var job_posting_id = "5895fe884a679b282c217fe4";
    var user_registration_id = "589761770676d8328c28a43a";
    
    userRegistration.findById(user_registration_id, function(err, doc) {
        if (err) {
            console.error('error, no entry found');
        }
        doc.jobs.push(job_posting_id);
        doc.save();
    })
    
    res.render("seed/seed-job-posting-update", {
                                            message: "Added to user."
                                        });
});