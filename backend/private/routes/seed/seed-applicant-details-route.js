var bodyParser = require("body-parser");
var express    = require("express");
var router     = express.Router();
var functions  = require("../../functions/functions");

router.use(bodyParser.urlencoded({ extended:true}));

var companyJobPosting = require('../../models/job-postings-model');
var userRegistration = require('../../models/user-registration-model');
var applicantDetails = require('../../models/applicant-details-model');
var companyDetails = require('../../models/company-details-model');

module.exports = router;

router.get('/', function(req, res, next){

    var item = {
                    firstName: "Jerryke",
                    middleName: "Alexander", 
                    surName: "Agudogo",
                    birthdate: "400 BC",
                    marital_status: "Married",
                    sex: "Confused",
                    region: "Greater Accra",
                    city: "Accra",
                    nationality:"Chinese",
                    country_of_residence: "Ghana",
                    level_education: "Primary",
                    occupation_status: "Waakye Seller",
                    address:"#4 Nsawam Prison",
                    email: "jmoney@nsawam.com",
                    phone: "034323212121"
    };

    var data = new applicantDetails(item);
    data.save();
    
    res.render("seed/seed-applicant-details",
        {
            message: "Inserted an applicant details record."
        });
});

router.get('/add-to-user-registration', function(req, res, next){

    var applicant_details_id = "58960d773ec22a04ac35403f";
    var user_registration_id = "589761770676d8328c28a43a";
    
    userRegistration.findById(user_registration_id, function(err, doc) {
        if (err) {
            console.error('error, no entry found');
        }
        doc.applicant_details = applicant_details_id;
        doc.save();
    })
    
    res.render("seed/seed-job-posting-update", {
                                            message: "Added to user."
                                        });
});