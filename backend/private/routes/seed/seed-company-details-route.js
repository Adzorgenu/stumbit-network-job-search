var bodyParser = require("body-parser");
var express    = require("express");
var router     = express.Router();
var functions  = require("../../functions/functions");

router.use(bodyParser.urlencoded({ extended:true}));

var companyDetails = require('../../models/company-details-model');

module.exports = router;

router.get('/', function(req, res, next){

    var item = { 
                companyName: "Barclays",  
                address: "400 Bank Street",
                email:  "info@barclays.com",
                phone: "0234345678",
                sector: "Bankings",
                region_of_location: "Greater Accra",
                category: "Swag",
                description: "The Bank for Ballas",
                website: "barclays.com"
    };

    var data = new companyDetails(item);
    data.save();
    
    res.render("seed/seed-company-details",
        {
            message: "Inserted a company detail object."
        });
});