/*
* Require necessary dependencies,frameworks and libraries
* :Mongoose,bodyParser,express
* Require the functions file.File has generic functions to be used in the app
*/
var functions = require("../../functions/functions");
var SignUpClass = require("../../classes/signUpClass");
var _ = require("lodash");

/*
exports this function containing the routes....
*/
module.exports = function(app,express) 
{
    let router = express.Router();

    //connection script
    var connection = app.locals.connection;

    /*
    * Deal with sign up post
    */
    router.post('/signUp', function(req, res)
    {
        //generate new response format and instantiate
        let response = new (require("../../functions/serverResponseFormat"))().getNewFrontEndResponse();

        //fetch form elements
        // console.log(req.body);
        var email = (req.body.email) ? req.body.email: null;
        var password = (req.body.password) ? req.body.password: null;
        var retypedPass = (req.body.retypePassword) ? req.body.retypePassword: null;
        var registrationType = (req.body.registrationType) ? req.body.registrationType:null;

        //perform validations.
        let checkedRegistrationType = functions.validateRegistrationType(registrationType);
        let checkedEmail = functions.validateEmail(email);
        let checkedPassword = functions.validatePassword(password);
        let checkedRetypePassword = functions.validateRetypePassword(password,retypedPass);

        //set error messages
        let errRegType = (checkedRegistrationType) ? "":"Invalid registration type. Please select a correct field";
        let errEmail = (checkedEmail) ? "":"Invalid email. A valid email is required";
        let errPass  =  (checkedPassword) ? "":"A valid password with a minimum of 8 characters is required.";
        let errRetypePass = (checkedRetypePassword) ? "" : "Password invalid";

        if( !checkedRegistrationType || !checkedEmail || !checkedPassword || !checkedRetypePassword)
        {
            // console.log('everything fails')
            //Render response
            response.response = "form";
            response.status = 200;
            response.msg = "Validation failed";

            //response.form
            response.form = {
                email:{value:email,isValid:checkedEmail,msg:errEmail},
                password:{value:password,isValid:checkedPassword,msg:errPass},
                retypedPassword:{value:retypedPass,isValid:checkedRetypePassword,msg:errRetypePass},
                registrationType:{value:registrationType,isValid:checkedRegistrationType,msg:errRegType}
            };

            //render json
            res.status(200);
            res.json(response);
        }
        else
        {
            //instantiate a signUpClass
            //pass registrationType,email and password into the constructor
            // fourth arg holds userID which is empty in this case
            let signUp = new SignUpClass(registrationType,email,password,"",connection);

            //sign up a user
            //returned vars in callback
            signUp.signUpUser(function(success,message,userId)
            {
                //check the status of the message
                //successful
                if(success) 
                {
                    response.response = "okay";
                    response.status = 200;
            
                    if(message == "yesEmailSent")
                    {
                        response.data.other = "sent";
                    }
                    else if(message == "noEmailSent")
                    {
                        response.data.other = "notsent";
                        response.data.fectched = userId;
                    }
        
                }
                else 
                {
                    response.response = "error";
                    response.status = 500;    
                    response.msg = " A problem was encountered";
                }

                //render response
                res.status(200);
                res.json(response);
            });//end of callback
        }
    });//end of sign up router


    /*
    * Send Verification Information to User
    * Get API
    */
    router.get('/resendEmail',function(req,res)
    {
        //generate new response format and instantiate
        let response = new (require("../../functions/serverResponseFormat"))().getNewFrontEndResponse();
        
        //user Id;
        var userId = req.query.id;

        //validate userId 
        let validUserId = functions.validateObjectId(userId);

            //check for validity
        if(validUserId)
        {
            //instantiate a signUpClass
            //pass registrationType,email,password,userId into the constructor
            let signUp = new SignUpClass("","","",userId,connection);


            //send email
            signUp.resendUserEmail(function(success,message,userId,opts)
            {
                //check the status of the message
                //successful
                if(success) 
                {
                    response.response = "okay";
                    response.status = 200;
            
                    if(message == "yesEmailSent")
                    {
                        response.data.other = "sent";
                    }
                    else if(message == "noEmailSent")
                    {
                        response.data.other = "notsent";
                        response.data.fectched = userId;
                    }
                    else if(message == "activated")
                    {
                        //if already activated, sign user in 
                        response.data.other = "activated";
                        //encrypt email,userID and role to give a token
                        //generate and return a token
                        let tokenAuth = new (require("../../classes/tokenAuthentication"))();
                        let payload = {userID: opts.id,role:opts.role,email:opts.email};
                        tokenAuth.setPayload(payload);
                        let token = tokenAuth.signToken();

                        //set the response properties
                        response.data.other = "activated";
                        response.data.token = token;
                        response.data.path = opts.role;
                    }//end of activated
        
                }//end of success
                else //if system fails
                {
                    response.response = "error";
                    response.status = 500;    
                    response.msg = " A problem was encountered";
                }

                //render response
                res.status(200);
                res.json(response);
            });//end of resending email
        }//end of if pass validation
        else //validation fails
        {
            //Render response
            response.response = "form";
            response.status = 200;
            response.msg = "Validation failed";

            //response.form
            response.form = {
                userID:{value:userId,isValid:false,msg:"Invalid input"}
            };
            
            //render json
            res.status(200);
            res.json(response);
        }// end of failed validation
    });//end of route


    /*
    * Handle email verification
    */
    router.get('/verify', function(req, res)
    {
        //generate new response format and instantiate
        let response = new (require("../../functions/serverResponseFormat"))().getNewFrontEndResponse();

        var userId = req.query.id;

        //validate userId 
        var validUserId = functions.validateObjectId(userId);

        //check for validity
        if (validUserId) 
        {
            //instantiate a signUpClass
            //pass registrationType,email and password into the constructor
            var signUp = new SignUpClass("", "", "", userId,connection);

            //send email
            signUp.verifyUser(function (success,verified,email,role)
            {
                //check the status of the message
                //successful
                if(success) 
                {
                    response.response = "okay";
                    response.status = 200;

                    if (verified)
                    {
                        //encrypt email,userID and role to give a token
                        //generate and return a token
                        let tokenAuth = new (require("../../classes/tokenAuthentication"))();
                        let payload = {userID: _.toString(userId),role:role,email:email};
                        tokenAuth.setPayload(payload);
                        let token = tokenAuth.signToken();

                        //set the response properties
                        response.data.other = {verified:true};
                        response.data.token = token;
                        response.data.path = role
                    }
                    else
                    {
                        //set the response properties
                        response.data.other = {verified:false};                        
                    }//end of is verified
        
                }
                else 
                {
                    response.response = "error";
                    response.status = 500;    
                    response.msg = " A problem was encountered";
                }//end of is operation successful

                //render response
                res.status(200);
                res.json(response);
            });//end of class operations
                
        }
        else  //validation failed
        {
            //Render response
            response.response = "form";
            response.status = 200;
            response.msg = "Validation failed";

            //response.form
            response.form = {
                userID:{value:userId,isValid:false,msg:"Invalid input"}
            }
            
            //render json
            res.status(200);
            res.json(response);
        }//failed validation
    });//end of route


    /*
    * This part handles an ajax request from the signUp page
    * THis route checks if an email exists in our database or is already used
    */
    router.get("/emailCheck",function (req,res)
    {
        let email = req.query.email;

        //generate new response format and instantiate
        let response = new (require("../../functions/serverResponseFormat"))().getNewFrontEndResponse();
                
        //validate email
        //check for validity
        if (functions.validateEmail(email)) 
        {
           let newSignUp = new SignUpClass("",email,"","",connection);

           //check if email exists
           newSignUp.doesEmailExist(function(success,exists)
            {
                if(success) 
                {
                    response.response = "okay";
                    response.status = 200;
                    response.data.other = {exists:exists};
                }
                else 
                {
                    response.response = "error";
                    response.status = 500;    
                    response.msg = " A problem was encountered";
                    response.data.other = {exists:false};
                }//end of is operation successful

                //render response
                res.status(200);
                res.json(response);
            });//end of class operations
            
        }
        else  //validation failed
        {
            //Render response
            response.response = "form";
            response.status = 200;
            response.msg = "Validation failed";

            //response.form
            response.form = {
                email:{value:email,isValid:false,msg:"A valid email is required"}
            };
            
            //render json
            res.status(200);
            res.json(response);
        }//failed validation

    });//end of class operations

    //return router
    return router;
};
    

