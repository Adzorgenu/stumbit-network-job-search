//express router
var express = require("express");
var router = express.Router();

//export router
module.exports = router;

/*
 * Remove all session variables. Redirect to signIn
 */
router.get('/logout', function(req, res,next)
{
                //destroy session data
        req.session.destroy(function (err)
        {
             if(!err){
                 res.redirect("/signIn");
             }
        });
});
