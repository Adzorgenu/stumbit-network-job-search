/*
 * Require necessary dependencies,frameworks and libraries
 * :Mongoose,bodyParser,express
 * Require the functions file.File has generic functions to be used in the app
 */
var bodyParser = require("body-parser");
var express = require("express");
var router = express.Router();
var functions = require("../../functions/functions");

router.use(bodyParser.urlencoded({ extended:true}));

userId = "58b6a5c0865c105c175fee6b";

//export router
module.exports = router;


/*
 * This route renders a change password hbs file 
 * (after user clicks on change password in the dashboard)
 */
 router.get("/changePassword",function(req,res,next)
 {
            //render changePassword page with userData
     res.render("accounts/changePassword",{ errormsg:{ oldPassErr:"",newPassErr:"",
         newPassConfirmErr:"",passMismatch:""},formData:{oldPass:"",
         newPass:"",newPassConfirm:""},okayMessage:false
     });
 });

/*
 * Handle Post route (change password)
 */
router.post("/changePassword",function(req,res,next)
{
       //get  form body
       var oldPass = req.body.oldPass;
       var newPass = req.body.newPass;
       var newPassConfirm = req.body.newPassConfirm;

       //validate field data
       //check for password validity and new password match
       var isOldPassValid = functions.validatePassword(oldPass);
       var isNewPassValid = functions.validatePassword(newPass);
       var isNewConfirmPassValid = functions.validatePassword(newPassConfirm);
       var isPassMatch = functions.valideRetypePassword(newPass,newPassConfirm);

      //set error messages
     var errOldPass = (isOldPassValid) ? "":"Invalid password. A minimum of 8 characters is required ";
     var errNewPass = (isNewPassValid) ? "" : "Invalid password. A minimum of 8 characters is required";
     var errNewConfirmPass = (isNewConfirmPassValid) ? "" : "Invalid password. A minimum of 8 characters is required";
     var errPassMatch = (isPassMatch) ? "" : "Password mismatch";

            //respond with an error message if  problem(s) is(are) detected
    if( !isOldPassValid || !isNewPassValid || !isNewConfirmPassValid || !isPassMatch)
    {
        //Pass error messages to change password Page
        //Also pass the submitted form data to the page as well
        //Set okayMessage to false(this prevents any toggling of success action on the frontend)
        res.render("accounts/changePassword",{ errormsg:{ oldPassErr:errOldPass,newPassErr:errNewPass,
            newPassConfirmErr:errNewConfirmPass,passMismatch:errPassMatch},formData:{oldPass:oldPass,
            newPass:newPass,newPassConfirm:newPassConfirm},okayMessage:false
         });
    }
    else
    {
         //instantiate an account setting class and pass in the user's id
         var accountClass = require("../../classes/accountSettings");

            //pass a userId into the object
         var newAcctClass = new accountClass(userId);

          //check if old password is correct
           newAcctClass.isUserPassCorrect(oldPass,function(match)
           {
                   //return an error with form data if old password is incorrect
                  if(!match)
                  {
                      //Pass error messages to change password Page
                      //Also pass the submitted form data to the page as well
                      //Set okayMessage to false(this prevents any toggling of success action on the frontend)
                      res.render("accounts/changePassword",{ errormsg:{ oldPassErr:"Incorrect password",newPassErr:"",
                          newPassConfirmErr:"",passMismatch:""},formData:{oldPass:oldPass,
                          newPass:newPass,newPassConfirm:newPassConfirm},okayMessage:false
                      });
                  }
                  else
                  {
                        //update user password
                      newAcctClass.updateUserPassword(newPass,function(isUpdated)
                      {
                          var errorMessg = null;
                          var okayMessage = null;
                          var formData1 = null;

                          if(isUpdated)
                          {
                              //set error message and okay message(this toggles a success action on the fronted)
                              errorMessg = { oldPassErr:"",newPassErr:"",newPassConfirmErr:"",passMismatch:""};
                              formData1 ={oldPass:null,newPass:null,newPassConfirm:null};
                              okayMessage = true;
                          }
                          else
                          {
                              //set error message and okay message(this toggles a success action on the fronted)
                              errorMessg = { oldPassErr:"",newPassErr:"",newPassConfirmErr:"",passMismatch:""};
                              formData1 ={oldPass:oldPass,newPass:newPass,newPassConfirm:newPassConfirm};
                              okayMessage = false;
                          }

                          //Render changePassword page with information
                          res.render("accounts/changePassword",
                                     { errormsg:errorMessg,formData:formData1,okayMessage:okayMessage}
                                    );
                      });
                  }
           });
    }
});

/*
 * Handle ajax request for password verification
 */
 router.post("/ajaxChangePassHandler",function(req,res,next)
 {
       var password = req.body.oldPass;

       //instantiate an account setting class and pass in the user's id
       var accountClass = require("../../classes/accountSettings");

         //pass a userId into the object
       var newAcctClass = new accountClass(userId);

       //check if password is correct
        newAcctClass.isUserPassCorrect(password,function(match)
        {
            (match) ? res.send({correct:true}) : res.send({correct:false});
        });
 });
 