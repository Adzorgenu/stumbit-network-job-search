/*
* Require necessary dependencies,frameworks and libraries
* :Mongoose,bodyParser,express
* Require the functions file.File has generic functions to be used in the app
*/
var functions = require("../../functions/functions");
var SignInClass = require("../../classes/signInClass");
var _ = require("lodash");

/*
exports this function containing the routes....
*/
module.exports = function(app,express) 
{
    let router = express.Router();

    //connection script
    var connection = app.locals.connection;

    /*
    * Deal with a form post
    */
    router.post("/signIn",function(req,res,next)
    {
        //generate new response format and instantiate
        let response = new (require("../../functions/serverResponseFormat"))().getNewFrontEndResponse();
            
        //get form parameters
        var email = req.body.email;
        var password = req.body.password;

        //validate email and password;
        var validEmail = functions.validateEmail(email);
        var validPassword = functions.validatePassword(password);

        //set error messages
        errEmail = (validEmail) ? "":"A valid email is required";
        errPassowrd = (validPassword) ? "" : "A valid password with a minimum of 8 characters is required";

        //render signIn page with an error if invalid credentials or incorrect login
        if(!validEmail || !validPassword)
        {
            //Render response
            response.response = "form";
            response.status = 200;
            response.msg = "Validation failed";

            //response.form
            response.form = {
                email:{value:email,isValid:validEmail,msg:errEmail},
                password:{value:password,isValid:validPassword,msg:errPassowrd}
            };
            
            //render json
            res.status(200);
            res.json(response);
        }
        else
        {
            //instantiate sign class
            let newSignIn = new SignInClass(email,password,connection);

            //note: callback has 3 arguments
            // arg1:operation successful ? arg 2: correctness/validity, arg3:isUserActivated,
            // arg4: userRole in the Db, arg5:user/object id;
            newSignIn.authenticateUser(function(success,validUser,activated,role,validUserId)
            {
                //check the status of the message
                //successful
                if(success) 
                {
                    response.response = "okay";
                    response.status = 200;

                    if(validUser )
                    {
                        //encrypt email,userID and role to give a token
                        //generate and return a token
                        let tokenAuth = new (require("../../classes/tokenAuthentication"))();
                        let payload = {userID: _.toString(validUserId),role:role,email:email};
                        tokenAuth.setPayload(payload);
                        let token = tokenAuth.signToken();

                        //set the response properties
                        response.data.other = {verified:true,msg:"",activated:activated};
                        response.data.token = token;
                        response.data.path = role;
                    }
                    else {
                        response.data.other = {verified:false,msg:"Incorrect username  or password"};
                    }                        
                }
                else 
                {
                    response.response = "error";
                    response.status = 500;    
                    response.msg = " A problem occurred";
                }

                //render response
                res.status(200);
                res.json(response);
            });//end of callback

        }

    });//end of route

    //export router
    return router;
};

