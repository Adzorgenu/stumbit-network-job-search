var bodyParser = require("body-parser");
var express = require("express");
var router = express.Router();
var functions = require("../../functions/functions");

router.use(bodyParser.urlencoded({ extended:true}));

var companyJobPosting = require('../../models/job-postings-model');
var userRegistration = require('../../models/user-registration-model');
var applicantDetails = require('../../models/applicant-details-model');
var companyDetails = require('../../models/company-details-model');

module.exports = router;

router.get('/', function(req, res, next){
    // var applicant_id = req.session.userId;
    var applicant_id = "589761770676d8328c28a43a";
    var locals = {};

    userRegistration.findById(applicant_id, function(err, userReg) {
      console.log(userReg);
      locals.user = userReg;
    }).populate('applicant_details');
    
      userRegistration.findById(applicant_id, function(err, userReg) {
        locals.userReg = userReg;
        console.log(locals);
        res.render('applicant/applicant-my-jobs', locals);
      }).populate('jobs');
});