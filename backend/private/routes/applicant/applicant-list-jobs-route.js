var bodyParser = require("body-parser");
var express = require("express");
var router = express.Router();
var functions = require("../../functions/functions");

router.use(bodyParser.urlencoded({ extended:true}));

var companyJobPosting = require('../../models/job-postings-model');
var userRegistration = require('../../models/user-registration-model');
var applicantDetails = require('../../models/applicant-details-model');
var companyDetails = require('../../models/company-details-model');

module.exports = router;

router.get('/', function(req, res, next){
    // var applicant_id = req.session.userId;
    var applicant_id = "589761770676d8328c28a43a";
    var locals = {};

    userRegistration.findById(applicant_id, function(err, userReg) {
      console.log(userReg);
      locals.user = userReg;
    }).populate('applicant_details');
    
    companyJobPosting.find(function(err, companyJobPostings) {
      locals.companyJobPostings = companyJobPostings;
      userRegistration.find(function(err, userRegistrations) {
        locals.userRegistrations = userRegistrations;
        applicantDetails.find(function(err, applicantDetails) {
          locals.applicantDetails = applicantDetails;
          companyDetails.find(function(err, companyDetails) {
            locals.companyDetails = companyDetails;
            console.log(locals);
            res.render('applicant/applicant-list-jobs', { locals });
          });
        });
      }).populate({path: 'jobs', populate: { path: 'companyId' }});
    }).populate('companyId');
});


// Delete Routes
router.post('/delete-job-posting', function(req, res, next) {
  var id = req.body.id;
  companyJobPosting.findByIdAndRemove(id).exec();
  res.redirect('/applicant-list-jobs');
});

router.post('/delete-user-registration', function(req, res, next) {
  var id = req.body.id;
  userRegistration.findByIdAndRemove(id).exec();
  res.redirect('/applicant-list-jobs');
});

router.post('/delete-applicant-details', function(req, res, next) {
  var id = req.body.id;
  applicantDetails.findByIdAndRemove(id).exec();
  res.redirect('/applicant-list-jobs');
});

router.post('/delete-company-details', function(req, res, next) {
  var id = req.body.id;
  companyDetails.findByIdAndRemove(id).exec();
  res.redirect('/applicant-list-jobs');
});

router.post('/add-to-my-jobs', function(req, res, next) {
    var user_id = req.body.user_id;
    var job_posting_id = req.body.posting_id;
    
    userRegistration.findById(user_id, function(err, doc) {
        if (err) {
            console.error('error, no entry found');
        }
        doc.jobs.push(job_posting_id);
        doc.save();
        res.redirect('/applicant-my-jobs');
    })
});