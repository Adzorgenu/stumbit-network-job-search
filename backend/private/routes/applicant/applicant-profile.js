//APPLICANT OPERATION ROUTE

//require necessary class(es)
var ApplicantOperations = require('../../classes/applicantOperations');
var UserRegistration = require('../../classes/userRegistration');
var fn = require("../../functions/functions");
var FrontEndResponse = new (require("../../functions/serverResponseFormat"))();
var defaultFields = require("../../functions/default-select-fields");
var multer  = require('multer');
var crypto = require('crypto');
const path = require('path');
var _ = require('lodash');

//FILE UPLOAD OPTIONS
const storage = multer.diskStorage({
  destination: 'file_uploads',
  filename: function (req, file, cb) {
      crypto.randomBytes(16, (err, buf) => {
         if (err) 
            return cb([err],null);
         cb(null,buf.toString('hex')+path.extname(file.originalname));
      });
  }
});


/*
exports this function containing the routes....
*/
module.exports = function(app,express) 
{
   let router = express.Router();
   let userID = "5a9fd63a2b41506427c1309c";

   /*=============================
   * GET APPLICANT PROFILE
   *==============================
   */
   router.get('/profile',function(req,res) 
   {
      //generate new response format and instantiate
      let response = FrontEndResponse.getNewFrontEndResponse();
      let phase = (req.query.p) ? parseInt(req.query.p) : 1;
      phase = isNaN(phase) ? 1 : phase;

      //controller
      let applicantOperations = new ApplicantOperations(userID,app.locals.connection);
      applicantOperations.setApplicantDetailsDriver();
      applicantOperations.getApplicantProfileController(phase,function(results)
      {
         let finalReturn = null;
         if(results)
         {
            let r = results.res;

            if(!results.isEmpty)
            { //NOT EMPTY RESULTS
               //re-set fields
               if(phase == 1)
               {
                  finalReturn = {
                     profileID:r._id,fname:r.firstName,mname:r.middleName,lname:r.surName,bdate:r.birthdate,
                     maritalStatus:r.marital_status,sex:r.sex,highEdu:r.level_education,
                     school_other:r.level_education_other,employStatus:r.occupation_status,
                     work_other:r.occupation_status_other
                  };

                  //set response other fields
                  //fetch necessary fields
                  response.data.other = {
                     isNewProfile:results.isEmpty, 
                     sexTypes:defaultFields.sexTypesObject(),
                     maritalStatusTypes:defaultFields.maritalStatusesObject(),
                     employStatusTypes:defaultFields.employmentStatusObjects(),
                     highEduTypes: defaultFields.educationLevelObject()
                  }

                  response.response = "okay";
                  response.status = 200;
                  response.data.fetched = finalReturn;

                  //render response
                  res.status(200);
                  res.json(response);
               }//end of phase 1
               else if(phase ==2)
               {
                  response.data.other = {
                     isNewProfile:results.isEmpty
                  }

                  //use user's registration email
                  finalReturn = {
                    profileID:r._id,email:r.email,altEmail:r.alt_email,phone:r.phone,
                    altPhoneNumber:r.alt_phone,postalAddress:r.post_address,
                    altPostalAddress:r.alt_post_address                  
                  }

                  //if no email is used, fetch the primary one(for registration)
                  if(r.email == "")
                  {
                     //fetch user registration email
                     let newUserReg = new UserRegistration(userID,app.locals.connection);
                     newUserReg.getUserRegisteredEmail(function(email){
                        if(email) {
                           finalReturn.email =  email;

                           response.response = "okay";
                           response.status = 200;
                           response.data.fetched = finalReturn;

                           //render response
                           res.status(200);
                           res.json(response);
                        }
                     });//end of get email
                  }//end of empty email
                  else
                  {
                     response.response = "okay";
                     response.status = 200;
                     response.data.fetched = finalReturn;

                     //render response
                     res.status(200);
                     res.json(response);                     
                  }
               }//end of phase 2
               else if(phase == 3)
               {
                  response.data.other = {
                     isNewProfile:results.isEmpty
                  };

                  //use user's registration email
                  finalReturn = {
                     profileID:r._id,countryOrigin:r.nationality,
                     countryResidence:r.country_of_residence,city:r.city
                  };

                  response.response = "okay";
                  response.status = 200;
                  response.data.fetched = finalReturn;

                  //render response
                  res.status(200);
                  res.json(response);                     
               }
               else if(phase == 4)
               {
                  response.data.other = {
                     isNewProfile:results.isEmpty
                  };

                  //use user's registration email
                  finalReturn = {
                     profileID:r._id,cv:r.cv_fileName,cert:r.degree_fileName
                  };

                  response.response = "okay";
                  response.status = 200;
                  response.data.fetched = finalReturn;

                  //render response
                  res.status(200);
                  res.json(response);                                       
               }
            }//end of res not empty
            else 
            {  //EMPTY RESULTS(NOTHING RETURNED)
               //the response data field only varies for phase 1
               if(phase == 1 ){
                  response.data.other = {
                     isNewProfile:results.isEmpty, 
                     sexTypes:defaultFields.sexTypesObject(),
                     maritalStatusTypes:defaultFields.maritalStatusesObject(),
                     employStatusTypes:defaultFields.employmentStatusObjects(),
                     highEduTypes: defaultFields.educationLevelObject(),
                  }  
               }
               else {
                  //if empty results
                  response.data.other = {
                     isNewProfile:results.isEmpty
                  }                  
               }               
                
               response.response = "okay";
               response.status = 200;
               response.data.fetched = finalReturn;

               //render response
               res.status(200);
               res.json(response);                                 }
         }
         else
         {
            response.response = "error";
            response.status = 500;    
            response.msg = " A problem occurred";           

            //render response
            res.status(200);
            res.json(response);

         }

         //memory management; 
      });

   });

   /*=============================
   * UPDATE APPLICANT PROFILE
   *==============================
   */
   router.post('/profile',function (req,res) 
   {
      let userID = "5a9fd63a2b41506427c1309c";
      //generate new response format and instantiate
      let response = FrontEndResponse.getNewFrontEndResponse();
      let mode = (req.query.m && req.query.m =="u") ? "update":"new"; 
      let phase = (req.query.p) ? parseInt(req.query.p) : 1;
      phase = isNaN(phase) ? 1 : phase;

      //user info and form
      let userInfo = null; 
      let profileID = (req.body.profileID) ? req.body.profileID:"";
      let formInfo = null;

      //perform validations here
      let isFormValid = true;

      let profileIDValid = true;
      let profileIDError = "";
      if(profileID !="" && !fn.validateObjectId(profileID)){//profileID(objectID)
         profileIDValid = false;
         profileIDError = "Invalid ID";
         isFormValid = false;
      }

      //VALIDATIONS for first phase (basic profile information)
      if(phase == 1)
      {
         //form Info
         formInfo =  {
           profileID: {value:profileID,error:profileIDError,valid:profileIDValid}, 
           fname:{value:(req.body.fname)? req.body.fname:"",error:"",valid:true},
           mname:{value:(req.body.mname)? req.body.mname:"",error:"",valid:true},
           lname:{value:(req.body.lname)? req.body.lname:"",error:"",valid:true},
           bdate:{value:(req.body.bdate)? req.body.bdate:"",error:"",valid:true},
           maritalStatus:{value:(req.body.maritalStatus)? req.body.maritalStatus:"",error:"",valid:true},
           sex:{value:(req.body.sex)? req.body.sex:"",error:"",valid:true},
           highEdu:{value:(req.body.highEdu)? req.body.highEdu:"",error:"",valid:true},
           school_other:{value:(req.body.school_other)?req.body.school_other:"",error:"",valid:true},
           employStatus:{value:(req.body.employStatus)? req.body.employStatus:"",error:"",valid:true},
           work_other:{value:(req.body.work_other)? req.body.work_other:"",error:"",valid:true}           
         }; 

         //userForm
         userInfo = {
            userID:userID,
            fname: formInfo.fname.value,
            mname:formInfo.mname.value,
            lname:formInfo.lname.value,
            bdate:formInfo.bdate.value,
            maritalStatus:formInfo.maritalStatus.value,
            sex:formInfo.sex.value,
            highEdu:formInfo.highEdu.value,
            school_other:formInfo.school_other.value,
            employStatus:formInfo.employStatus.value,
            work_other:formInfo.work_other.value
         };

         //PERFORM VALIDATIONS
         if(!fn.isValidWord(formInfo.fname.value)){//first name
            formInfo.fname.valid = false;
            formInfo.fname.error = "Invalid name";
            isFormValid = false;
         }
         if(formInfo.mname.value !="" && !fn.isValidWord(formInfo.mname.value)){//middle name
            formInfo.mname.valid = false;
            formInfo.mname.error = "Invalid name";
            isFormValid = false;
         }
         if(!fn.isValidWord(formInfo.lname.value)){//last name
            formInfo.lname.valid = false;
            formInfo.lname.error = "Invalid name";
            isFormValid = false;
         }
         if(!fn.validateDate(formInfo.bdate.value)){//bdate
            formInfo.bdate.valid = false;
            formInfo.bdate.error = "Invalid date";
            isFormValid = false;
         }
         if(!fn.validateMaritalStatus(formInfo.maritalStatus.value)){//marital status
            formInfo.maritalStatus.valid = false;
            formInfo.maritalStatus.error = "Unknown marital status";
            isFormValid = false;
         }
         if(!fn.validateSex(formInfo.sex.value)){//sex
            formInfo.sex.valid = false;
            formInfo.sex.error = "Unknown sex type";
            isFormValid = false;
         }
         if(!fn.validateHighEdu(formInfo.highEdu.value)){//education status
            formInfo.highEdu.valid = false;
            formInfo.highEdu.error = "Unknown type";
            isFormValid = false;
         }
         if(formInfo.school_other.value !="" && 
            !fn.isValidWords(formInfo.school_other.value)){//other educ level
            formInfo.school_other.valid = false;
            formInfo.school_other.error = "Invalid word";
            isFormValid = false;
         }
         if(!fn.validateEmploymentStatus(formInfo.employStatus.value)){//employment status
            formInfo.employStatus.valid = false;
            formInfo.employStatus.error = "Unknown employment status";
            isFormValid = false;
         }   
         if(formInfo.work_other.value !="" && 
            !fn.isValidWords(formInfo.work_other.value)){//other educ level
            formInfo.work_other.valid = false;
            formInfo.work_other.error = "Invalid name";
            isFormValid = false;
         }

      }//end of phase 1
      else if(phase == 2) 
      {
         formInfo =  {
           profileID: {value:profileID,error:profileIDError,valid:profileIDValid}, 
           email:{value:(req.body.email)? req.body.email:"",error:"",valid:true},
           altEmail:{value:(req.body.altEmail)? req.body.altEmail:"",error:"",valid:true},
           phone:{value:(req.body.phone)? req.body.phone:"",error:"",valid:true},
           altPhoneNumber:{value:(req.body.altPhoneNumber)? req.body.altPhoneNumber:"",error:"",valid:true},
           postalAddress:{value:(req.body.postalAddress)? req.body.postalAddress:"",error:"",valid:true},
           altPostalAddress:{value:(req.body.altPostalAddress)? req.body.altPostalAddress:"",error:"",valid:true}
         }; 

         userInfo = {
            userID:userID,
            email: formInfo.email.value,
            altEmail:formInfo.altEmail.value,
            phone:formInfo.phone.value,
            altPhoneNumber:formInfo.altPhoneNumber.value,
            postalAddress:formInfo.postalAddress.value,
            altPostalAddress:formInfo.altPostalAddress.value
         };

         //PERFORM VALIDATIONS
         if(!fn.validateEmail(formInfo.email.value)){//email
            formInfo.email.valid = false;
            formInfo.email.error = "Invalid email";
            isFormValid = false;
         }
         if(formInfo.altEmail.value !="" && !fn.validateEmail(formInfo.altEmail.value)){//alt email
            formInfo.altEmail.valid = false;
            formInfo.altEmail.error = "Invalid email";
            isFormValid = false;
         }
         if(!fn.validatePhone(formInfo.phone.value)){//phone
            formInfo.phone.valid = false;
            formInfo.phone.error = "Invalid phone number";
            isFormValid = false;
         }
         if(formInfo.altPhoneNumber.value !="" && !fn.validatePhone(formInfo.altPhoneNumber.value)){
            //altPhoneNumber
            formInfo.altPhoneNumber.valid = false;
            formInfo.altPhoneNumber.error = "Invalid phone number";
            isFormValid = false;
         }
         if(!fn.isValidWords(formInfo.postalAddress.value)){//postalAddress
            formInfo.postalAddress.valid = false;
            formInfo.postalAddress.error = "Invalid postal address";
            isFormValid = false;
         }
         if(formInfo.altPostalAddress.value !="" && !fn.isValidWords(formInfo.altPostalAddress.value)){
            //altPostalAddress
            formInfo.altPostalAddress.valid = false;
            formInfo.altPostalAddress.error = "Invalid postal address";
            isFormValid = false;
         }

      }//end of phase 2
      else if(phase == 3)
      {
         formInfo =  {
           profileID: {value:profileID,error:profileIDError,valid:profileIDValid}, 
           countryOrigin:{value:(req.body.countryOrigin)? req.body.countryOrigin:"",error:"",valid:true},
           countryResidence:{value:(req.body.countryResidence)? req.body.countryResidence:"",error:"",valid:true},
           city:{value:(req.body.city)? req.body.city:"",error:"",valid:true}
         }; 

         userInfo = {
            userID:userID,
            countryOrigin: formInfo.countryOrigin.value,
            countryResidence:formInfo.countryResidence.value,
            city:formInfo.city.value
         };

         //PERFORM VALIDATIONS
         if(!fn.isValidWords(formInfo.countryOrigin.value)){//countryOrigin
            formInfo.countryOrigin.valid = false;
            formInfo.countryOrigin.error = "Invalid text. Special characters are not allowed";
            isFormValid = false;
         }
         if(!fn.isValidWords(formInfo.countryResidence.value)){//countryResidence
            formInfo.countryResidence.valid = false;
            formInfo.countryResidence.error = "Invalid text. Special characters are not allowed";
            isFormValid = false;
         }
         if(!fn.isValidWords(formInfo.city.value)){//city of residence
            formInfo.city.valid = false;
            formInfo.city.error = "Invalid text. Special characters are not allowed";
            isFormValid = false;
         }
      }//end of phase 3

      //valid form
      if(isFormValid)
      {
         //controller
         let applicantOperations = new ApplicantOperations(userID,app.locals.connection);
         applicantOperations.setApplicantDetailsDriver();
         applicantOperations.updateApplicantProfileController(mode,phase,profileID,userInfo,function(results)
         {
            if(results){
               response.response = "okay";
               response.status = 200;
               response.data.other = {isSaved:true};
            }
            else{
               response.response = "error";
               response.status = 500;    
               response.msg = " A problem occurred";           
            }

            //render response
            res.status(200);
            res.json(response);

            //memory management; 
         });
      }
      else 
      {
         //validation stuffs
         response.response = "form";
         response.status = 200;
         response.msg = "Validation failed";

         //response.form
         response.form = formInfo;
        
         //render json
         res.status(200);
         res.json(response);

         //memory management here later
      }

   });//end of route 



   /*=======================================
   * UPDATE APPLICANT DOCS (cv,cert,other)
   *========================================
   */
   var upload = multer({
     storage : storage, 
     limits: { fieldNameSize: 100, fileSize: 514000},
     fileFilter : function(req, file, cb){
       if(!file.originalname.match(/\.(pdf|doc|docx)$/)){
         return cb([{fieldName:file.fieldname,error:'Only .pdf and .doc(.docx) files are allowed!'}],null);
       }
       cb(null, true);
     }
   }).fields([{name:'cv',maxCount:1 },{name:'cert',maxCount:1}]); //field names
   
   //route handling
   router.post('/profile/files',function (req,res) 
   {
      //generate new response format and instantiate
      let response = FrontEndResponse.getNewFrontEndResponse();
      let isFormValid = true;
      let isUploaded = false;
      let formInfo = {
         profileID:{value:(req.query.pID) ? req.query.pID:"",error:"",valid:true},
         cv:{value:"",error:"",valid:true},
         cert:{value:"",error:"",valid:true}
      };

      if(formInfo.profileID.value !="" && 
         !fn.validateObjectId(formInfo.profileID.value)){//profileID(objectID)
         formInfo.profileID.valid = false;
         formInfo.profileID.error = "Invalid ID";
         isFormValid = false;
      }

      upload(req, res, function(err)
      {
         if(err)
         {
            isFormValid = false;
            isUploaded = false;

            let f1 = err[0];
            if(f1.fieldName && f1.fieldName == "cv"){
               formInfo.cv.valid = false;
               formInfo.cv.error =  f1.error;
            }
            if(f1.fieldName && f1.fieldName == "cert"){
               formInfo.cert.valid = false;
               formInfo.cert.error =  f1.error;
            }
         }
         else
         {
            // console.log(req.body);
            // console.log(req.files);
            if(_.isEmpty(req.files))
            {
               console.log('req.files is empty');
               isFormValid = false;

               formInfo.cv.valid = false;
               formInfo.cv.error =  "No document was provided";
               formInfo.cert.valid = false;
               formInfo.cert.error =  "No document was provided";
            }
   
            isUploaded = true;
         }

         //form is valid and file(s) was(ere) uploaded
         if(isFormValid && isUploaded)
         {
            let userInfo = {
               userID:userID,
               cv:(req.files.cv) ? req.files.cv[0].filename:"",
               cert:(req.files.cert) ? req.files.cert[0].filename:""
            };

            //controller
            let applicantOperations = new ApplicantOperations(userID,app.locals.connection);
            applicantOperations.setApplicantDetailsDriver();
            applicantOperations.updateApplicantFiles(formInfo.profileID.value,userInfo,function(results)
            {
               console.log('results after update');
               if(results){
                  response.response = "okay";
                  response.status = 200;
                  response.data.other = {isSaved:true};
               }
               else{
                  response.response = "error";
                  response.status = 500;    
                  response.msg = " A problem occurred";           
               }

               //render response
               res.status(200);
               res.json(response);

               //memory management; 
            });
         }
         else
         {
            //invalid form or problem in uploading form
            //validation stuffs
            response.response = "form";
            response.status = 200;
            response.msg = "Validation failed";

            //response.form
            response.form = formInfo;
           
            //render json
            res.status(200);
            res.json(response);
         }
      });
   });//end of router

   //export router
   return router;
};
