var bodyParser = require("body-parser");
var express = require("express");
var router = express.Router();
var functions = require("../../functions/functions");

router.use(bodyParser.urlencoded({ extended:true}));

var companyJobPosting = require('../../models/job-postings-model');
var userRegistration = require('../../models/user-registration-model');
var applicantDetails = require('../../models/applicant-details-model');
var companyDetails = require('../../models/company-details-model');
var jobApplication = require('../../models/job-applications-model');

module.exports = router;

// router.get('/', function(req, res, next){
//     var locals = {};
//     var job_posting_id = "5895fe884a679b282c217fe4";

//     companyJobPosting.findById(job_posting_id, function(err, doc) {
//         if (err) {
//             console.error('error, no entry found');
//         }
//         locals = doc.jobRequirements;
//         res.render('job-application', { locals });
//     })
// });

router.post('/', function(req, res, next){
    var locals = {};
    locals.user_id = req.body.user_id;
    locals.posting_id = req.body.posting_id;

    companyJobPosting.findById(req.body.posting_id, function(err, doc) {
        if (err) {
            console.error('error, no entry found');
        }
        locals.company_id = doc.companyId;
        console.log(doc);
        locals.jobRequirements = doc.jobRequirements;
        res.render('applicant/job-application', { locals });
    })
});

router.post('/submit', function(req, res, next){

    var locals = {
        basic_requirements: [],
        advanced_requirements: []
    };

    locals.applicant = req.body.user_id;
    locals.company = req.body.company_id;
    locals.job_posting = req.body.posting_id; 

    var results = req.body.advanced_requirements;
    results.forEach(function(value){
        var temp = {
            response: value
        }
        locals.advanced_requirements.push(temp);
    });
    var data = new jobApplication(locals);
    data.save();
    res.render('applicant/job-application-submit', { locals });
});

