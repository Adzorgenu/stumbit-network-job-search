var express = require('express');
var mongoose = require('mongoose');
var bodyparser = require('body-parser');
var Companies = require('../../models/companyReg-schema');
var hbs = require('hbs');



var app = express();
var adRouter = express.Router();
adRouter.use(bodyparser.urlencoded({extended:false}));


adRouter.route('/')

.get(function(req, res){
   

    //trying to get queried data from the database into an array 
    //and get in displayed in console
    Companies.find({

    }, function(err, results){
      if(err)
      throw err;
      
      
      
      
    
      var emailCollections = []; // holding the company emails
      var companyNameCollections = []; //holding the company Names 
      var phoneCollections = []; // holding the company contact line
      var length = results.length; // the number of companies in the database
      var displayLength = 2; // number of companies to display per instance
      
      
      if(!results){
        
      res.render('ad/ad_index.hbs', {
        
         errorMessage : ""
        
      });
        
 
    }else{
      
     for(var i = 0; i < displayLength; i++){
        // declare a random number holder to randomly pick our 
        //company to be advertised
        var rand = Math.floor((Math.random() * length) + 0);
        emailCollections.push(results[rand].email);
        companyNameCollections.push(results[rand].companyName);
        phoneCollections.push(results[rand].phone);
      }
      
       //now checking to see that the same company was not picked twice
      while(emailCollections[0] == emailCollections[1]){
         var alt_rand =  Math.floor((Math.random() * length) + 0);
         emailCollections[0] = results[alt_rand].email;
         companyNameCollections[0] = results[alt_rand].companyName;
         phoneCollections[0] = results[alt_rand].phone;
      }
      
        // showing data collected from the database and stored in their 
        //respective arrays
        // console.log("Results in email collections array: " + emailCollections.join(" , ") + " ");
        // console.log("Results in websites collections array: " + websiteCollections.join(" , ") + " ");
        // console.log("Results in company name collections array: " + companyNameCollections.join(" , ") + " ");
        
        
        res.render('ad/ad_index.hbs', {
           firstCompanyName : companyNameCollections[0],
           firstCompanyEmail : emailCollections[0],
           firstCompanyPhone : phoneCollections[0],
           
           secondCompanyName : companyNameCollections[1],
           secondCompanyEmail : emailCollections[1],
           secondCompanyPhone : phoneCollections[1]
        });
      
      
    }
    
  }); 
  
});

module.exports = adRouter;