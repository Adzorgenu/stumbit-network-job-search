var ServerLogger = new (require("../classes/ServerLogger"))();
var Promise = require("bluebird");

class ApplicantDetailsDriver {

    //constructor
    constructor(connection)
    {
        this.profileModel = null;

        //require schema/model
        this.requireSchemaModel(connection);
    }

    /*
     * Require applicant-details model from models directory
     * @{parameters} - connection
     * @{return } - none
     * 09-08-2017
     */
    requireSchemaModel(connection)
    {
        try{
            //change later
            this.profileModel = require("../models/applicant-details-model")(connection);
        }catch(e) {
            // log error
            ServerLogger.writeToLog(__filename,"requireSchemaModel",e);
        }
    }//end of method

    /*
     * update user basic profile information
     * @{params} - userInfo(objects)
     * @{return} - promise(success,error)
     * 03-08-2018
     */
    updateApplicantBasicProfile(profileID,userInfo)
    {
        //console.log('in updateApplicantBasicProfile');
        let q=(profileID != "")?{"_id":profileID,"userID":userInfo.userID}:{"userID":userInfo.userID};
        let updatedInfo = {
            firstName: userInfo.fname,
            middleName:userInfo.mname,
            surName:userInfo.lname,
            birthdate:userInfo.bdate,
            marital_status:userInfo.maritalStatus,
            sex:userInfo.sex,
            level_education:userInfo.highEdu,
            level_education_other:userInfo.school_other,
            occupation_status:userInfo.employStatus,
            occupation_status_other:userInfo.work_other
        }

        let that = this;
        return new Promise(function (resolve,reject)
        {
            //update useInfo, insert if not exists(new)
            that.profileModel.update(q,updatedInfo,{overwrite :false}).exec()
                .then(function(results){
                    resolve(results);
                })
                .catch(function(error){
                    reject(error);
                });
        }); //end of promise
    }//end of method

    /*
     * update user contact profile information
     * @{params} - userInfo(objects)
     * @{return} - promise(success,error)
     * 03-08-2018
     */
    updateApplicantContactProfile(profileID,userInfo)
    {
        //console.log('in updateApplicantContactProfile');
        let q=(profileID != "")?{"_id":profileID,"userID":userInfo.userID}:{"userID":userInfo.userID};
        let updatedInfo = {
            post_address: userInfo.postalAddress,
            alt_post_address: userInfo.altPostalAddress,
            email:userInfo.email,
            alt_email:userInfo.altEmail,
            phone:userInfo.phone,
            alt_phone: userInfo.altPhoneNumber
        }

        let that = this;
        return new Promise(function (resolve,reject)
        {
            //update useInfo, insert if not exists(new)
            that.profileModel.update(q,updatedInfo,{overwrite :false}).exec()
                .then(function(results){
                    resolve(results);
                })
                .catch(function(error){
                    reject(error);
                });
        }); //end of promise
    }//end of method

    /*
     * update user residence profile information
     * @{params} - userInfo(objects)
     * @{return} - promise(success,error)
     * 03-08-2018
     */
    updateApplicantResidenceProfile(profileID,userInfo)
    {
        //console.log('in updateApplicantResidenceProfile');
        let q=(profileID != "")?{"_id":profileID,"userID":userInfo.userID}:{"userID":userInfo.userID};
        let updatedInfo = {
            city: userInfo.city,
            nationality: userInfo.countryOrigin,
            country_of_residence:userInfo.countryResidence
        }

        let that = this;
        return new Promise(function (resolve,reject)
        {
            //update useInfo, insert if not exists(new)
            that.profileModel.update(q,updatedInfo,{overwrite :false}).exec()
                .then(function(results){
                    resolve(results);
                })
                .catch(function(error){
                    reject(error);
                });
        }); //end of promise
    }//end of method

    /*
     * update applicant profile files
     * @{params} - userInfo(objects)
     * @{return} - promise(success,error)
     * 14-08-2018
     */
    updateApplicantFilesProfile(profileID,userInfo)
    {
        // console.log('in updateApplicantFilesProfile');
        // console.log(userInfo);
        // console.log('profile:'+profileID);
        let q=(profileID != "")?{"_id":profileID,"userID":userInfo.userID}:{"userID":userInfo.userID};
        let updatedInfo = null;
        
        //please don't overide cv_fileName or degree if empty
        if(userInfo.cv == "" && userInfo.cert !=""){
            updatedInfo = {
                degree_fileName:userInfo.cert
            }
        }else if(userInfo.cv != "" && userInfo.cert ==""){
            updatedInfo = {
                cv_fileName:userInfo.cv
            }
        }else if(userInfo.cv !="" && userInfo.cert !=""){
            updatedInfo = {
                cv_fileName:userInfo.cv,
                degree_fileName:userInfo.cert
            }
        }

        let that = this;
        return new Promise(function (resolve,reject)
        {
            //update useInfo, insert if not exists(new)
            that.profileModel.update(q,updatedInfo,{overwrite :false}).exec()
                .then(function(results){
                    resolve(results);
                })
                .catch(function(error){
                    reject(error);
                });
        }); //end of promise
    }//end of method


    /*
     * insert user  profile information - basic
     * @{params} - userInfo(objects)
     * @{return} - promise(success,error)
     * 11-08-2018
     */
    insertNewApplicantBasicProfile(userInfo)
    {
        //console.log('in insertNewApplicantBasicProfile');
        let newUserProfile = new this.profileModel({
            userID:userInfo.userID,
            firstName: userInfo.fname,
            middleName:userInfo.mname,
            surName:userInfo.lname,
            birthdate:userInfo.bdate,
            marital_status:userInfo.maritalStatus,
            sex:userInfo.sex,
            level_education:userInfo.highEdu,
            level_education_other:userInfo.school_other,
            occupation_status:userInfo.employStatus,
            occupation_status_other:userInfo.work_other
        })

        return new Promise(function (resolve,reject)
        {
            //update useInfo, insert if not exists(new)
            newUserProfile.save()
                .then(function(results){
                    resolve(results);
                })
                .catch(function(error){
                    reject(error);
                });
        }); //end of promise
    }//end of method

    /*
     * insert user  profile information -contact
     * @{params} - userInfo(objects)
     * @{return} - promise(success,error)
     * 11-08-2018
     */
    insertNewApplicantContactProfile(userInfo)
    {
        //console.log('in insertNewApplicantContactProfile');
        let newUserProfile = new this.profileModel({
            userID:userInfo.userID,
            post_address: userInfo.postalAddress,
            alt_post_address: userInfo.altPostalAddress,
            email:userInfo.email,
            alt_email:userInfo.altEmail,
            phone:userInfo.phone,
            alt_phone: userInfo.altPhoneNumber
        })

        return new Promise(function (resolve,reject)
        {
            //update useInfo, insert if not exists(new)
            newUserProfile.save()
                .then(function(results){
                    resolve(results);
                })
                .catch(function(error){
                    reject(error);
                });
        }); //end of promise
    }//end of method

    /*
     * insert user  profile information -residence
     * @{params} - userInfo(objects)
     * @{return} - promise(success,error)
     * 11-08-2018
     */
    insertNewApplicantResidenceProfile(userInfo)
    {
        //console.log('in insertNewApplicantResidenceProfile');
        let newUserProfile = new this.profileModel({
            userID:userInfo.userID,
            city: userInfo.city,
            nationality: userInfo.countryOrigin,
            country_of_residence:userInfo.countryResidence
        })

        return new Promise(function (resolve,reject)
        {
            //update useInfo, insert if not exists(new)
            newUserProfile.save()
                .then(function(results){
                    resolve(results);
                })
                .catch(function(error){
                    reject(error);
                });
        }); //end of promise
    }//end of method

    /*
    * get user  profile information
    * @{params} - userID,returnedData
    * @{return} - promise(success,error)
    * 08-08-2018
    */
    getApplicantProfile(userID,returned)
    {
        let that = this;
        return new Promise(function (resolve,reject)
        {
            //update useInfo, insert if not exists(new)
            that.profileModel.findOne({"userID":userID},returned).exec()
                .then(function(results){
                    resolve(results);
                })
                .catch(function(error){
                    reject(error);
                });
        }); //end of promise
    }
}

//exports
module.exports = ApplicantDetailsDriver;
