var ServerLogger = new (require("../classes/ServerLogger"))();
var Promise = require("bluebird");

class UserRegistrationDriver 
{
    //constructor
    constructor(connection)
    {
        this.regModel = null;

        //require schema/model
        this.requireSchemaModel(connection);
    }

    /*
     * Require user-registration model from models directory
     * @{parameters} - connection
     * @{return } - none
     * 11-08-2017
     */
    requireSchemaModel(connection)
    {
        try{
            //change later
            this.regModel = require("../models/user-registration-model")(connection);
        }catch(e) {
            // log error
            ServerLogger.writeToLog(__filename,"requireSchemaModel",e);
        }
    }

    /*
    * get user  registered email
    * @{params} - userID
    * @{return} - promise(success,error)
    * 11-08-2018
    */
    getUserRegisteredEmail(userID)
    {
        let that = this;
        return new Promise(function (resolve,reject)
        {
            //update useInfo, insert if not exists(new)
            that.regModel.findOne({"_id":userID},{email:1}).exec()
                .then(function(results){
                    resolve(results);
                })
                .catch(function(error){
                    reject(error);
                });
        }); //end of promise
    }
}

//exports
module.exports = UserRegistrationDriver;
