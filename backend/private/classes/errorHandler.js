/*
 * This class performs all error handling operations
 * 1. Write to file(default)
 * 2. Generate response Code
 * 3. Wrap error in a nice
 */
let errorHandler = class ErrorHandler 
{
    constructor(error)
    {
        this.error = error
    }

    //Write error message to a file
    writeToFile()
    {
        // write to file
    }

    // wrap error message
    wrapErrorInfo()
    {
        // retur
    }
}//end of class

module.exports = errorHandler;