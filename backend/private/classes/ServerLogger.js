/*
 * This class handles the logging of errors/messages
 * Server Logs can be found in ./logs/serverLogs.txt
 */
class ServerLogger {
    constructor()
    {
        this.logFile = "serverLogs.txt";
        this.fs = require('fs');
        this.moment = require('moment');
    }

    //msgLoc: error location,method: method generating the error, msg:error message
    writeToLog(msgLoc,method,msg)
    {
        //generate current timestamp(Datetime) in UTC format
        let timestamp = this.moment.utc().format();
        let logMsg = "\nERROR: loc:"+msgLoc+"  Method:"+method+"  DateTime:"+timestamp;
            logMsg += "\nERROR MSG:"+msg;

        //write to log file
        this.fs.appendFile("./logs/"+this.logFile,logMsg,(err)=>{/*console.log(err);*/});
    }
}

//export
module.exports = ServerLogger;