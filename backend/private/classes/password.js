/*
 * This Class performs all password operations
 * Some operations include ( encrypting a password and comparing two passwords)
 * 30-07-2017
 */
class PasswordClass
{    
    //constructor
    constructor()
    {
       //define attributes
       this.bcrypt = null;
       this.originalPassword = null;
       this.hashedPassword = null;

       //require bcrypt API
       this.setBcryptAPI()
    } 

    /*
     * Set password fields (a plaintext pass and a hashedOne)
     * @{params} - originalPassword (plain string),hashedPassword(string/hashed)
     * @{return} - void
     */
    setPasswordFields(originalPassword,hashedPassword)
    {
       this.originalPassword = originalPassword;
       this.hashedPassword = hashedPassword;
    }

   /*
    * Require dependency(bcrypt) and assign it to bcrypt
    * @{parameters} - none
    * @{return } - none
    */
    setBcryptAPI()
    {
       try {
           this.bcrypt = require("bcrypt");
       }catch(e) {
          console.log('ERROR when requiring bcrypt-nodejs.js in password.js');
          console.log('Error details:=>'+e)
        }
     }
      
    /*
     * This method encrypts the original password(plain text) using bcrypt 
     * @{params} - password
     * @{return} - encrypted password - string (synchronously)
     */
    encryptPassword(password)
    { 
        //encrypt password
        //this is the work factor for the salt
       let salt = this.bcrypt.genSaltSync(10);

       //encrypt the plain unencrypted password
       return this.bcrypt.hashSync(password,salt);
    }
    
    /*
     * This method encrypts the original password(plain text) asynchronously
     * @{params} - none
     * @{return} - encrypted password - string (asynchronously)
     */
    encryptPasswordAsync(password,cb)
    { 
       //encrypt password
       //this is the work factor for the salt
       let salt = this.bcrypt.genSaltSync(10);

      this.bcrypt.hash(password,salt,null,function(err,hash)
      {
      	   if (error)
      	   {
      	      //error handler goes here
              console.log('Password could not be hashed asynchronously');
              console.log('This is the error :=> '+ error);

      	      return cb(null);      	  	      	         	  	
      	   }
      	   else
      	   {      	   	
          	  return cb(hash);
      	   }
      });

    }

    /*
     * THis method compares two passwords using bcrypt
     * The plaintext and the hashed passwords
     * @{params} - plaintext and hashed password
     * @{return} - boolean (synchronous)
     */
     compareTwoPasswords(plaintext,hashedPassword)
     {
        //compare hashed password with plain  password
        return this.bcrypt.compareSync(plaintext,hashedPassword);
     }

    /*
     * THis method compares two passwords using bcrypt
     * The plaintext and the hashed passwords
     * @{params} - plaintext and hashed password
     * @{return} - boolean (asynchronous)
     */
     compareTwoPasswordsAsync(plaintext,hashedPassword,cb)
     {
        //compare plain  password with hashed password
        this.bcrypt.compareSync(plaintext,hashedPassword,function(error,isMatched)
        {
      	   if (error)
      	   {
            //handle errors here
       	  	 console.log('Problem with password compare bcrypt');
      	     console.log('This is the error :=> '+ error);

      	     return cb(isMatched);  
      	   }
      	   else
      	  	return cb(isMatched);
        });	

     }

}//end of class

//export class
module.exports = PasswordClass;