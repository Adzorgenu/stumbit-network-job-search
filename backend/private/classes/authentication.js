/*
 * Require dependencies: jsonwebtoken
 */
var jwt = require("jsonwebtoken");

/*
 * This is a class for authenticating users.
 * Token expires in 30 minutes. This means 30min of inactivity i.e when no request is made to the server
 * Token is always refreshed whenever a request is made to server.
 */
AuthClass  = function(userId)
{
    //set user id and secrecret key
    this.userId = userId;    
    this.secretKey =  "gJ5pZqZ3nR-RO7g7swY2F-zOIMxkWmI8-mwnNKjXikC-0VSkbejM1Q";
}

/*
 * This method signs/generates the token
 */
AuthClass.prototype.signToken = function()
{
    //jwt takes payload,secret key,options, and an optional callback;
    //expires in 30min;
    var token = jwt.sign({userId:this.userId},
                          this.secretKey,
                         {expiresIn:30*60});
    return token;
}

/*
 * Backdate the token
 */
AuthClass.prototype.backDateToken = function()
{
    //jwt takes payload,secret key,options, and an optional callback;
    //expires in 30min;
    //set issue at to 30 min earlier
    var token = jwt.sign({userId:this.userId,iat: Math.floor(Date.now() / 1000) - (30 * 60)},
        this.secretKey,
        {expiresIn:30*60});

    return token;
}

module.exports = AuthClass;