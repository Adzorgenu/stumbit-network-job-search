//require server logger file
var ServerLogger = new (require("./ServerLogger")) ();
var _ = require('lodash');
 
class UserRegistration
{
  //constructor
   constructor(id,connection)
   {
        this.userID = id;

        //require the db_connection
        this.connection = connection;

        //drivers
        this.userRegistrationDriver = null;

        //set driver
        this.setUserRegistrationDriver();
   }


   /*
   * Require userRegistrationDriver model from models directory
   * @{parameters} - none
   * @{return } - none
   * 11-08-2017
   */
   setUserRegistrationDriver()
   {
      try 
      {
         //require and instantiate user-registration-model-driver right away
         let DClass = require("../model-drivers/user-registration");
         this.userRegistrationDriver = new DClass (this.connection);
      }catch(error) 
      {
            ServerLogger.writeToLog(__filename,"setUserRegistrationDriver",error);
      }
   }//end of method

   /*
   * Get user registration email from the db
   * @{parameters} - none
   * @{return } - callback(results/false)
   * 11-08-2018
   */
   getUserRegisteredEmail(cb)
   {
      //get user registration email information   
      this.userRegistrationDriver.getUserRegisteredEmail(this.userID)
        .then(function(results)
          {
            let res = (_.isEmpty(results) || _.isNull(results)) ? "": results.email; 
            cb(res);            
          })
          .catch(function (error) {
            // console.log(error);
            //write log the error to a file
            ServerLogger.writeToLog(__filename,"getUserRegisteredEmail",error);
            cb(false);
        });
   }

}//end of class

module.exports  = UserRegistration;