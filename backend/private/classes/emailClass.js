/*
 * Include required files
 * Use Nodemailer to send email to clients
 * Transporter to be used is mailgun
 */

 //I've commented out eh templates for now.
 //we'll have a nicer html for sending our emails later.
var nodemailer = require("nodemailer");
// var EmailTemplate = require('email-templates').EmailTemplate;
var mailGun = require('nodemailer-mailgun-transport');
// var path = require('path');
// var templateDir = path.join(__dirname, '../../public/templates/email/confirm-email');

//smtp config
// var smtpConfig = {
//     host: 'smtpout.secureserver.net',
//     port: 80,
//     secure: false,  // use SSL works on port 465 for godaddy ; 80 for unsecured connection
//     auth: {
//         user: '',
//         pass: ''
//     }
// };


// This is your API key that you retrieve from www.mailgun.com/cp (free up to 10K monthly emails)
//this will be changed
var auth = {
    auth: {
        api_key:'key-ad57d22cb9e77bf968f04e04e1b5f5fc',
        domain: 'sandboxac2583fc8bb54562a2aec41ceaaffa8c.mailgun.org'
    }
};


//var transporter = nodemailer.createTransport(smtpConfig);
var transporter = nodemailer.createTransport(mailGun(auth));

/*
 * This function handles the generating of html from templates
 */
//email contents
function mailBody(id)
{
    let body =  '<div>';
        body += '<h2>Email Verification </h2>';
        body += '</div>';
        body += '<div>';
        body += '<p class="bord">';
        body += 'Your account needs to be activated.<br> Please click on this button to verify your Stumbit Account . <br> <br>';
        body += '<a href="http://localhost:3000/accounts/verify?id='+id+'" title="Verify Account" class="custom-button">';
        body += ' Verify your account';
        body += '</a>';
        body += '</p>';
        body += '</div>';

        return body;
}

function emailContents(emailTo, userId,callback)
{
    // //verification email and its context
    // var verification = new EmailTemplate(templateDir);
    // var context = {userId:userId};

    //     //render html from my template
    // verification.render(context, function (err, result)
    // {
    //     //email contents
    //     var emailContents =
    //     {
    //         from:"Stumbit Network <clients@edugren.com>",
    //         to:emailTo,
    //         subject:"Account verification by Stubmit",
    //         html:result.html,
    //         replyTo:"do-not-reply@stumbit.com",
    //         alternatives: [{
    //             filename: 'font.jpg',
    //             path: path.join(__dirname, '../../public/templates/email/font.JPG'),
    //             cid: 'font-pic@kreata.ee' //same cid value as in the html img src
    //         }]
    //     };


    //     return callback(emailContents);
    // });


    var emailContents =
    {
        from:"Stumbit Network <clients@edugren.com>",
        to:emailTo,
        subject:"Account verification by Stubmit",
        html:mailBody(userId),
        replyTo:"do-not-reply@stumbit.com"
    };

    return callback(emailContents);
}

//send email
function sendEmail(emailTo,userid,ServerLogger,callback)
{
   var emailData = emailContents(emailTo,userid,function(emailData)
   {
     //sendMail returns a promise if a callback function is not passed
       var sendEmail = transporter.sendMail(emailData);

           sendEmail.then(function (success)
           {
               //for debugging purposes
               //console.log(success);

            //note you return callback function since this is an asynchronousCall
              return callback(true);

           },function (err)
           {
                 //just for debugging purposes console.log(err);
                //  console.log(err);                 
                 ServerLogger.writeToLog(__filename,"sendEmail",err);
                 
               //note you return callback function since this is an asynchronousCall
              return callback(false);
           });//end of promise
   });//end of generating email contents
}

        //export the send function;
module.exports = sendEmail;