/*
 * This class handles all job posting activities by a/the company
 * Operations include posting a new job,reviewing an already posted job,displaying job on applicants board etc
 */

 function JobPostingClass(userId,jobId)
 {
    this.userId = userId;
    this.jobId  = jobId;

     //require mongoose
     this.mongoose = require("mongoose");

     // Use native promises
     this.mongoose.Promise = global.Promise;

     //require the db_connection
     this.connection = require("../configuration/db_connection");

 }

/*
 * This method defines a new basic job post schema
 * This schema will be used in the entire job posting section
 */
 JobPostingClass.prototype.newJobPostSchema = function ()
 {
     //define the schema
     var Schema = this.mongoose.Schema;


          //define basic/advanced job requirement schema
    var basicJobReqSchema = new Schema({fieldName:String},{_id:false,autoIndex:false,validateBeforeSave:false});
    var advJobReqSchema   = new Schema({fieldName:String,acceptedType:String},{_id:false,autoIndex:false});

          //define job requirement schema
    var jobReqSchema = new Schema({
           basicReq:[basicJobReqSchema],
           advancedReq:[advJobReqSchema]
        },{_id:false});

         //define job salary schema
     var salarySchema = new Schema({
         addSalary:{type:String,required:true},
         salaryCurrency:{type:String},
         salaryAmount:{type:String}
     },{_id:false});

          //define job Post Schema
     var jobPost = new Schema({
         jobTitle:{type:String,required:true},
         jobType:{type:String,required:true},
         jobValidity :{type:String,required:true},
         jobDescription:{type:String,required:true},
         jobSalary: [salarySchema],
         jobRequirements:[jobReqSchema],
         companyId:{type:Schema.Types.ObjectId,ref:'userRegistration'},
         postedDate:{type:Date,default:Date.now()}
     }, { collection: 'companyjobpostings' });

     //.model create an instance
     var jobPostSchema = this.connection.model("companyJobPosting", jobPost);

     return jobPostSchema;
 }

/*
 * This method adds a new job job post.
 * I mean basic one
 * jData is an object which holds the necessary information
 */
 JobPostingClass.prototype.addNewBasicJobPost = function(jData,callback)
 {
       //call schema function
       var jobModel = this.newJobPostSchema();
       var userId   = this.userId;

        var newJobModel = new jobModel({
            jobTitle:jData.jTitle,
            jobType:jData.jType,
            jobValidity :jData.jValidity,
            jobDescription:jData.jDesc,
            jobSalary: [{addSalary:jData.jAddSal,salaryCurrency:jData.jSalCurr,salaryAmount:jData.jsalary}],
            jobRequirements:[{}],
            companyId:userId
        });

        //console.log(newJobModel);
        newJobModel.save()
        .then(function(results)
        {
           return callback(results._id,{response:false,errMsg:""});

        }).then(null, function (err)
        {
           return callback("",{response:true,errMsg:err});
        });
 }


 /*
  * This method updates a company's job post with job requirements
  * Both basic and advanced requirement
  */
JobPostingClass.prototype.addJobRequirements = function(basicReqObj,advReqObj,callback)
{
    //call schema function
    var jobModel = this.newJobPostSchema();
    var jobId   = this.jobId;
    var userId  = this.userId;

      jobModel.update({_id:jobId,companyId:userId},
                      {$set:{
                              "jobRequirements.0":{
                                                    basicReq:basicReqObj,
                                                    advancedReq:advReqObj
                                                  }
                            }
                      },{multi:false,upsert:false}
                     ).exec()
          .then(function(data)
          {
                //second arg represent an err response
             if(data.nModified == 1)
               return callback(true,{response:false,errMsg:""});
             else if(data.nModified == 0)
               return callback(false,{response:false,errMsg:""});

          }).then(null,function(err)
          {
             return callback(false,{response:true,errMsg:err});
          });
}


/*
 * This method updates a company's basic job information
 * THis basic info includes jobtitle,jobtype,jobvalidity,jobdescription and jobsalary
 */
JobPostingClass.prototype.updateJobRequirements = function(jData,callback)
{
    //call schema function
    var jobModel = this.newJobPostSchema();
    var jobId   = this.jobId;
    var userId  = this.userId;


    jobModel.update({_id:jobId,companyId:userId},
                    {$set:{
                            jobTitle:jData.jTitle,
                            jobType:jData.jType,
                            jobValidity :jData.jValidity,
                            jobDescription:jData.jDesc,
                            "jobSalary.0": {
                                             addSalary:jData.jAddSal,
                                             salaryCurrency:jData.jSalCurr,
                                             salaryAmount:jData.jsalary
                                           }
                           }
                    },{multi:false,upsert:false}
                   ).exec()
        .then(function(data)
        {
            //second arg represent an err response
            if(data.nModified == 1)
                return callback(true,{response:false,errMsg:""});
            else if(data.nModified == 0)
                return callback(false,{response:false,errMsg:""});

        }).then(null,function(err)
    {
        return callback(false,{response:true,errMsg:err});
    });
}

/*
 * This function returns a specific job information(details)
 */
 JobPostingClass.prototype.getJobInformation = function(callback)
 {
     //call schema function
     var jobModel = this.newJobPostSchema();
     var jobId   = this.jobId;
     var userId  = this.userId;

            //second callback param is an error response
        jobModel.findOne({_id:jobId,companyId:userId},{__v:0,companyId:0,_id:0}).exec()
            .then(function(results){
                return callback(results,{response:false,errMsg:""});
            })
            .then(null,function(err){
                return callback("",{response:true,errMsg:err});
            });
 }

/*
 * This method is equivalent to getJobInformation method.
 * The only difference is, this method fetches only the job information(no requirements)
 */
JobPostingClass.prototype.getJobInformationOnly = function(callback)
{
    //call schema function
    var jobModel = this.newJobPostSchema();
    var jobId   = this.jobId;
    var userId  = this.userId;

    //second callback param is an error response
    jobModel.findOne({_id:jobId,companyId:userId},{__v:0,companyId:0,_id:0,jobRequirements:0,postedDate:0}).exec()
        .then(function(results){
            return callback(results,{response:false,errMsg:""});
        })
        .then(null,function(err){
            return callback("",{response:true,errMsg:err});
        });
}


/*
 * This method is equivalent to getJobInformation method.
 * The only difference is, this method fetches only the job requirements(no basic info)
 */
JobPostingClass.prototype.getJobRequirements = function(callback)
{
    //call schema function
    var jobModel = this.newJobPostSchema();
    var jobId   = this.jobId;
    var userId  = this.userId;

    //second callback param is an error response
    jobModel.findOne({_id:jobId,companyId:userId},{__v:0,companyId:0,_id:0,jobTitle:0,jobType:0,jobValidity:0,
                      jobDescription:0,jobSalary:0,postedDate:0}).exec()
        .then(function(results){
            return callback(results,{response:false,errMsg:""});
        })
        .then(null,function(err){
            return callback("",{response:true,errMsg:err});
        });
}


 /*
  * This method prepares a result fetch from the db an presents in  usable manner
  */
JobPostingClass.prototype.formatResult = function(result)
{
       //prepare the data
      var myResults = [];
      var requirement = [];

                //pus job title
        myResults.push({jobTitle:result.jobTitle});

                    //push job type
        if (result.jobType == "part")
            myResults.push({jobType:"Part Time"});
        else
            myResults.push({jobType:"Full Time"});


        //push job validity
        //use moment.js to parse and validate date
        var moment = require("moment");
        myResults.push({jobValidity:moment(result.jobValidity).format("dddd, MMMM Do YYYY")});

        //push job description
        myResults.push({jobDescription:result.jobDescription});

       //push job salary
       if(result.jobSalary[0].addSalary == 'false')
          myResults.push({jobSalary:"Not Available"});
        else
           myResults.push({jobSalary:result.jobSalary[0].salaryCurrency+' '+result.jobSalary[0].salaryAmount});

      //console.log(result.jobRequirements[0].basicReq[0]);

    //require lodash
    var _ = require("lodash");

    //deal with job requirements

    //push basic requirements
    _.forEach(result.jobRequirements[0].basicReq,function(value)
    {
        requirement.push({fieldName:value.fieldName});
    });

    //push advanced requirements ie if there is
    _.forEach(result.jobRequirements[0].advancedReq,function(value)
    {
        requirement.push({fieldName:value.fieldName});
    });

    //finally, push requirement into myResult array
    myResults.push(requirement);

    return myResults;
}

/*
 * This method prepares a result fetch from the db an presents in  usable manner
 * The job requirement is formatted in a usable way(by handlebars)
 */
JobPostingClass.prototype.formatJobRequirement = function(result)
{
    //prepare the data
    var myResults = [];
    var basicRequirement = [];
    var advRequirement = [];
    
    //push advanced job requirements size
    myResults.push({advancedReqSize:result.jobRequirements[0].advancedReq.length});

    //require lodash
    var _ = require("lodash");

    //push advanced job requirements
    //push advanced requirements ie if there is
    _.forEach(result.jobRequirements[0].advancedReq,function(value)
    {
        advRequirement.push({fieldName:value.fieldName,acceptedType:value.acceptedType});
    });

    //push advancedReq array into myResults array
     myResults.push(advRequirement);


    //push basic requirements
    _.forEach(result.jobRequirements[0].basicReq,function(value)
    {
        basicRequirement.push(value.fieldName);
    });

    //push advancedReq array into myResults array
    myResults.push(basicRequirement);

     return myResults;
}

 //export class as a module
module.exports = JobPostingClass;