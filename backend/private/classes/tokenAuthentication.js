/*
 * This is a class for token creation and validation.
 * Token expires in 30 minutes.
 * This means 30min of inactivity i.e when no request is made to the server
 * Token is always refreshed whenever a request is made to server.
 * 30-07-2017
 */

class TokenAuth
{
	//payload is an obj/json
   constructor()
   {
      //Require dependency: jsonwebtoken
      try {
           this.jwt = require("jsonwebtoken");
      } catch(err) {
          console.log("ERROR: tokenAuthentication.js");
          console.log("Details:=> "+err);
      }


      //require necessary vars
      this.secretKey = null;
      this.payload = null;
      this.token = null;

      //set jwt config
      this.config();
   }

   /*
    * Sign/generate the token given the payload
    * @{params} - none
    * @{return} - token(string);
    * 30-07-2017
    */
	signToken()
	{
	    //jwt takes payload,secret key,options, and an optional callback;
	    //expires in 30min;
	    // this.token = this.jwt.sign(this.payload,this.secretKey,{expiresIn:30*60});
      this.token = this.jwt.sign(this.payload,this.secretKey);

	    return this.token;
	}

   /*
    * Verify the token
    * @{params} - none
    * @{return} - callback
    * 31-07-2017
    */
	verifyToken(cb)
	{
	    //verify the token
	    this.jwt.verify(this.getToken(), this.secretKey, function(err, decoded)
	    {
	      if (err) {
	        console.log("error in token auth class: => "+err);
          return cb(err,null);
	      } else
	      {
	        return cb(null,decoded);
	      }
	    });
	}


   /*
    * Set config parameters of jwt
    * @{arams} - none
    * @{return} - void
    * 30-07-2017
    */
   config()
   {
   	  //set secret key
      this.secretKey =  "gJ5pZqZ3nR-RO7g7swY2F-zOIMxkWmI8-mwnNKjXikC-0VSkbejM1Q";
   }

   /*
    * Set Payload
    * @{params} - json/obj payload
    * @[return] - void
    * 30-07-2017
    */
    setPayload(payload)
    {
      this.payload = payload;
    }

   /*
    * Get Payload
    * @{params} - json/obj payload
    * @[return] - void
    * 30-07-2017
    */
    getPayload()
    {
    	return this.payload;
    }

   /*
    * Set token
    * @{params} - json/obj payload
    * @[return] - void
    * 30-07-2017
    */
    setToken(token)
    {
    	this.token = token;
    }

   /*
    * Get Token
    * @{params} - json/obj payload
    * @[return] - void
    * 30-07-2017
    */
    getToken()
    {
    	return this.token;
    }

} //end of class

//export class
module.exports = TokenAuth;
