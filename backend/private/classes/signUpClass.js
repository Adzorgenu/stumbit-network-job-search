
/*
 * This class coordinates all activities involved in successfully
 * signing up/ creating an account with stumbit network
 */
 //require server logger file
 var ServerLogger = new(require("./ServerLogger")) ();
 var _= require("lodash");
 
class SignUpClass
{
        //constructor
   constructor(registrationType,email,password,id,connection)
   {
        this.email = email;
        this.password = password;
        this.registrationType = registrationType;
        this.userId = id;

        //mongoose
        this.mongoose = require("mongoose");

        // Use native promises
        this.mongoose.Promise = require('bluebird');

        //require the db_connection
        this.connection = connection;
   }

   /*
    *  Define the db schema
    *  User's password is also encrypted
    */
    UserSchema()
    {
       return  require("../models/user-registration-model")(this.connection);
    }//end of user schema

    /*
    * This method save the records
    * Email is sent to the user for verification
    */
    signUpUser(cb)
    {
        let regSchema = this.UserSchema();

        //require password class
        //require and instantiate on the same line
        let passwordClass = new (require("./password")) ();
        

        let newSignUp = new regSchema({
            role : this.registrationType,
            email : this.email,
            password: passwordClass.encryptPassword(this.password),
            activated:false,
            registeredDate:Date.now()
        });

        //after saving
        newSignUp.save().then(function (results)
        {   
            //comment the sending of emails for now
            // require("emailClass")(email, result._id,function(emailSender){

            //send email account verification to user
            var path = require('path');
            //include user's email, and id
            //send email to user
            //pass serverLogger to log any error
            require( path.resolve( __dirname, "emailClass.js" ) )(results.email, results._id,ServerLogger,function(emailSender)
            {
                //check on success
                if(emailSender)
                {
                   return cb(true,"yesEmailSent","");
                }
                else
                {
                   return cb(true,"noEmailSent",results._id);
                }

            });

        }).catch(function(err)
        {   
            //save record error
            //write the errors to a log file later
            // console.log(err);
            ServerLogger.writeToLog(__filename,"saveRecord",err);
            
            return cb(false,"saveErr","");
        });

    }//end of function

    /*
    * This method resends a verification email to the user
    * Email is sent to the user for verification
    *@return {callback(success,activated/emailsent,userId,optionalArgForActivated)}
    */
    resendUserEmail(callback)
    {
        let userId = this.userId;

        //create and instantiate another mongoose schema
        let regSchema = this.UserSchema();
        
        //get email with the given id
        //return result will be passed to the next promise function
        regSchema.findOne({_id:userId},{password:0}).exec()
        .then(function(results)
            {
                if(!_.isEmpty(results))
                {
                    //if account is already verified, move to login
                    if(results.activated)
                    {
                        return callback(true,"activated","",{id:_.toString(userId),
                               role:results.role,email:results.email});                        
                    }
                    else
                    {
                        //send email account verification
                        //require("emailClass")(results.email,userId, function (emailSender)

                        var path = require('path');
                        require( path.resolve( __dirname, "emailClass.js" ) )(results.email,userId,ServerLogger,function (emailSender)
                        {
                            //check on success
                            if (emailSender) {
                                return callback(true,"yesEmailSent", "",{});
                            }
                            else {
                                return callback(true,"noEmailSent", userId,{});
                            }
                        });
                    }
                }
                else {
                    //id does not exist
                    return callback(true,"noEmailSent", userId,{});                    
                }

            }).catch(function (err)
            {
                console.log(err);
                ServerLogger.writeToLog(__filename,"resendEmailToUser",err);
                
                return callback(false,"","",{});
            });
    }//end of function

    /*
    * This method deals with email verification
    *@return{success{Bool},verified{Bool},email{String},role{string}}
    */
    verifyUser(callback)
    {
        //create and instantiate another mongoose schema
        let regSchema = this.UserSchema();        
        let userId = this.userId
        //create the model
        // var emailModel = connection.model("userRegistration",newemailSchema, "userRegistration");

        //get Id of added user
        //return result will be passed to the next promise function
        //new returns the update result
        var query = regSchema.findOneAndUpdate({ _id: userId},
                                                { $set: {activated:true,activatedDate:Date.now()}
                                                },
                                            {new:true}).exec();

        // //when query is executed succesfully then show to user
        query.then(function(success)
        {
            //when update is successfull
            if(success && success.activated)
                return callback(true,true,success.email,success.role);
            else {
                    return callback(true,false,"","");
            }

        })
        .catch(function (err) {
            console.log(err);
            return callback(false,false,"","");
        });//end of query

    }//end of function

    //check whether an email exists or not
    //params:1=> operation was successful 2nd:=> email exists or not
    //@return {success{bool},exist{bool}}
    doesEmailExist(callback)
    {
        //define the schema
        var Schema = this.mongoose.Schema;
        var email = this.email;

            //load load lash
        var _ = require("lodash");

        var UserRegister = new Schema({email:{type:String}});

        //.model create an instance
        var signUpSchema = this.connection.model("userRegistration", UserRegister, "userRegistration");

        signUpSchema.find({email:email},{_id:0,role:0,password:0,activated:0}).limit(1).exec()
        .then(function(results)
        {
            if(_.isEmpty(results))
                return callback(true,false);
            else
                return callback(true,true);
        })
        .catch(function (err)
        {
                return callback(false,false);
        });
    }//end of function

 }//end of class

 //export class
module.exports = SignUpClass;
