/*
 * THis class logs information.
 * It is just like an access log.however, the information is logged into our db
 * We'll use this to track activity on our app
 */

 function DbLogger()
 {
     //mongoose
     this.mongoose = require("mongoose");

     // Use native promises
     this.mongoose.Promise = global.Promise;

     //require the db_connection
     this.connection = require("../configuration/db_connection");
 }

 DbLogger.prototype.logSignUp = function()
 {
     //define the schema
     var Schema = this.mongoose.Schema;

     var DbLog = new Schema({
         logType: {type: String},
         logDate: {type: Date, default: Date.now},
     });

     //.model create an instance
     var logInfo = this.connection.model("Logger", DbLog, "Logger");

     var newlogInfo = new logInfo({
         logType: "SignUp",
         logDate: Date.now,
     });

        //save info
     newlogInfo.save();
 }