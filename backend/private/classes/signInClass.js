/*
 * This class coordinates all activities involved in successfully
 * signing in  a user
 */
 //require server logger file
 var ServerLogger = new(require("./ServerLogger")) ();
 var _ = require("lodash");
 
class SignInClass
{
         //constructor
    constructor(email,password,connection)
    {
        this.email = email;
        this.password = password;

        //mongoose
        this.mongoose = require("mongoose");

        // Use bluebird promises
        this.mongoose.Promise = require("bluebird");

        //require the db_connection
        this.connection = connection;        
    }

    /*
    * Check in the db if user exists
    */
    userSchema()
    {
        return  require("../models/user-registration-model")(this.connection);
    }

    /*
    * check for password exactness
    * @param {}
    * @return {callback()}
    * arg1: operation successful arg 2: correctness/validity, arg3:isUserActivated, arg4: userRole in the Db
    * arg5:user/object id;
    *Note: Get the password of the email provided and check if provided password matches with db password
    */
    authenticateUser(callback)
    {
        var userModel = this.userSchema();
        let password = this.password;

        //require password class
        //require and instantiate on the same line
        let passwordClass = new (require("./password")) ();
        


        //fetch data and compare password
        userModel.findOne({email:this.email}).exec()
        .then(function(userData)
        {
            if(!_.isEmpty(userData))
            {
                //set plaintext and hashed
                let correct = passwordClass.compareTwoPasswords(password,userData.password);
    
                //note: callback has 3 arguments
                // arg1:operation successful ? arg 2: correctness/validity, arg3:isUserActivated, arg4: userRole in the Db
                //arg5:user/object id;
                if(correct && userData.activated)
                    return callback(true,true,true,userData.role,userData._id);
                else if(correct && !userData.activated)
                    return callback(true,true,false,userData.role,userData._id);
                else if(!correct)
                    return callback(true,false,false,"","");
    
            }
            else {
                return callback(true,false,false,"","");
            }
        })
        .catch(function(err)
        {
            ServerLogger.writeToLog(__filename,"authenticateUser",err);

            //this means that user email does not exist.
            return callback(false,false,false,"","");
        });
    }

}

//export class
module.exports  = SignInClass;