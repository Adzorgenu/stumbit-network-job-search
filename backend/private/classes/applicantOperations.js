//require server logger file
var ServerLogger = new (require("./ServerLogger")) ();
var _ = require('lodash');
 
class ApplicantOperations
{
  //constructor
   constructor(id,connection)
   {
        this.userID = id;

        //require the db_connection
        this.connection = connection;

        //drivers
        this.applicantDetailsDriver = null;
   }


   /*
   * Require applicantDetailsDriver model from models directory
   * @{parameters} - none
   * @{return } - none
   * 09-08-2017
   */
   setApplicantDetailsDriver()
   {
      try 
      {
         //require and instantiate class right away
         let DClass = require("../model-drivers/applicant-details-info");
         this.applicantDetailsDriver = new DClass (this.connection);
      }catch(error) 
      {
            ServerLogger.writeToLog(__filename,"setApplicantDetailsDriver",error);
      }
   }//end of method

   /* 
   * Update the applicant profile
   * @{params} - phase,userInfo(object)
   * @{return} - callback(boolean:succeeded/failed)
   * 02-08-2018
   */
   updateApplicantProfile(phase,profileID,userInfo,cb)
   {
      // console.log('in updateApplicantProfile');
      // console.log('phase:'+phase+"  , profileID:"+profileID);
      if(phase == 1)
      {
         //update all documents
         this.applicantDetailsDriver.updateApplicantBasicProfile(profileID,userInfo)
            .then(function (results) {
              cb((results)?true:false);
            })
            .catch(function (error) {
               // console.log(error);
               //write log the error to a file
               ServerLogger.writeToLog(__filename,"updateApplicantProfile:phase1",error);
               cb(false);
            });
      }
      else if(phase ==2)
      {
         // console.log('in phase 2 block');
         //update all documents
         this.applicantDetailsDriver.updateApplicantContactProfile(profileID,userInfo)
            .then(function (results) {
              cb((results)?true:false);
            })
            .catch(function (error) {
               // console.log(error);
               //write log the error to a file
               ServerLogger.writeToLog(__filename,"updateApplicantProfile:phase2",error);
               cb(false);
            });         
      }
      else if(phase == 3)
      {
         // console.log('in phase 3 block');
         //update all documents
         this.applicantDetailsDriver.updateApplicantResidenceProfile(profileID,userInfo)
            .then(function (results) {
              cb((results)?true:false);
            })
            .catch(function (error) {
               // console.log(error);
               //write log the error to a file
               ServerLogger.writeToLog(__filename,"updateApplicantProfile:phase3",error);
               cb(false);
            });                  
      }
   }//end of method

  /* 
   * Controller Update the applicant profile
   * @{params} - phase,userInfo(object)
   * @{return} - callback(boolean:succeeded/failed)
   * 02-08-2018
   */
   updateApplicantProfileController(mode,phase,profileID,userInfo,cb)
   {
      let that = this;
      //first check if data exsist before inserting
      //if exists, then udpate, else insert
      this.getApplicantProfileController(phase,function(results)
      {
         if(results.isEmpty){//insert if no doc was found
            that.insertApplicantProfile(phase,userInfo,function(res){
               cb(res); //res is a boolean
            });               
         }else{
            that.updateApplicantProfile(phase,profileID,userInfo,function(res){
               cb(res); //res is a boolean
            });               
         }
      });
   }//end of method


   /* 
   * Insert new applicant profile
   * @{params} - userInfo(object)
   * @{return} - callback(succeeded/failed)
   * 02-08-2018
   */
   insertApplicantProfile(phase,userInfo,cb)
   {
      if(phase == 1)
      {
         //insert document
         this.applicantDetailsDriver.insertNewApplicantBasicProfile(userInfo)
            .then(function (results) {
              cb((results)?true:false);
            })
            .catch(function (error) {
               // console.log(error);
               //write log the error to a file
               ServerLogger.writeToLog(__filename,"insertApplicantProfile:phase1",error);
               cb(false);
            });

      }
      else if(phase == 2)
      {
         //insert document
         this.applicantDetailsDriver.insertNewApplicantContactProfile(userInfo)
            .then(function (results) {
              cb((results)?true:false);
            })
            .catch(function (error) {
               // console.log(error);
               //write log the error to a file
               ServerLogger.writeToLog(__filename,"insertApplicantProfile:phase2",error);
               cb(false);
            });
      }
      else if(phase == 3)
      {
         //insert document
         this.applicantDetailsDriver.insertNewApplicantResidenceProfile(userInfo)
            .then(function (results) {
              cb((results)?true:false);
            })
            .catch(function (error) {
               // console.log(error);
               //write log the error to a file
               ServerLogger.writeToLog(__filename,"insertApplicantProfile:phase3",error);
               cb(false);
            });         
      }
   }//end of method

  /* 
   * Controller - get the applicant profile
   * @{params} - phase
   * @{return} - callback({results,isEmpty}/false)
   * 08-08-2018
   */
   getApplicantProfileController(phase,cb)
   {
      //returned data for each phase
      let returned = null;
      if(phase == 1)
      {
         returned = {"firstName": 1,"middleName":1,"surName":1,"birthdate":1,"marital_status":1,"sex":1,
         "level_education":1,"level_education_other":1,"occupation_status":1,"occupation_status_other":1
         };
      }
      else if(phase == 2)
      {
         returned={"post_address":1,"alt_post_address":1,"email":1,"alt_email":1,"phone":1,"alt_phone":1};
      }
      else if(phase == 3)
      {
         returned = {"city":1,"nationality":1,"country_of_residence":1};
      }
      else if(phase == 4)
      {
         returned = {"cv_fileName":1,"degree_fileName":1};
      }  

      //get applicant profile   
      this.applicantDetailsDriver.getApplicantProfile(this.userID,returned)
         .then(function(results)
         {
            let isEmpty = (_.isEmpty(results) || _.isNull(results)) ? true: false;

            cb({res:results,isEmpty:isEmpty});            
         })
         .catch(function (error) {
            // console.log(error);
            //write log the error to a file
            ServerLogger.writeToLog(__filename,"getApplicantProfileController",error);
            cb(false);
         });
   }//end of method

  /* 
   * Controller - update the applicant profile files(cv & degree)
   * @{params} - phase
   * @{return} - callback(succeeded/failed)
   * 08-08-2018
   */
   updateApplicantFiles(profileID,userInfo,cb)
   {
      let that = this;

      if(profileID !== "")
      {
         //update applicant profile files
         this.applicantDetailsDriver.updateApplicantFilesProfile(profileID,userInfo)
            .then(function (results) {
              cb((results)?true:false);
            })
            .catch(function (error) {
               // console.log(error);
               //write log the error to a file
               ServerLogger.writeToLog(__filename,"updateApplicantFiles",error);
               cb(false);
            });         
      }
      else
      {
         //come back and check later
         cb(true);
      }
   }//end of method
}//end of class

module.exports  = ApplicantOperations;