/**
 * Created by Wisdom on 01/03/2017.
 * This class deals with user's account settings
 *  This include changing password and reseting account
 */

function AccountSetting (userid)
{
    //set user's id
    this.userId = userid;

    //require mongoose and bluebird
    this.mongoose = require("mongoose");
    this.promise = require("bluebird");

    // Use native promises
    this.mongoose.Promise = this.promise;

    //require the db_connection
    this.connection = require("../configuration/db_connection");
}

/*
 *  Define the db schema
 *  User's password is also encrypted
 */
AccountSetting.prototype.UserSchema = function()
{
    //define the schema
    var Schema = this.mongoose.Schema;

    var UserSchema = new Schema({email : {type:String},password: {type:String}});

    //.model create an instance
    var newUserSchema = this.connection.model("userRegistration", UserSchema, "userRegistration");

    return newUserSchema;
}

/*
 * This method checks of a user's password is correct
 * @arguments: userPassword and a callback
 */
AccountSetting.prototype.isUserPassCorrect = function(oldPass,callback)
{
    //new user model
    var UserSchema = this.UserSchema();

    //instantiate a password class
    var passwordClass = require("../classes/password");
    var newPasswordClass = new passwordClass();

    //get the old password using the id as the search param
    UserSchema.findOne({_id:this.userId},{__v:0,companyId:0,_id:0}).exec()
        .then(function(results)
        {
            //compare password
            newPasswordClass.comparePasswordAsync(oldPass,results.password,function (match)
            {
                return callback(match);
            })
        })
        .catch(function(err)
        {
            console.log("error in accountSettings.js" + err);
            return callback(false);
        });
}

/*
 * This method updates a user's password
 * @arguments: new password, callback
 */
 AccountSetting.prototype.updateUserPassword = function(newPassword,callback)
 {
     //new user model
     var UserSchema = this.UserSchema();

     //instantiate a password class and generate a new password hash
     var passwordClass = require("../classes/password");
     var newPasswordClass = new passwordClass();
     var hashPassword = newPasswordClass.generateNewHashSync(newPassword);

     //update user's password
     UserSchema.update({_id:this.userId},{$set:{"password":hashPassword}},{multi:false,upsert:false}).exec()
         .then(function(results)
         {
            console.log(results);

             //second arg represent an err response
             if(results.nModified == 1)
                 return callback(true);
             else if(results.nModified == 0)
                 return callback(false);
         })
         .catch(function(err)
         {
             console.log("there was an error in accountSettings.js in update function" + err);
             return callback(false);
         });
 }

//export class as a module
module.exports = AccountSetting;