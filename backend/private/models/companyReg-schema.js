var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var connection = require('../configuration/db_connection.js');
mongoose.connection = connection;

var newSchema = new mongoose.Schema({      
                                        companyName: String,  
                                        // country_of_location: String,
                                        address: String,
                                        alt_address: String,
                                        email:  String,
                                        phone: String,
                                        sector: String,
                                        region_of_location: String,
                                        category: String,
                                        alt_phone : String,
                                        description :  String,
                                        password : String,
                                        website : String,
                                    }, {
                                        timestamps: true
                                    });

var Companies  = mongoose.model('Company', newSchema, 'companydetails');
 
module.exports = Companies;