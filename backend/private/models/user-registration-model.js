module.exports = function(connection)
{
    var mongoose = require('mongoose');
    var Schema = mongoose.Schema;
    
    
    var UserRegistrationSchema = new Schema({
        role: {type: String, required: true },
        email: {type: String, required: true, index: {unique: true} },
        password: {type: String, required: true},
        activated: {type: Boolean},
        registeredDate: {type: Date},
        activatedDate: {type: Date},
        jobs: [{type: Schema.Types.ObjectId, ref: 'companyJobPosting'}],
        applicant_details: {type: Schema.Types.ObjectId, ref: 'applicantDetails'},
        company_details: {type: Schema.Types.ObjectId, ref: 'companyDetails'}
    });
    
    var userRegistration = connection.model("userRegistration", UserRegistrationSchema, "userRegistration");
    
    return userRegistration;
}
