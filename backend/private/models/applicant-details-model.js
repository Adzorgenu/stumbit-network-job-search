module.exports = function(connection)
{
   let mongoose = require('mongoose');

   //define promises
   mongoose.Promise = require('bluebird');
   let Schema = mongoose.Schema;

   let applicantDetailsSchema = new Schema(
      {
         userID: {type: mongoose.Schema.Types.ObjectId, ref: 'userRegistration'},
         firstName: {type:String,default:""},
         middleName: {type:String,default:""}, 
         surName: {type:String,default:""},
         birthdate: {type:String,default:""},
         marital_status: {type:String,default:""},
         sex: {type:String,default:""},
         region: {type:String,default:""},
         city: {type:String,default:""},
         nationality: {type:String,default:""},
         country_of_residence: {type:String,default:""},
         level_education: {type:String,default:""},
         level_education_other:{type:String,default:""},
         occupation_status: {type:String,default:""},
         occupation_status_other:{type:String,default:""},
         post_address: {type:String,default:""},
         alt_post_address: {type:String,default:""},
         email:  {type:String,default:""},
         alt_email:{type:String,default:""},
         phone: {type:String,default:""},
         alt_phone: {type:String,default:""},
         cv_fileName:{type:String,default:""},
         degree_fileName:{type:String,default:""}
    },
    {
    timestamps: true
    }
   );

   //we'll make the cv && cert an array so that we'll use for ML later
   //only the fileNames with(isCurrent) will be used;

   return connection.model('APPLICANT_DETAILS',applicantDetailsSchema,'APPLICANT_DETAILS');
};

