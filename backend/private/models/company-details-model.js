var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var connection = require('../configuration/db_connection.js');
mongoose.connection = connection;

var CompanyDetailsSchema = new mongoose.Schema({ 
                                                companyName: String,  
                                                address: String,
                                                alt_address: String,
                                                email :  String,
                                                phone : String,
                                                sector: String,
                                                region_of_location: String,
                                                category: String,
                                                alt_phone : String,
                                                description :  String,
                                                website : String
                                            }, {
                                                timestamps: true
                                            });

var companyDetails = mongoose.model("companyDetails", CompanyDetailsSchema);

module.exports = companyDetails;