var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var connection = require('../configuration/db_connection.js');
mongoose.connection = connection;

var newSchema = new Schema({
                                firstName: String,
                                middleName: String,
                                surName: String,
                                birthdate: String,
                                marital_status: String,
                                sex: String,
                                region: String,
                                city: String,
                                nationality: String,
                                country_of_residence: String,
                                level_education: String,
                                occupation_status: String,
                                address: String,
                                alt_address: String,
                                email: String,
                                phone: String,
                                alt_phone: String,
                                password: String,
                            }, {
                                timestamps: true
                            });

var Applicants = mongoose.model('Applicant', newSchema, 'applicantdetails');

module.exports = Applicants;