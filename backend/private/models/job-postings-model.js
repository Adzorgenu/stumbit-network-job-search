var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var connection = require('../configuration/db_connection.js');
mongoose.connection = connection;

var basicJobReqSchema = new Schema({
                                    fieldName: String
                                    }, {
                                        _id: false,
                                        autoIndex: false,
                                        validateBeforeSave: false
                                    });

var advJobReqSchema = new Schema({
                                    fieldName: String,
                                    acceptedType: String
                                }, {
                                    _id: false,
                                    autoIndex: false
                                });

var jobReqSchema = new Schema({
                                basicReq: [basicJobReqSchema],
                                advancedReq: [advJobReqSchema]
                            }, {
                                _id: false
                            });

var salarySchema = new Schema({
                                addSalary: {type: String, required: true},
                                salaryCurrency: {type: String},
                                salaryAmount: {type: String}
                            }, {
                                _id: false
                            });

var companyJobPostingSchema = new Schema({
                            jobTitle: {type: String, required: true},
                            jobType: {type: String, required: true},
                            jobValidity: {type: String, required: true},
                            jobDescription: {type: String, required: true},
                            jobSalary: [salarySchema],
                            jobRequirements: [jobReqSchema],
                            companyId: {type: Schema.Types.ObjectId, ref: 'userRegistration'},
                            postedDate: {type: Date, default: Date.now()}
                        });

var companyJobPosting = mongoose.model("companyJobPosting", companyJobPostingSchema);

module.exports = companyJobPosting;