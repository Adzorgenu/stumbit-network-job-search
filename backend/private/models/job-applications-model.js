var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var connection = require('../configuration/db_connection.js');
mongoose.connection = connection;

var basicRequirementsSchema = new Schema({  
                                            response: String
                                        });

var advancedRequirementsSchema = new Schema({
                                                response: String,
                                            });

var JobApplicationSchema = new mongoose.Schema({ 
                                                advanced_requirements: [ advancedRequirementsSchema ],  
                                                basic_requirements: [ basicRequirementsSchema ],
                                                company: {type: Schema.Types.ObjectId, ref: 'userRegistration'},
                                                applicant: {type: Schema.Types.ObjectId, ref: 'userRegistration'},
                                                job_posting: {type: Schema.Types.ObjectId, ref: 'companyJobPosting'}
                                            }, {
                                                timestamps: true
                                            });

var jobApplication = mongoose.model("jobApplication", JobApplicationSchema);

module.exports = jobApplication;