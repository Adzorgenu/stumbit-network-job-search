/*
 * This script establishes a connection with our mongoDB database
 * Used driver is mongoose
 */
var mongoose = require("mongoose");

//define connection variables
var username = "bossu";
var password = "nokofioGH";
var database = "stumbit_db_launch";

//localhost has the same address 127.0.0.1
var host = "127.0.0.1";
var port = "27017";


//create connection function
function createConnection(){
        // options
        var opts = { user: username, pass: password };

        //this returns the connection variable
        //return mongoose.createConnection(host, database, port, opts);
        return mongoose.createConnection(host, database, port);
}

//close connection function
function closeConnection(){
        mongoose.disconnect();
}

//create the connection itself
var connection = createConnection();

//export router
module.exports = connection;
