module.exports = function(app,express)
 {
     //define router
     let approuter = express.Router();
     let cors = require('cors');

    //serve asset folders .
    // approuter.use(express.static("public/lib"));
    approuter.use(express.static(__dirname, 'file_uploads'));
    //var functions = require("./private/functions/functionsClass.js");
  
    let bodyParser = require("body-parser");

    //USE BODY-PARSER 
    approuter.use(bodyParser.urlencoded({ extended:true}));
    approuter.use(bodyParser.json());


     // after the code that uses bodyParser and other cool stuff
     // this is my front-end url for development
     var originsWhitelist = ['http://localhost:4200','http://localhost:5200'];

     let corsOptions = {
         origin: function(origin, callback){
             var isWhitelisted = originsWhitelist.indexOf(origin) !== -1;
             callback(null, isWhitelisted);
         },
         credentials:true
     };

     //use cors
     approuter.use(cors(corsOptions));

     return approuter;
};