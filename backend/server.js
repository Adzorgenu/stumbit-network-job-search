/*
 * Include the necessary libraries,middlewares or frameworks
 * Require express
 */
var express = require("express");
var app = express();
var port = 3000;
var hostname = 'localhost';

//create database connection and set to locals to be used throughout the app
app.locals.connection = require('./private/configuration/db_connection');


/* =========================
 *  SERVER'S CONFIGURATIONS
 * =========================
 */
const config = require("./private/configuration/config")(app,express);
app.use(config);
  

/* ===================
 * Define all routes
 * ===================
 */
const routes = require("./private/routes/routes-definition")(app,express);
app.use(routes);


/*
 * Define localhost port: 3000
 */
app.listen(port,hostname,function()
{
    console.log("Listening on port "+port);
});