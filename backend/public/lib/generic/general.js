/*
 *  List of functions used in this application
 */

/*
 * This function adds a new job requirement in a table row
 * this function constructs the row and add its content to the table.The row number is also incremented
 */
function addNewJobRequirement(tableSelector,rowNoSelector)
{
    //fetch current row number;
    var currentRow = rowNoSelector.val();
    var newRow = Number(currentRow) + 1 ;

    var html  = '<tr class="actual">';
        html += '<td></td>';
        html += '<td><input type="text" name="field_'+newRow+'" class="form-control validate-input" value="" placeholder="input"></td>';
        html += '<td> <select name="fType_'+newRow+'" class="form-control validate-select">';
        html += '<option value="none" selected>None</option>';
        html += '<option value="numb">Number</option>';
        html += '<option value="numbRange">Number Range</option>';
        html += '<option value="word">Word(s)</option>';
        html += '<option value="YesNo">Yes/No Reply</option>';
        html += '<option value="file">File(pdf)</option>';
        html += '</select></td>';
        html += '<td>';
        html += '<div class="btn-group" data-toggle="buttons">';
        html += '<label class="btn btn-primary active">';
        html += '<input type="radio" name="add_'+newRow+'" value="on" autocomplete="off" checked>';
        html += '<i class="fa fa-plus"></i>';
        html += '</label>';
        html += '<label class="btn btn-primary removeReq">';
        html += '<input type="radio" name="add_'+newRow+'" value="off" autocomplete="off">';
        html += '<i class="fa fa-remove"></i>';
        html += '</label>';
        html += '</div>';
        html += '</td>';
        html += '</tr>';


            //append html to table
        tableSelector.append(html);


        //update row number in the hidden input form
        rowNoSelector.val(newRow);
}

/*
 * THis function removes a job requirment from a table
 * THe requirements and its row is taken out
 */
 function removeJobRequirement(remButSel)
{
    //move up four levels
    //first parent is div.btn-group and second is td;third is the row;
   remButSel.parent().parent().parent().remove();
}


/*
 * validate email using regular expressions
 */
function validateEmail(email)
{
    return (email.match(/^[A-Za-z0-9._-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/g)) ? true:false;
}


/*
 * this function validates a password
 * password should have a minimum of 8 characters
 */
function validatePassword(password)
{
    return (password.length >= 8 ) ? true:false;
}

/*
 * this function validates a retyped password. check for exactness
 */
function valideRetypePassword(password,retypedPass)
{
    return (password.length >= 8 && (retypedPass == password)) ? true:false;
}

/*
 * this function validates a word
 * only letters /alphabets are allowed
 */
function validateWord(word)
{
    return (word.match(/^[A-Za-z]+$/g)) ? true:false;
}

/*
 * This file sends an ajax request to the server
 * ajax reques is a post req.
 * Server responds with a message
 */
function doesEmailExist(url,data,callback)
{
    $.post(url,data,function (result)
    {
       //return callback;
        return callback(result);
    });
}

/*
 * This file sends an ajax request to the server
 * ajax request is a post req.
 * Server responds with a message
 * @arguments: url(path),data and callback
 */
function isOldPasswordCorrect(url,data,callback)
{
    $.post(url,data,function (result)
    {
       //return callback;
        return callback(result);
    });
}

/*
 * This function validate a string.
 * Only letters,numbers,spaces and hyphen is allowed
 * Function allso sets error message while validating
 */
function validateWords(field,selector,errorMsg)
{
    if(field.match(/^[A-Za-z0-9-,'";_:@$\(\)\?\s\n\t\r\.]+$/g))
    {
        Okay(selector);
        return true;
    }
    else
    {
        ShowError(selector,errorMsg);
        return false;
    }
}

/* This function validates a select field.
 * None select is seen as invalid
 * Regex determines valid characters
 */
function validateSelectTextField(value,selector,errMsg)
{
    if(value == "None" || value == "none")
    {
        ShowError(selector,errMsg);
        return false;
    }
    else if(!value.match(/^[a-zA-Z0-9_\.\s-]+$/g))
    {
        ShowError(selector,errMsg);
        return false;
    }
    else {

        Okay(selector);
        return true;
    }
}

/*
 * This function shows/hide a  field/element/item.
 * A toggling function
 */
 function toggleField(selector,value)
 {
    if(value == "on")
    {
      selector.css("display","block");
      //selector.show()
    }
    else if(value == "off")
    {
        selector.css("display","none");
        //selector.hide()
    }
 }

/*  This function loops through an options in a select field
 *  While looping, it compares each option value to the 2nd function param;
 *  Code makes a matched value the default selected option
 * the 1st param is not an actual selector but the string selector. eg ".sel option"
 */
function changeSelectOption(selectorString,compare)
{
    var found = false;
    //concatenate selector with :nth-child(1)
    var firstChild = selectorString + ":nth-child(1)";

    // Remove the selected option from the select form
    $(firstChild).removeAttr('selected');

    // Loop through all options . If any option's value matches
    // with  what it is being compared with say ""
    // make it the default select;
    $(selectorString).each(function ()
    {
        var SelOption = $(this).val();

        if(SelOption == compare)
        {
            //make it selected and stop the looping
            $(this).attr('selected','selected');
            found = true;
        }
    })

    //if not found reset to none
    if(!found){
        $(firstChild).attr('selected','selected');
    }
}

/*
 * This function validates jobTypes currently set by the system
 */
 function validateJobTypes(value,errSelector,errMsg)
 {
        //list of available job types
        var jobTypes = ["full","part"];

     if(value == jobTypes[0] || value == jobTypes[1])
     {
         Okay(errSelector);
         return true;
     }
     else
     {
         ShowError(errSelector,errMsg);
         return false;
     }

 }

/*
 * THis function validates the currency used on this application
 */
 function validateCurrency(currency,errSelector,errorMsg)
 {
     //acceptable currencies
     var currencyArray = ["GHC","$","£","€","₣","¥","₹"];

       //inArray function returns -1 if value is not found in the array
     if( currency == currencyArray[0] || currency == currencyArray[1] || currency == currencyArray[2] ||
         currency == currencyArray[3] || currency == currencyArray[4] || currency == currencyArray[5] ||
         currency == currencyArray[6])
     {
         Okay(errSelector);
         return true;
     }     else
     {
         ShowError(errSelector,errorMsg);
         return false;
     }

 }

/*
 * This function validates a number range. Acceptable inputs are numbers,spaces and a hyphen
 */
 function validateNumberRange(fieldValue,errSelector,errorMsg)
 {
    if(fieldValue.match(/^[0-9-,\s]+$/g))
    {
        Okay(errSelector);
        return true;
    }
    else
    {
        ShowError(errSelector,errorMsg);
        return false;

    }
 }

/* THis function validates a date
 * Returns true if date is valid or false if otherwise
 * Error message is also set if validation fails
 */
function ValidateDate(date,selector)
{
    //get day,month and year from date
    var day = date.slice(date.lastIndexOf('-')+1);
    var month = date.slice(date.indexOf('-')+1,date.lastIndexOf('-'));
    var year = date.slice(0,date.indexOf('-'));

    var valid = false;

    var DaysOfMonth = [31,28,31,30,31,30,31,31,30,31,30,31];

    var maxDay = DaysOfMonth[month-1];

    if((day == "") || (month == "") || (year == "") || (Number(month) != 2 && Number(day) > maxDay))
        valid = false;
    else
    {
        var LeapYear = ((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0) ? true : false;

        valid = ((LeapYear && (month == 2) && (day > 29)) || ((!LeapYear) && (month ==2) && (day >28))) ? false:true;
    }

    if(valid)
    {
        Okay(selector);
        return true;
    }
    else
    {
        ShowError(selector,"Incorrect Date");
        return false;
    }

}


/*
 * Error handler for forms
 */

/* THis function displays an error message on a page
 * Error is tied to a selector
 * Error is a popover error message
 */
function ShowError(selector,message)
{
    selector.css('border','1px solid #df1f1f');

    //selector.popover('destroy');

    selector.popover({
        placement:'bottom',
        content:message
    });

    selector.popover('show');

    //popver styles
    $(".popover").css({
        'background':'#b84d4d',
        'color':'#ffffff',
        'min-height':'35px'
    })
    $(".popover.bottom ").css('margin-top','9px');
    $(".popover-content ").css('padding','5px 7px');
}

/* THis function displays an okay message on a page
 * Okay message is tied to a selector
 * Okay message is a popover
 */
function Okay(selector)
{
    selector.popover('destroy');
    selector.css('border','1px solid #19a353');
}

/* THis function displays an a neutral color on a page
 * works with the border. Resets the border color
 */
function NeutralColor(selector)
{
    selector.popover('destroy');
    selector.css('border','1px solid #3da2a8');
}
