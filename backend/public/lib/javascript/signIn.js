/*
 * This front end js script handles all events required for a successful
 * login in of a user(could be an applicant or a company)
 * Validations are done
 */
$(document).ready(function()
{
    //get form fields
    var emailField = $("#email");

    /*
     * Perform a series of validation
     * and error checking
     */

    emailField.on('change blur',function ()
    {
        if(!validateEmail(emailField.val()))
          //show error information
            emailField.parent().next().html("Invalid email");
        else
            emailField.parent().next().html("");
    });


    /*
     * Validate form before submission
     */
    var form = $("#signInForm");

    form.submit(function()
    {
        if(!validateEmail(emailField.val()))
            return false;
        else
            return true;
    });

});