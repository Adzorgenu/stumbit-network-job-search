$(document).ready(function ()
{
    //hack for font awesome
    //to show and hide password contents
    //for old password
    var faToggle1 = false;
    var faToggle2 = false;
    var faToggle3 = false;

    //deal with toggling of password==>text or vice versa
    $("#oldPassSec .fa").on("click",function()
    {
        if(!faToggle1)
        {
            $(this).removeClass("fa-eye");
            $(this).addClass("fa-eye-slash");
            $("#oldPassSec #oldPass").prop("type","text");
            faToggle1 = true;
        }
        else if(faToggle1)
        {
            $(this).removeClass("fa-eye-slash");
            $(this).addClass("fa-eye");
            $("#oldPassSec #oldPass").prop("type","password");
            faToggle1 = false;
        }
    });

    //deal with toggling of password==>text or vice versa
    $("#newPassSec .fa").on("click",function()
    {
        if(!faToggle2)
        {
            $(this).removeClass("fa-eye");
            $(this).addClass("fa-eye-slash");
            $("#newPassSec #newPass").prop("type","text");
            faToggle2 = true;
        }
        else if(faToggle2)
        {
            $(this).removeClass("fa-eye-slash");
            $(this).addClass("fa-eye");
            $("#newPassSec #newPass").prop("type","password");
            faToggle2 = false;
        }
    });

            //deal with toggling of password==>text or vice versa
    $("#newConfirmPassSec .fa").on("click",function()
    {
        if(!faToggle3)
        {
            $(this).removeClass("fa-eye");
            $(this).addClass("fa-eye-slash");
            $("#newConfirmPassSec #newPassConfirm ").prop("type","text");
            faToggle3 = true;
        }
        else if(faToggle3)
        {
            $(this).removeClass("fa-eye-slash");
            $(this).addClass("fa-eye");
            $("#newConfirmPassSec #newPassConfirm ").prop("type","password");
            faToggle3 = false;
        }
    });

    // ----------- password section --------------
    //if hidden modal has value of yes, display sweet alert
    // var showModal = $("#showModal");
    // if(showModal.val() == "1")
    // {
    //     swal("Success", "Password has been updated", "success");
    // }
    // else if(showModal.val() == "-1")
    // {
    //     swal("Oops ...", "There was a problem . Password could not be updated", "error");
    // }
    //
    var oldPass = $("#oldPass");
    var newPass = $("#newPass");
    var newPassConfirm = $("#retypNewPass");
    var passwordForm = $("#changePassForm");

    //check for password validity
    oldPass.bind("change blur",function()
    {
        if(!validatePassword(this.val()))
           $(this).next().html("Invalid password");
        else
        {
            //send an ajax request to the server
            // check if old password is correct
            isOldPasswordCorrect("/accounts/ajaxChangePassHandler",{oldPass:$(this).val()},function(data)
            {
                if(!data.correct)
                    $(this).next().html("password is incorrect");
                else
                    $(this).next().html("");
            });
        }
    });

    //check for password validity
    newPass.bind("change blur",function()
    {
        if(!validatePassword(this.val()))
            $(this).next().html("Invalid password");
    });

    //check for password validity
    newPassConfirm.bind("change blur",function()
    {
        if(!validatePassword(this.val()))
            $(this).next().html("Invalid password");
         else
         {
                 //check it both passwords match
             if(valideRetypePassword($(this).val(),newPass.val()))
                 $(this).next().html("");
             else
                 $(this).next().html("password mismatch");
         }
    });

    /* ----------on submit of the form ----------------*/
    // passwordForm.submit(function(){
    //     if(!ValidateOldPass(oldPass.val(),oldPass) || $("#oldIncorrect").val() == "yes")
    //     {
    //         return false;
    //     }
    //     else if(!ValidateNewPass(newPass.val(),newPass))
    //     {
    //         return false;
    //     }
    //     else if(!ValidateRetypedPass(newPass.val(),retyped.val(),retyped))
    //     {
    //         return false;
    //     }
    //     else {
    //         return true;
    //     }
    //
    // });
});