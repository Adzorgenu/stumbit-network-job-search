$(document).ready(function()
{
    //define all available fields
    var jTitle    = $("#jobTitle");
    var jType     = $("#jType");
    var jAddSal   = $("#jAddSal");
    var toggleSal = $(".toggle-sal");
    var jsalary   = $("#jsalary");
    var jValidity = $("#jValidity");
    var jDesc     = $("#jDesc");
    var jPostForm = $("#jPostForm");
    var jaSalCurr = $("#jSalCurr");
    var hiddenJSalCurr = $("#hidJSal");

        //this is applicable when editing job post
       if(hiddenJSalCurr.val() !="")
       {
                //change selected option
           changeSelectOption("#jSalCurr option",hiddenJSalCurr.val());
       }

    //handle events
    //perform validations too
            //job title
    jTitle.on("change blur",function(){
        validateWords($(this).val(),$(this),"These characters {!@#$%^&*()+=<>?/|} are not allowed");
    });

            //job type
    jType.on("change blur",function(){
        validateJobTypes($(this).val(),$(this),"Invalid selection");
    });

            //add job salary
    jAddSal.on("change blur",function(){
        if($(this).val() == "true")
            //toggle salary field on
            toggleField(toggleSal,"on");
        else if($(this).val() == "false")
            //toggle salary field off
            toggleField(toggleSal,"off");

        Okay($(this));
    });


            //job post validity
    jValidity.on("change blur",function(){
        ValidateDate($(this).val(),$(this));
    });

            //job description
    jDesc.on("change blur",function(){
        validateWords($(this).val(),$(this),"These characters {!@#$%^&*()+=<>?/|} are not allowed");
    });

        //job salary (currency)
    jaSalCurr.on("change blur",function(){
        validateCurrency($(this).val(),$(this),"Please select on of the listed curriencies");
    });

            //job salary
    jsalary.on("change blur",function(){
        validateNumberRange($(this).val(),$(this),"Invalid range. {Sample range 1000 - 5000}");
    });

     //validate required fields
    //handle a submit event
    jPostForm.submit(function ()
    {
        var validCurrency= null;
        var validSalary = null;

        //set validity of currency field and job salary
        if(jAddSal.val() == "true")
        {
          validCurrency = validateCurrency(jaSalCurr.val(),jaSalCurr,"Please select on of the listed currencies");
          validSalary  = validateNumberRange(jsalary.val(),jsalary,"Invalid range. {Sample range 1000 - 5000}");
        }

       //perform validations
        if (!validateWords(jTitle.val(),jTitle,"These characters {!@#$%^&*()+=<>?/|} are not allowed"))
            return false;
        else if (!validateJobTypes(jType.val(),jType,"Invalid selection"))
            return false;
        else if (jAddSal.val() == "true" && (!validCurrency || !validSalary))
            return false;
        else if(!ValidateDate(jValidity.val(),jValidity))
            return false;
        else if(!validateWords(jDesc.val(),jDesc,"These characters {!@#$%^&*()+=<>?/|} are not allowed"))
            return false;
        else
            return true;
    });
});