$(function()
{
    //define various selectors
    var addReqButt = $(".add-requirements");  //add requirement button
    var addvTable  = $("#adv-req");           //add requirements table
    var remButt    = $(".removeReq");         // remove button(will be undefine at this moment)
    var row_no     = $("#row_no");            //this variable tells us the number of add rows.default is set to 0
    var jPostRForm = $("#jPostRForm");        //jobrequirement posting form
    var inputField = $(".validate-input");    //input field
    var selectField= $(".validate-select");   //select field


   //handle and adding of a requirement in a  table row
   addReqButt.on("click",function()
   {
       addNewJobRequirement(addvTable,row_no);

        //update the selector
       remButt   =  $(".removeReq");
       inputField = $(".validate-input");
       selectField= $(".validate-select");

       //handle the removal of a requirement other than the first req
       remButt.on("click",function()
       {
           removeJobRequirement($(this));
       });

       //validate form fields
        inputField.on("change blur",function(){
                //validate input field
            validateWords($(this).val(),$(this),"Invalid requirement.These special characters {!@#$%^&*()+=<>?/|} are not allowed");
        });

        selectField.on("change blur",function(){
                //validate select field
            validateSelectTextField($(this).val(),$(this),"Please select one of these options.");
        });

       //validate added requirements.
       jPostRForm.submit(function()
       {
            valid = true;

           //validate each input field and its corresponding select field
           inputField.each(function () {
               //get corresponding select field
               var selectF = $(this).parent().next().find("select");

               //prevent submission if validation fails
               if(!validateWords($(this).val(),$(this),"Invalid requirement.These special characters {!@#$%^&*()+=<>?/|} are not allowed") ||
                   !validateSelectTextField(selectF.val(),selectF,"Please select one of these options.") )
                   {
                       valid = false;
                       return false;            //this statement stops the loop
                   }
           });

            return (valid) ? true:false;

       });

   });

    //validate form fields
    inputField.on("change blur",function(){
        //validate input field
        validateWords($(this).val(),$(this),"Invalid requirement.These special characters {!@#$%^&*()+=<>?/|} are not allowed");
    });

    selectField.on("change blur",function(){
        //validate select field
        validateSelectTextField($(this).val(),$(this),"Please select one of these options.");
    });

    //validate added requirements.
    jPostRForm.submit(function()
    {
        valid = true;

        //validate each input field and its corresponding select field
        inputField.each(function () {
            //get corresponding select field
            var selectF = $(this).parent().next().find("select");

            //prevent submission if validation fails
            if(!validateWords($(this).val(),$(this),"Invalid requirement.These special characters {!@#$%^&*()+=<>?/|} are not allowed") ||
                !validateSelectTextField(selectF.val(),selectF,"Please select one of these options.") )
            {
                valid = false;
                return false;            //this statement stops the loop
            }
        });

        return (valid) ? true:false;

    });

});

