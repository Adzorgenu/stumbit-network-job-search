/*
 * This front end js script handles all events required for a successfull
 * signing of a user(could be an applicant or a company
 * Validations are done
 */
$(document).ready(function()
{
    //get form fields
    var emailField = $("#email");
    var passwordField = $("#password");
    var retPasswordField =  $("#retypePassword");
    var regType = $("#registrationType");

    /*
     * Perform a series of validation
     * and error checking
     * Disable the submit form by default
     */
     $("#signUp").addClass("disabled");

    //validate email
    emailField.change(function ()
    {
      if(emailField.val() == "")
          emailField.next().html("");
      else
       {
           if(!validateEmail(emailField.val()))
           //show error information
               emailField.next().html("Invalid email");
           else
           {
               //send an ajax request to the server checking for the status of the
               // of the email provided
               doesEmailExist("/accounts/ajaxHandler",{email:emailField.val()},function(data)
               {
                   if(data.exists)
                       emailField.next().html("Email exists");
                   else
                       emailField.next().html("");
               });
           }
       }

    });

    //validate password
    passwordField.change(function () {
      if(passwordField.val() == "")
          passwordField.next().html("");
      else
       {
           if(!validatePassword(passwordField.val()))
               passwordField.next().html("Minimum of 8 characters required");
           else
               passwordField.next().html("");
       }

    });

    //validate confirm password
    retPasswordField.change(function () {
        if(retPasswordField.val() == "")
            retPasswordField.next().html("");
        else
        {
            if(!validatePassword(retPasswordField.val()))
                retPasswordField.next().html("Minimum of 8 characters required");
            else
            {
                if(!valideRetypePassword(passwordField.val(),retPasswordField.val()))
                    retPasswordField.next().html("Password mismatch");
                else
                    retPasswordField.next().html("");
            }
        }
    });

    //handle 'I Agree checkbox'
    $("#Iagree").click(function ()
    {
       if($(this).prop('checked') && validateEmail(emailField.val()) && validatePassword(passwordField.val()) &&
           validatePassword(retPasswordField.val()) && valideRetypePassword(passwordField.val(),retPasswordField.val())
          )
          {
            //enable the submit button
            $("#signUp").removeClass("disabled");
          }
        else
           $("#signUp").addClass("disabled");
    });

    /*
     * Validate form before submission
     */
    var form = $("#signUpForm");

       form.submit(function()
       {
           if(!validateEmail(emailField.val()))
               return false;
           else if(!validatePassword(passwordField.val()))
               return false;
           else if(!validatePassword(retPasswordField.val()))
               return false;
           else if(!valideRetypePassword(passwordField.val(),retPasswordField.val()))
               return false;
           else if(!($("#Iagree").prop('checked')))
               return false;
           else
               return true;
       });
});